﻿/****************************************************
	文件：PECommon.cs
	作者：Ling
	邮箱: 1759147969@qq.com
	日期：2019/01/11 20:24   	
	功能：客户端服务端公用工具类
*****************************************************/

using PENet;
using PEProtocal;

public enum LogType
{
    Log = 0,
    Warn = 1,
    Error = 2,
    Info = 3
}

public class PECommon
{
    public static void Log(string msg = "",LogType tp = LogType.Log)
    {
        LogLevel lv = (LogLevel)tp;
        PETool.LogMsg(msg, lv);
    }

    //计算战斗力
    public static int GetFightByProps(PlayerData playerData)
    {
        //返回角色的战斗力，这边就是随便计算的
        return playerData.lv * 100 + playerData.ap + playerData.ap + playerData.addef + playerData.apdef;
    }

    //获得当前等级可以拥有的上限体力值
    public static int GetPowerLimit(int lv)
    {
        return (lv - 1) / 10 * 60 + 100;
    }

    //返回当期等级需要升级的经验值
    public static int GetExpUpVal(int lv)
    {
        return 100 * lv * lv;
    }

    //升级操作
    public static void CalcExp(PlayerData player)
    {
        while (player.exp >= GetExpUpVal(player.lv))
        {
            player.exp -= GetExpUpVal(player.lv);
            player.lv += 1;
        }
    }

    public const int PowerSpace = 5;
    public const int PowerCount = 2;
}

