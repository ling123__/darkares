﻿/****************************************************
	文件：Class1.cs
	作者：Ling
	邮箱: 1759147969@qq.com
	日期：2019/01/11 14:55   	
	功能：网络通信协议（客户端，服务端通用）
*****************************************************/
using PENet;
using System;

namespace PEProtocal
{
    public enum CMD
    {
        None = 0,
        //登录相关
        ReqLogin = 101,
        RspLogin = 102,
        ReqRename = 103,   //重命名
        RspRename = 104,  //返回重命名结果

        //主城相关
        ReqGuide = 200,   //导航
        RspGuide = 201,

        //强化相关
        ReqEquip = 203,
        RspEquip = 204,

        //
        SendChat = 205,
        PushChat = 206,

        ReqBuy = 207,
        RspBuy = 208,

        RspPower = 209,

        ReqTask = 210,
        RspTask = 211,

        PshTaskPro = 212,

        ReqDungeon = 213,
        RspDungeon = 214,
    }

    public enum Error
    {
        None = 0,
        //错误码
        AcctIsOnline,   //这个是账号已经登录的
        WrongPass,      //密码错误
        NameIsExist,    //名字已经存在
        UpdateDBError,  //更新数据库出错
        ServerDataError,//服务器数据异常
        LockStar,       //星级已满
        LockLv,         //等级不够
        LockCoin,       //金币不够
        LockCrystal,    //水晶不够
        LockDiamond,    //钻石不够
        LockPower,      //体力不足
        DungeonId,      //进入未解锁关卡
    }

    [Serializable]
    public class GameMsg : PEMsg
    {
        //登陆相关
        public ReqLogin reqLogin;
        public RspLogin rspLogin;
        //修改角色名相关
        public ReqRename reqRename;
        public RspRename rspRename;
        //人物导航相关
        public ReqGuide reqGuide;
        public RspGuide rspGuide;
        //强化相关
        public ReqEquip reqEquip;
        public RspEquip rspEquip;
        //聊天内容
        public SendChat sendChat;
        public PushChat pushChat;
        //购买内容
        public ReqBuy reqBuy;
        public RspBuy rspBuy;
        //体力相关
        public RspPower rspPower;
        //任务相关
        public ReqTask reqTask;
        public RspTask rspTask;
        //任务进度
        public PshTaskPro pshTaskPro;
        //进入副本
        public ReqDungeon reqDungeon;
        public RspDungeon rspDungeon;
    }

    public class SrvCfg
    {
        public const string srvIP = "127.0.0.1";
        public const int srvPort = 17666;
    }
    //服务端接受客户端
    [Serializable]
    public class ReqLogin
    {
        public string acct;    //账号
        public string pass;    //密码
    }
    //服务端回复客户端
    [Serializable]
    public class RspLogin
    {
        public PlayerData playerData;
    }
    //任务导航
    [Serializable]
    public class ReqGuide
    {
        public int npcID;
    }
    [Serializable]
    public class RspGuide
    {
        public int npcID;
        public int coin;
        public int exp;
        public int lv;
    }
    //玩家数据
    [Serializable]
    public class PlayerData
    {
        public int id;        //人物id
        public string name;   //人物名称
        public int lv;        //人物等级
        public int exp;       //人物经验
        public int power;     //人物体力
        public int coin;      //人物金钱
        public int diamond;   //人物钻石
        public int crystal;    //人物水晶

        public int hp;        //人物血量
        public int ad;        //人物攻击
        public int ap;        //人物法术
        public int addef;     //人物物防
        public int apdef;     //人物法防
        public int dodge;     //人物闪避
        public int pierce;    //人物穿透
        public int critical;  //人物暴击
        public int guideid;   //任务进度
        public int[] equip;   //人物装备星级
        public long timer;    //时间
        public string[] taskArr;//任务
        public int dungeon;//副本关卡
    }
    [Serializable]
    public class ReqRename
    {
        public string name;
    }
    [Serializable]
    public class RspRename
    {
        public string name;
    }

    [Serializable]
    public class ReqEquip
    {
        public int pos;
    }

    [Serializable]
    public class RspEquip
    {
        public int pos;
        public int hp;
        public int ad;
        public int ap;
        public int addef;
        public int apdef;
        public int coin;
        public int crystal;
        public int[] equip;  //返回当前物品当前星际的升级属性
    }

    [Serializable]
    public class SendChat
    {
        public int chatType;
        public string msg;
    }

    [Serializable]
    public class PushChat
    {
        public int chatType;
        public string name;
        public string msg;
    }

    [Serializable]
    public class ReqBuy
    {
        public int diamond;
        public int type;
    }

    [Serializable]
    public class RspBuy
    {
        public int type;
        public int diamond;
        public int coin;
        public int power;
    }

    [Serializable]
    public class RspPower
    {
        public int power;
    }
    [Serializable]
    public class ReqTask
    {
        public int id;
    }

    [Serializable]
    public class RspTask
    {
        public int exp;
        public int coin;
        public int lv;
        public string[] taskArr;
    }

    [Serializable]
    public class PshTaskPro
    {
        public string[] taskArr;
    }

    [Serializable]
    public class ReqDungeon
    {
        public int tid;
    }

    [Serializable]
    public class RspDungeon
    {
        public int tid;
        public int power;
    }
}
