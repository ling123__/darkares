﻿/****************************************************
	文件：DBMgr.cs
	作者：Ling
	邮箱: 1759147969@qq.com
	日期：2019/01/12 14:59   	
	功能：数据库管理类
*****************************************************/


using MySql.Data.MySqlClient;
using PEProtocal;
using System;

public class DBMgr
{
    private static DBMgr _instance = null;
    public static DBMgr Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = new DBMgr();
            }
            return _instance;
        }
    }

    private MySqlConnection conn;

    public void Init()
    {
        conn = new MySqlConnection("server=localhost;User Id=root;password=root;Database=darkgod;port=3306;Charset=utf8");
        conn.Open();    //连接后需要打开才能够使用
        PECommon.Log("DBMgr Init Done");

        //QueryPlayerData("ling", "ling");
    }

    public PlayerData QueryPlayerData(string acct, string pass)
    {
        PlayerData playerData = null;
        MySqlDataReader reader = null;   //保存查询数据库得到的结果
        bool usNewData = true;  //判断是否是新的数据

        try
        {
            MySqlCommand cmd = new MySqlCommand("Select * from account where acct = @acct", conn);
            cmd.Parameters.AddWithValue("acct", acct);  //设置变量的值
            reader = cmd.ExecuteReader();  //查询并保存

            if (reader.Read())
            {
                usNewData = false;   //如果进入到了这个里面，就说明之前已经有了数据了
                string _pass = reader.GetString("pass");
                if (_pass.Equals(pass))
                {
                    //密码正确
                    playerData = new PlayerData
                    {
                        id = reader.GetInt32("id"),
                        name = reader.GetString("name"),
                        lv = reader.GetInt32("lv"),
                        exp = reader.GetInt32("exp"),
                        power = reader.GetInt32("power"),
                        coin = reader.GetInt32("coin"),
                        diamond = reader.GetInt32("diamond"),
                        crystal = reader.GetInt32("crystal"),

                        hp = reader.GetInt32("hp"),
                        ad = reader.GetInt32("ad"),
                        ap = reader.GetInt32("ap"),
                        addef = reader.GetInt32("addef"),
                        apdef = reader.GetInt32("apdef"),
                        dodge = reader.GetInt32("dodge"),
                        pierce = reader.GetInt32("pierce"),
                        critical = reader.GetInt32("critical"),
                        guideid = reader.GetInt32("guideid"),
                        timer = reader.GetInt64("timer"),
                        dungeon = reader.GetInt32("dungeon"),
                        //查询数据，并切割字符串
                    };
                    string[] tempArr = reader.GetString("equip").Split('#');
                    int[] equipArr = new int[6];
                    for (int i = 0; i < tempArr.Length; i++)
                    {
                        if (tempArr[i] == "")
                            continue;
                        if (int.TryParse(tempArr[i], out int value))
                        {
                            equipArr[i] = value;
                        }
                        else
                        {
                            PECommon.Log("装备升级信息无法转化成整数");
                        }
                    }
                    playerData.equip = equipArr;

                    //任务信息
                    tempArr = reader.GetString("taskArr").Split('#');
                    playerData.taskArr = new string[6];
                    for (int i = 0; i < tempArr.Length; i++)
                    {
                        if (tempArr[i] == "")
                            continue;
                        else if (tempArr[i].Length >= 5)
                        {
                            playerData.taskArr[i] = tempArr[i];
                        }
                        else
                        {
                            throw new Exception("TaskArr Error");
                        }
                    }
                }
            }
        }
        catch (Exception e)
        {
            PECommon.Log("Query PlayerData By Acct & Pass Error:" + e, LogType.Error);
        }
        finally
        {
            if (reader != null)
            {
                reader.Close();   //关闭连接查询
            }
            if (usNewData)
            {
                //配置新用户的数据,应该是要读取配置文件的
                playerData = new PlayerData
                {
                    id = -1,
                    name = "",
                    lv = 1,
                    exp = 0,
                    power = 100,
                    coin = 5000,
                    diamond = 50,
                    crystal = 50,

                    hp = 2000,
                    ad = 275,
                    ap = 265,
                    addef = 67,
                    apdef = 43,
                    dodge = 7,
                    pierce = 5,
                    critical = 2,
                    guideid = 1001,
                    equip = new int[6],
                    timer = TimerSvc.Instance.GetNowTimer(),
                    taskArr = new string[6],
                    dungeon = 10001,
                };
                //初始化任务奖励数据  ,这边任务长度写死了
                for (int i = 0; i < 6; i++)
                {
                    playerData.taskArr[i] = (i + 1) + "|0|0";
                }
                playerData.id = InsertNewAccData(acct, pass, playerData);
            }
        }
        return playerData;
    }

    private int InsertNewAccData(string acct, string pass, PlayerData playerData)
    {
        int id = -1;
        try
        {
            MySqlCommand cmd = new MySqlCommand(
                "insert into account set acct = @acct,pass=@pass,name=@name," +
                "lv=@lv,exp=@exp,power=@power,coin=@coin,diamond=@diamond,crystal=@crystal," +
                "hp=@hp,ad=@ad,ap=@ap,addef=@addef,apdef=@apdef,dodge=@dodge," +
                "pierce=@pierce,critical=@critical,guideid=@guideid,equip=@equip," +
                "timer=@timer,taskArr=@taskArr,dungeon=@dungeon", conn);
            /*设置参数值*/
            cmd.Parameters.AddWithValue("acct", acct);
            cmd.Parameters.AddWithValue("pass", pass);
            cmd.Parameters.AddWithValue("name", playerData.name);
            cmd.Parameters.AddWithValue("lv", playerData.lv);
            cmd.Parameters.AddWithValue("exp", playerData.exp);
            cmd.Parameters.AddWithValue("power", playerData.power);
            cmd.Parameters.AddWithValue("coin", playerData.coin);
            cmd.Parameters.AddWithValue("diamond", playerData.diamond);
            cmd.Parameters.AddWithValue("crystal", playerData.crystal);

            cmd.Parameters.AddWithValue("hp", playerData.hp);
            cmd.Parameters.AddWithValue("ad", playerData.ad);
            cmd.Parameters.AddWithValue("ap", playerData.ap);
            cmd.Parameters.AddWithValue("addef", playerData.addef);
            cmd.Parameters.AddWithValue("apdef", playerData.apdef);
            cmd.Parameters.AddWithValue("dodge", playerData.dodge);
            cmd.Parameters.AddWithValue("pierce", playerData.pierce);
            cmd.Parameters.AddWithValue("critical", playerData.critical);
            cmd.Parameters.AddWithValue("guideid", playerData.guideid);
            //装备信息写入
            string equipStr = "";
            for (int i = 0; i < playerData.equip.Length; i++)
            {
                equipStr += playerData.equip[i] + "#";
            }
            cmd.Parameters.AddWithValue("equip", equipStr);
            cmd.Parameters.AddWithValue("timer", playerData.timer);
            //任务信息写入
            string taskStr = "";
            for (int i = 0; i < playerData.taskArr.Length; i++)
            {
                taskStr += playerData.taskArr[i] + "#";
            }
            cmd.Parameters.AddWithValue("taskArr", taskStr);
            cmd.Parameters.AddWithValue("dungeon", playerData.dungeon);

            cmd.ExecuteNonQuery();
            id = (int)cmd.LastInsertedId;  //获得数据插入后的id值
        }
        catch (Exception e)
        {
            PECommon.Log("Query PlayerData By Acct&Pass Error:" + e, LogType.Error);
        }
        return id;
    }

    //名字是否存在
    public bool QueryNameData(string name)
    {
        bool exist = false;
        MySqlDataReader reader = null;
        try
        {
            MySqlCommand cmd = new MySqlCommand("select * from account where name=@name", conn);
            cmd.Parameters.AddWithValue("name", name);
            reader = cmd.ExecuteReader();
            if (reader.Read())
            {
                exist = true;
            }
        }
        catch (Exception e)
        {
            PECommon.Log("Query PlayerData By Acct&Pass Error:" + e, LogType.Error);
        }
        finally
        {
            if (reader != null)
            {
                reader.Close();
            }
        }
        return exist;
    }

    //更新用户数据
    public bool UpdatePlayerData(int id, PlayerData playerData)
    {
        try
        {
            MySqlCommand cmd = new MySqlCommand(
                "Update account set name=@name,lv=@lv,exp=@exp," +
                "power=@power,coin=@coin,diamond=@diamond,crystal=@crystal," +
                "hp=@hp,ad=@ad,ap=@ap,addef=@addef,apdef=@apdef," +
                "dodge=@dodge,pierce=@pierce,critical=@critical," +
                "guideid=@guideid,equip=@equip,timer=@timer,taskArr=@taskArr," +
                "dungeon=@dungeon where id=@id", conn);
            cmd.Parameters.AddWithValue("id", id);
            cmd.Parameters.AddWithValue("name", playerData.name);
            cmd.Parameters.AddWithValue("lv", playerData.lv);
            cmd.Parameters.AddWithValue("exp", playerData.exp);
            cmd.Parameters.AddWithValue("power", playerData.power);
            cmd.Parameters.AddWithValue("coin", playerData.coin);
            cmd.Parameters.AddWithValue("diamond", playerData.diamond);
            cmd.Parameters.AddWithValue("crystal", playerData.crystal);

            cmd.Parameters.AddWithValue("hp", playerData.hp);
            cmd.Parameters.AddWithValue("ad", playerData.ad);
            cmd.Parameters.AddWithValue("ap", playerData.ap);
            cmd.Parameters.AddWithValue("addef", playerData.addef);
            cmd.Parameters.AddWithValue("apdef", playerData.apdef);
            cmd.Parameters.AddWithValue("dodge", playerData.dodge);
            cmd.Parameters.AddWithValue("pierce", playerData.pierce);
            cmd.Parameters.AddWithValue("critical", playerData.critical);
            cmd.Parameters.AddWithValue("guideid", playerData.guideid);

            string equipStr = "";
            for (int i = 0; i < playerData.equip.Length; i++)
            {
                equipStr += playerData.equip[i] + "#";
            }
            cmd.Parameters.AddWithValue("equip", equipStr);
            cmd.Parameters.AddWithValue("timer", playerData.timer);
            //任务信息写入
            string taskStr = "";
            for (int i = 0; i < playerData.taskArr.Length; i++)
            {
                taskStr += playerData.taskArr[i] + "#";
            }
            cmd.Parameters.AddWithValue("taskArr", taskStr);
            cmd.Parameters.AddWithValue("dungeon", playerData.dungeon);

            cmd.ExecuteNonQuery();
        }
        catch (Exception e)
        {
            PECommon.Log("Update PlayerData Error:" + e, LogType.Error);
            return false;
        }
        return true;
    }
}