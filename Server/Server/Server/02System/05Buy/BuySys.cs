﻿/****************************************************
	文件：BuySys.cs
	作者：Ling
	邮箱: 1759147969@qq.com
	日期：2019/08/17 9:23   	
	功能：购买商品系统
*****************************************************/
using PEProtocal;

class BuySys
{
    private static BuySys _instance = null;
    public static BuySys Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = new BuySys();
            }
            return _instance;
        }
    }

    private CacheSvc cachSvc = null;
    public void Init()
    {
        cachSvc = CacheSvc.Instance;
        PECommon.Log("BuySys  Init Done");
    }

    public void ReqBuy(MsgPack pack)
    {
        ReqBuy req = pack.msg.reqBuy;
        PlayerData player = cachSvc.GetPlayerDataBySession(pack.session);
        PshTaskPro pshTaskPro = null;

        GameMsg msg = new GameMsg
        {
            cmd = (int)CMD.RspBuy,
        };
        //TODO 这边以后有机会也要改，不能写死数值
        if(player.diamond < 10)
        {
            msg.err = (int)Error.LockDiamond;
        }
        else
        {
            player.diamond -= 10;
            switch (req.type)
            {
                case 0:
                    player.power += 100;
                    pshTaskPro = TaskSys.Instance.GetTaskPrgs(player, (int)TaskID.Task4);
                    break;
                case 1:
                    player.coin += 1000;
                    pshTaskPro = TaskSys.Instance.GetTaskPrgs(player, (int)TaskID.Task5);
                    break;
            }
            if (!cachSvc.UpdatePlayerData(player.id, player))
            {
                msg.err = (int)Error.UpdateDBError;
            }
            RspBuy rsp = new RspBuy
            {
                type = req.type,
                diamond = player.diamond,
                coin = player.coin,
                power = player.power
            };
            msg.rspBuy = rsp;
            msg.pshTaskPro = pshTaskPro;
        }
        pack.session.SendMsg(msg);
    }
}
