﻿/****************************************************
	文件：PowerSys.cs
	作者：Ling
	邮箱: 1759147969@qq.com
	日期：2019/08/17 15:29   	
	功能：体力系统
*****************************************************/
using PEProtocal;
using System.Collections.Generic;

class PowerSys
{
    private static PowerSys _instance = null;
    public static PowerSys Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = new PowerSys();
            }
            return _instance;
        }
    }
    private CacheSvc cachSvc = null;
    public void Init()
    {
        cachSvc = CacheSvc.Instance;

        //使用计时器定时增加体力
        TimerSvc.Instance.AddTimeTask(CalcPowerAdd, PECommon.PowerSpace, PETimeUnit.Minute, 0);
        PECommon.Log("PowerSys  Init Done");
    }

    private void CalcPowerAdd(int tid)
    {
        GameMsg msg = new GameMsg
        {
            cmd = (int)CMD.RspPower,
            rspPower = new RspPower()
        };

        Dictionary<ServerSession, PlayerData> onLineDict = cachSvc.GetOnLineCache();
        foreach(var item in onLineDict)
        {
            PlayerData player = item.Value;
            ServerSession session = item.Key;
            int power = PECommon.GetPowerLimit(player.lv);
            if(player.power >= power)
            {
                continue;
            }
            else
            {
                player.power += PECommon.PowerCount;
                player.timer = TimerSvc.Instance.GetNowTimer();
                if (player.power > power)
                    player.power = power;
            }
            if (!cachSvc.UpdatePlayerData(player.id, player))
            {
                msg.err = (int)Error.UpdateDBError;
            }
            else
            {
                msg.rspPower.power = player.power;
                session.SendMsg(msg);
            }
            
        }
    }
}

