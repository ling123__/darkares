﻿/****************************************************
	文件：EquipSys.cs
	作者：Ling
	邮箱: 1759147969@qq.com
	日期：2019/08/16 8:35   	
	功能：装备强化系统
*****************************************************/


using PEProtocal;
using Server._01Service._02CfgSvc;

class EquipSys
{
    private static EquipSys _instance = null;
    public static EquipSys Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = new EquipSys();
            }
            return _instance;
        }
    }

    private CacheSvc cachSvc = null;
    public void Init()
    {
        cachSvc = CacheSvc.Instance;
        PECommon.Log("EquipSys  Init Done");
    }

    //处理客户端发送过来的强化消息
    public void ReqEquip(MsgPack pack)
    {
        ReqEquip req = pack.msg.reqEquip;

        GameMsg msg = new GameMsg
        {
            cmd = (int)CMD.RspEquip
        };

        PlayerData player = cachSvc.GetPlayerDataBySession(pack.session);
        int currentLvel = player.equip[req.pos];
        EquipCfg equip = CfgSvc.Instance.GetEquipData(req.pos, currentLvel + 1);

        if(equip == null)
            msg.err = (int)Error.LockStar;
        else if (player.lv < equip.MinLv)
            msg.err = (int)Error.LockLv;
        else if (player.coin < equip.Coin)
            msg.err = (int)Error.LockCoin;
        else if (player.crystal < equip.Crystal)
            msg.err = (int)Error.LockCrystal;
        else
        {
            //任务进度更新
            TaskSys.Instance.CalcTaskPrgs(player, (int)TaskID.Task3);
            player.coin -= equip.Coin;
            player.crystal -= equip.Crystal;
            player.equip[req.pos] += 1;  //星级加一

            //TODO 这边写死了装备
            player.hp += equip.AddHp;
            player.ad += equip.AddHurt;
            player.ap += equip.AddHurt;
            player.addef += equip.AddDef;
            player.apdef += equip.AddDef;
        }

        if (!cachSvc.UpdatePlayerData(player.id, player))
        {
            msg.err = (int)Error.UpdateDBError;
        }
        else
        {
            msg.rspEquip = new RspEquip
            {
                coin = player.coin,
                crystal = player.crystal,
                hp = player.hp,
                ad = player.ad,
                ap = player.ap,
                addef = player.addef,
                apdef = player.apdef,
                equip = player.equip
            };
        }

        pack.session.SendMsg(msg);
    }
}

