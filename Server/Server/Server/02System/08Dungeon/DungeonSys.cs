﻿/****************************************************
	文件：DungeonSys.cs
	作者：Ling
	邮箱: 1759147969@qq.com
	日期：2019/08/22 17:26   	
	功能：副本战斗系统
*****************************************************/
using PEProtocal;
using Server._01Service._02CfgSvc;

class DungeonSys
{
    private static DungeonSys _instance = null;
    public static DungeonSys Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = new DungeonSys();
            }
            return _instance;
        }
    }
    private CacheSvc cachSvc = null;
    private PlayerData _player;
    public void Init()
    {
        cachSvc = CacheSvc.Instance;
        PECommon.Log("DungeonSys  Init Done");
    }

    public void ReqDungeon(MsgPack pack)
    {
        ReqDungeon req = pack.msg.reqDungeon;
        _player = cachSvc.GetPlayerDataBySession(pack.session);

        int power = CfgSvc.Instance.GetMapCfg(req.tid).power;

        GameMsg msg = new GameMsg
        {
            cmd = (int)CMD.RspDungeon,
            rspDungeon = new RspDungeon(),
        };
        if(req.tid <= _player.dungeon)
        {
            if(_player.power >= power)
            {
                _player.power -= power;
                if (!cachSvc.UpdatePlayerData(_player.id, _player))
                {
                    msg.err = (int)Error.UpdateDBError;
                }
                else
                {
                    msg.rspDungeon.tid = req.tid;
                    msg.rspDungeon.power = _player.power;
                }
            }
            else
            {
                msg.err = (int)Error.LockPower;
            }
        }
        else
        {
            msg.err = (int)Error.DungeonId;
        }
        pack.session.SendMsg(msg);
    }
}

