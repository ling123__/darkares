﻿/****************************************************
	文件：GuideSys.cs
	作者：Ling
	邮箱: 1759147969@qq.com
	日期：2019/01/26 19:06   	
	功能：主城任务引导类
*****************************************************/

using PEProtocal;
using Server._01Service._02CfgSvc;

namespace Server._02System._02Guide
{
    class GuideSys
    {
        private static GuideSys _instance = null;
        public static GuideSys Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new GuideSys();
                }
                return _instance;
            }
        }

        private CacheSvc cachSvc = null;
        private CfgSvc cfgSvc = null;

        public void Init()
        {
            cachSvc = CacheSvc.Instance;
            cfgSvc = CfgSvc.Instance;
            PECommon.Log("GuideSys Init Done");
        }


        public void ReqGuide(MsgPack pack)
        {
            ReqGuide data = pack.msg.reqGuide;

            GameMsg msg = new GameMsg 
            {
                cmd = (int)CMD.RspGuide
            };

            PlayerData pd = cachSvc.GetPlayerDataBySession(pack.session);
            GuideCfg gc = cfgSvc.GetGuideData(data.npcID);

            //更新引导ID
            if (pd.guideid == data.npcID)
            {
                //触发任务检测
                if (pd.guideid == 1001) {
                    //TODO 这里不应该用明文
                    TaskSys.Instance.CalcTaskPrgs(pd,(int)TaskID.Task1);
                }
                pd.guideid += 1;
                //更新玩家数据
                pd.coin += gc.coin;
                ClaExp(pd, gc.exp);

                if (!cachSvc.UpdatePlayerData(pd.id, pd))
                {
                    msg.err = (int)Error.UpdateDBError;  //数据库更新错误
                }
                else
                {
                    msg.rspGuide = new RspGuide
                    {
                        npcID = pd.guideid,
                        coin = pd.coin,
                        exp = pd.exp,
                        lv = pd.lv
                    };
                }
            }
            else
            {
                msg.err = (int)Error.ServerDataError;
            }

            pack.session.SendMsg(msg);
        }

        public void ClaExp(PlayerData pd,int addExp)
        {
            int curtLV = pd.lv;
            int curtExp = pd.exp;
            int addRestExp = addExp;
            while (true)
            {
                int upNeedExp = PECommon.GetExpUpVal(curtLV) - curtExp;
                //如果当前经验值大于升级所需经验值
                if(addRestExp >= upNeedExp)
                {
                    curtLV++;
                    curtExp = 0;
                    addRestExp -= upNeedExp;
                }
                else
                {
                    //当前经验不足以升级
                    pd.lv = curtLV;
                    pd.exp = curtExp + addRestExp;
                    break;
                }
            }
        }
    }
}

