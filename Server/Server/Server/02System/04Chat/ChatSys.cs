﻿/****************************************************
	文件：ChatSys.cs
	作者：Ling
	邮箱: 1759147969@qq.com
	日期：2019/08/16 22:04   	
	功能：网络聊天系统
*****************************************************/
using PEProtocal;
using System.Collections.Generic;

class ChatSys
{
    private static ChatSys _instance = null;
    public static ChatSys Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = new ChatSys();
            }
            return _instance;
        }
    }

    private CacheSvc cachSvc = null;
    public void Init()
    {
        cachSvc = CacheSvc.Instance;
        PECommon.Log("ChatSys  Init Done");
    }

    public void SendChat(MsgPack pack)
    {
        //TODO  根据chatType发送给不同的用户
        SendChat send = pack.msg.sendChat;
        PlayerData player = cachSvc.GetPlayerDataBySession(pack.session);

        TaskSys.Instance.CalcTaskPrgs(player, (int)TaskID.Task6);
        GameMsg msg = new GameMsg
        {
            cmd = (int)CMD.PushChat,
            pushChat = new PushChat
            {
                chatType = send.chatType,
                name = player.name,
                msg = send.msg
            }
        };
        List<ServerSession> list = cachSvc.GetOnLineAcctSession();
        //先将消息转换成二进制
        byte[] bytes = PENet.PETool.PackNetMsg(msg);
        for(int i = 0;i < list.Count;i++)
        {
            list[i].SendMsg(bytes);
        }
    }
}

