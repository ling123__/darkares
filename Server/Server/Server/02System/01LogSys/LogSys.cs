﻿/****************************************************
	文件：LogSys.cs
	作者：Ling
	邮箱: 1759147969@qq.com
	日期：2019/01/11 14:44   	
	功能：登录业务系统
*****************************************************/

using PENet;
using PEProtocal;

class LogSys
{
    private static LogSys _instance = null;
    public static LogSys Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = new LogSys();
            }
            return _instance;
        }
    }

    private CacheSvc cachSvc = null;

    public void Init()
    {
        cachSvc = CacheSvc.Instance;
        PECommon.Log("LogSys Init Done");
    }

    public void ReqLogin(MsgPack msgPack)
    {
        ReqLogin data = msgPack.msg.reqLogin;
        GameMsg msg = new GameMsg
        {
            cmd = (int)CMD.RspLogin
        };
        //判断当前账号是否上线
        if (cachSvc.isAcctOnLine(data.acct))
        {
            //已经上线，返回错误信息
            msg.err = (int)Error.AcctIsOnline;
        }
        else
        {
            //没有上线，判断账号是否存在。
            PlayerData playerData = cachSvc.GetPlayerData(data.acct, data.pass);

            //如果存在，检测密码
            if (playerData == null)
            {
                msg.err = (int)Error.WrongPass;
            }
            else
            {
                //TODO 计算用户离线体力
                int power = playerData.power;
                long nowTimer = TimerSvc.Instance.GetNowTimer();
                int addPower = (int)((nowTimer - playerData.timer) / (1000 * 60 * PECommon.PowerSpace) * PECommon.PowerCount);
                int maxPower = PECommon.GetPowerLimit(playerData.lv);
                if (addPower > 0)
                {
                    playerData.power += addPower;
                    if (playerData.power >= maxPower)
                    {
                        playerData.power = maxPower;
                    }
                }
                if(power != playerData.power)
                {
                    cachSvc.UpdatePlayerData(playerData.id, playerData);
                }
                //密码正确
                msg.rspLogin = new RspLogin
                {
                    playerData = playerData
                };
                //缓存账号
                cachSvc.AddAcctOnLine(data.acct, msgPack.session, playerData);
            }
        }      
        //回应客户端
        msgPack.session.SendMsg(msg);
    }

    public void ReqRename(MsgPack msgPack)
    {
        ReqRename data = msgPack.msg.reqRename;
        GameMsg msg = new GameMsg
        {
            cmd = (int)CMD.RspRename
        };

        //判断名字是否存在
        if (cachSvc.IsNameExist(data.name))
        {
            //存在 返回错误码
            msg.err = (int)Error.NameIsExist;
        }
        else
        {
            //不存在 更新缓存。以及数据库，再返回给客户端
            PlayerData playerData = cachSvc.GetPlayerDataBySession(msgPack.session);
            playerData.name = data.name;

            if (!cachSvc.UpdatePlayerData(playerData.id, playerData))
            {
                msg.err = (int)Error.UpdateDBError;
            }
            else
            {
                msg.rspRename = new RspRename
                {
                    name = data.name
                };
            }
        }
        msgPack.session.SendMsg(msg);
    }

    //中转站，调用CachesSvc中的清除数据
    public void ClearOfflineData(ServerSession session)
    {
        PlayerData playerData = cachSvc.GetPlayerDataBySession(session);
        if (playerData != null)
        {
            playerData.timer = TimerSvc.Instance.GetNowTimer();
            if (!cachSvc.UpdatePlayerData(playerData.id, playerData))
            {
                PECommon.Log("Update offline timer error");
            }
            cachSvc.RemoveAcctOutLine(session);
        }
    }
}

