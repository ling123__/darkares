﻿/****************************************************
	文件：TaskSys.cs
	作者：Ling
	邮箱: 1759147969@qq.com
	日期：2019/08/21 16:27   	
	功能：任务管理系统
*****************************************************/
using PEProtocal;
using Server._01Service._02CfgSvc;

class TaskSys
{
    private static TaskSys _instance = null;
    public static TaskSys Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = new TaskSys();
            }
            return _instance;
        }
    }
    private CacheSvc cachSvc = null;
    private PlayerData _player;
    public void Init()
    {
        cachSvc = CacheSvc.Instance;
        PECommon.Log("TaskSys  Init Done");
    }

    public void ReqTask(MsgPack pack)
    {
        int id = pack.msg.reqTask.id;
        _player = cachSvc.GetPlayerDataBySession(pack.session);
        //获得id任务奖励
        TaskRewardCfg rewardCfg = CfgSvc.Instance.GetTaskCfg(id);
        _player.taskArr[id - 1] = id + "|" + rewardCfg.Count + "|1";
        _player.exp += rewardCfg.Exp;

        PECommon.CalcExp(_player);
        GameMsg msg = new GameMsg
        {
            cmd = (int)CMD.RspTask,
            rspTask = new RspTask
            {
                exp = _player.exp,
                coin = rewardCfg.Coin,
                lv = _player.lv,
                taskArr = _player.taskArr
            }
        };
        if (!cachSvc.UpdatePlayerData(_player.id, _player))
        {
            msg.err = (int)Error.UpdateDBError;
        }
        pack.session.SendMsg(msg);
    }
    //更新任务进度
    public void CalcTaskPrgs(PlayerData player, int id)
    {
        string[] task = player.taskArr[id - 1].Split('|');
        int pro = int.Parse(task[1]);
        int count = int.Parse(task[2]);
        TaskRewardCfg cfg = CfgSvc.Instance.GetTaskCfg(id);
        //任务还没有完成
        if (pro < cfg.Count)
        {
            pro += 1;
            player.taskArr[id - 1] = id + "|" + pro + "|0";

            ServerSession session = cachSvc.GetOnLineServerSession(player.id);

            GameMsg msg = new GameMsg
            {
                cmd = (int)CMD.PshTaskPro,
                pshTaskPro = new PshTaskPro
                {
                    taskArr = player.taskArr
                }
            };

            if (!cachSvc.UpdatePlayerData(player.id, player))
            {
                msg.err = (int)Error.UpdateDBError;
            }
            else if (session != null)
            {
                session.SendMsg(msg);
            }
        }
    }

    public PshTaskPro GetTaskPrgs(PlayerData player, int id)
    {
        string[] task = player.taskArr[id - 1].Split('|');
        int pro = int.Parse(task[1]);
        int count = int.Parse(task[2]);
        TaskRewardCfg cfg = CfgSvc.Instance.GetTaskCfg(id);
        //任务还没有完成
        if (pro < cfg.Count)
        {
            pro += 1;
            player.taskArr[id - 1] = id + "|" + pro + "|0";

            return new PshTaskPro
            {
                taskArr = player.taskArr
            };
        }
        else
        {
            return null;
        }
    }
}

