﻿/****************************************************
	文件：ServerSession.cs
	作者：Ling
	邮箱: 1759147969@qq.com
	日期：2019/01/11 15:03   	
	功能：网络会话连接
*****************************************************/

using PENet;
using PEProtocal;


public class ServerSession : PESession<GameMsg>
{
    public int sessionID = 0;  //sessionID号
    //建立连接时有反馈
    protected override void OnConnected()
    {
        sessionID = ServerRoot.Instance.GetSessionID();
        PECommon.Log("SessionID:" + sessionID + "Client Connect");
    }
    //收到数据时，进行数据处理
    protected override void OnReciveMsg(GameMsg msg)
    {
        PECommon.Log("SessionID:" + sessionID + " Recive CMD:" + ((CMD)msg.cmd).ToString());

        NetService.Instance.AddMsgQueue(this,msg);    //将信息添加到Queue中
    }
    //断开连接时，处理
    protected override void OnDisConnected()
    {
        LogSys.Instance.ClearOfflineData(this);  //登出时清除用户数据
        PECommon.Log("SessionID:" + sessionID + "Client Discount");
    }
}
