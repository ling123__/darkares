﻿/****************************************************
	文件：NetService.cs
	作者：Ling
	邮箱: 1759147969@qq.com
	日期：2019/01/11 14:42   	
	功能：网络服务
*****************************************************/

using PENet;
using PEProtocal;
using Server._02System._02Guide;
using System.Collections.Generic;

public class MsgPack
{
    public ServerSession session;
    public GameMsg msg;
    public MsgPack(ServerSession session, GameMsg msg)
    {
        this.session = session;
        this.msg = msg;
    }
}

class NetService
{
    private static NetService _instance = null;
    public static NetService Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = new NetService();
            }
            return _instance;
        }
    }
    public static readonly string obj = "lock";  //上锁
    private Queue<MsgPack> msgPackQue = new Queue<MsgPack>();

    public void Init()
    {
        PESocket<ServerSession, GameMsg> server = new PESocket<ServerSession, GameMsg>();
        server.StartAsServer(SrvCfg.srvIP, SrvCfg.srvPort);   //创建服务器

        PECommon.Log("NetServer Init Done");
    }

    public void AddMsgQueue(ServerSession session ,GameMsg msg)
    {
        //以防多线程
        lock (obj)
        {
            msgPackQue.Enqueue(new MsgPack(session,msg));
        }
    }

    public void Update()
    {
        if (msgPackQue.Count > 0)
        {
            lock (obj)
            {
                MsgPack msg = msgPackQue.Dequeue();  //取出数据
                HandOutMsg(msg);
            }
        }
    }

    //分发消息出去
    private void HandOutMsg(MsgPack pack)
    {
        switch ((CMD)pack.msg.cmd)
        {
            case CMD.ReqLogin:
                LogSys.Instance.ReqLogin(pack);
                break;
            case CMD.ReqRename:
                LogSys.Instance.ReqRename(pack);
                break;
            case CMD.ReqGuide:
                GuideSys.Instance.ReqGuide(pack);
                break;
            case CMD.ReqEquip:
                EquipSys.Instance.ReqEquip(pack);
                break;
            case CMD.SendChat:
                ChatSys.Instance.SendChat(pack);
                break;
            case CMD.ReqBuy:
                BuySys.Instance.ReqBuy(pack);
                break;
            case CMD.ReqTask:
                TaskSys.Instance.ReqTask(pack);
                break;
            case CMD.ReqDungeon:
                DungeonSys.Instance.ReqDungeon(pack);
                break;
        }
    }
}

