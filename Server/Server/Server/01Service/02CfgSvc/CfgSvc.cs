﻿/****************************************************
	文件：CfgSvc.cs
	作者：Ling
	邮箱: 1759147969@qq.com
	日期：2019/01/26 19:43   	
	功能：服务器数据文件配置
*****************************************************/

using System;
using System.Collections.Generic;
using System.Xml;

namespace Server._01Service._02CfgSvc
{
    class CfgSvc
    {
        private static CfgSvc _instance = null;
        public static CfgSvc Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new CfgSvc();
                }
                return _instance;
            }
        }

        public void Init()
        {
            PECommon.Log("CfgSvc init Done");
            InitGuideCfg();   //读取XML配置文件
            InitEquip();
            InitTaskCfg();
            InitMapCfg();
        }
        #region 任务导航配置文件
        private Dictionary<int, GuideCfg> guideCfg = new Dictionary<int, GuideCfg>();
        private void InitGuideCfg()
        {
            XmlDocument doc = new XmlDocument();
            doc.Load(@"        F:\SelfGame\Game\darkares\darkares\Assets\Resources\ResCfgs\guide.xml");

        XmlNodeList nodList = doc.SelectSingleNode("root").ChildNodes;

            for(int i = 0;i < nodList.Count; i++)
            {
                XmlElement ele = nodList[i] as XmlElement;  //得到根目录节点之后再一次获取里面的数值
                //如果不存在这个ID
                if(ele.GetAttributeNode("ID") == null)
                {
                    continue;
                }
                int ID = Convert.ToInt32(ele.GetAttributeNode("ID").InnerText);
                GuideCfg mc = new GuideCfg
                {
                    ID = ID,
                };
                foreach (XmlElement e in nodList[i].ChildNodes)
                {
                    switch (e.Name) {
                        case "coin":
                            mc.coin = int.Parse(e.InnerText);
                            break;
                        case "exp":
                            mc.exp = int.Parse(e.InnerText);
                            break;
                    }
                }
                guideCfg.Add(ID, mc);
            }
            PECommon.Log("GuideCfg init Done");
        }
        public GuideCfg GetGuideData(int id)
        {
            GuideCfg agc = null;
            if(guideCfg.TryGetValue(id,out agc))
            {
                return agc;
            }
            return null;
        }
        #endregion

        #region  强化装备配置文件
        private Dictionary<int, Dictionary<int, EquipCfg>> _equipDict = new Dictionary<int, Dictionary<int, EquipCfg>>();
        private void InitEquip()
        {
            XmlDocument doc = new XmlDocument();
            doc.Load(@"F:\SelfGame\Game\darkares\darkares\Assets\Resources\ResCfgs\strong.xml");

            //root取决于xml文件中的root名称
            XmlNodeList nodLst = doc.SelectSingleNode("root").ChildNodes;

            for (int i = 0; i < nodLst.Count; i++)
            {
                XmlElement ele = nodLst[i] as XmlElement;
                //如果不存在这个ID
                if (ele.GetAttributeNode("ID") == null)
                {
                    continue;
                }
                //获得文本中的ID数值
                int ID = Convert.ToInt32(ele.GetAttributeNode("ID").InnerText);
                EquipCfg equipCfg = new EquipCfg
                {
                    ID = ID
                };
                //拿到id项中对应的子节点
                foreach (XmlElement e in nodLst[i].ChildNodes)
                {
                    int val = int.Parse(e.InnerText);
                    switch (e.Name)
                    {
                        case "pos":
                            equipCfg.Pos = val;
                            break;
                        case "starlv":
                            equipCfg.StartLv = val;
                            break;
                        case "addhp":
                            equipCfg.AddHp = val;
                            break;
                        case "addhurt":
                            equipCfg.AddHurt = val;
                            break;
                        case "adddef":
                            equipCfg.AddDef = val;
                            break;
                        case "minlv":
                            equipCfg.MinLv = val;
                            break;
                        case "coin":
                            equipCfg.Coin = val;
                            break;
                        case "crystal":
                            equipCfg.Crystal = val;
                            break;
                    }
                }
                //因为每个武器有不同的星级，所以每个pos下保存九个字典
                Dictionary<int, EquipCfg> dict = null;
                if (_equipDict.TryGetValue(equipCfg.Pos, out dict))
                {
                    dict.Add(equipCfg.StartLv, equipCfg);
                }
                else
                {
                    dict = new Dictionary<int, EquipCfg>();
                    dict.Add(equipCfg.StartLv, equipCfg);
                    _equipDict.Add(equipCfg.Pos, dict);
                }
            }
            PECommon.Log("EquipCfg init Done");
        }
        public EquipCfg GetEquipData(int pos, int startlv)
        {
            EquipCfg equipCfg = null;
            Dictionary<int, EquipCfg> dict = null;
            if (_equipDict.TryGetValue(pos, out dict))
            {
                if (dict.ContainsKey(startlv))
                {
                    equipCfg = dict[startlv];
                }
            }

            if (equipCfg == null)
            {
                PECommon.Log("当前位置：" + pos + "的物品不存在",LogType.Error);
            }
            return equipCfg;
        }
        #endregion

        #region     任务配置文件
        private Dictionary<int, TaskRewardCfg> _taskCfg = new Dictionary<int, TaskRewardCfg>();
        private void InitTaskCfg()
        {
            XmlDocument doc = new XmlDocument();
            doc.Load(@"F:\SelfGame\Game\darkares\darkares\Assets\Resources\ResCfgs\TaskReward.xml");

            XmlNodeList nodList = doc.SelectSingleNode("root").ChildNodes;

            for (int i = 0; i < nodList.Count; i++)
            {
                XmlElement ele = nodList[i] as XmlElement;  //得到根目录节点之后再一次获取里面的数值
                //如果不存在这个ID
                if (ele.GetAttributeNode("ID") == null)
                {
                    continue;
                }
                int ID = Convert.ToInt32(ele.GetAttributeNode("ID").InnerText);
                TaskRewardCfg trc = new TaskRewardCfg
                {
                    ID = ID,
                };
                foreach (XmlElement e in nodList[i].ChildNodes)
                {
                    switch (e.Name) 
                    {
                        case "count":
                            trc.Count = int.Parse(e.InnerText);
                            break;
                        case "exp":
                            trc.Exp = int.Parse(e.InnerText);
                            break;
                        case "coin":
                            trc.Coin = int.Parse(e.InnerText);
                            break;
                    }
                }
                _taskCfg.Add(ID, trc);
            }
            PECommon.Log("TaskCfg init Done");
        }

        public TaskRewardCfg GetTaskCfg(int id)
        {
            TaskRewardCfg agc = null;
            if (_taskCfg.TryGetValue(id, out agc))
            {
                return agc;
            }
            return null;
        }
        #endregion

        #region 地图信息配置
        private Dictionary<int, MapCfg> mapCfgDataDic = new Dictionary<int, MapCfg>();
        public void InitMapCfg()
        {
            XmlDocument doc = new XmlDocument();
            doc.Load(@"F:\SelfGame\Game\darkares\darkares\Assets\Resources\ResCfgs\map.xml");

            //root取决于xml文件中的root名称
            XmlNodeList nodLst = doc.SelectSingleNode("root").ChildNodes;

            for (int i = 0; i < nodLst.Count; i++)
            {
                XmlElement ele = nodLst[i] as XmlElement;
                //如果不存在这个ID
                if (ele.GetAttributeNode("ID") == null)
                {
                    continue;
                }
                //获得文本中的ID数值
                int ID = Convert.ToInt32(ele.GetAttributeNode("ID").InnerText);
                MapCfg mc = new MapCfg
                {
                    ID = ID
                };
                //拿到id项中对应的子节点
                foreach (XmlElement e in nodLst[i].ChildNodes)
                {
                    switch (e.Name)
                    {
                        case "power":
                            mc.power = int.Parse(e.InnerText);
                            break;
                    }
                }
                mapCfgDataDic.Add(ID, mc);  //将id号和对应的数据存放到字典中
            }
            PECommon.Log("MapCfg init Done");
        }

        //获取地图配置数据
        public MapCfg GetMapCfg(int id)
        {
            MapCfg data;
            //如果有相应的地图数据，则返回，否则返回空
            if (mapCfgDataDic.TryGetValue(id, out data))
            {
                return data;
            }
            return null;
        }
        #endregion

        //根据位置和星级得到
    }
    public class BaseData<T>
    {
        public int ID;
    }
    //任务导航配置
    public class GuideCfg : BaseData<GuideCfg>
    {
        public int coin;
        public int exp;
    }
    //装备配置
    public class EquipCfg : BaseData<EquipCfg>
    {
        public int Pos;
        public int StartLv;
        public int AddHp;
        public int AddHurt;
        public int AddDef;
        public int MinLv;
        public int Coin;
        public int Crystal;
    }
    //任务属性配置
    public class TaskRewardCfg : BaseData<TaskRewardCfg>
    {
        public int Count;
        public int Exp;
        public int Coin;
    }
    //存储地图的基本信息
    public class MapCfg : BaseData<MapCfg>
    {
        public int power;               //进入场景所需体力
    }
}
