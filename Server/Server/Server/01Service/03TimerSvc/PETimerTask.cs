﻿/****************************************************
    文件：PETimer.cs
	作者：Ling
    邮箱: 1759147969@qq.com
    日期：#CreateTime#
	功能：任务属性
*****************************************************/

using System;

public class PETimerTask 
{
    public int tid;          //任务id号
    public double delay;   //显示间隔时间
    public double beginTime;  //开始时间
    public Action<int> callback;  //委托函数
    public int count;       //任务执行次数

    public PETimerTask(int tid, double beginTime, double delay,int count, Action<int> callback)
    {
        this.tid = tid;
        this.beginTime = beginTime;
        this.delay = delay;
        this.count = count;
        this.callback = callback;
    }
}

public class PEFrame
{
    public int tid;          //任务id号
    public int delay;   //显示间隔时间
    public int beginFrame;  //开始时间
    public Action<int> callback;  //委托函数
    public int count;       //任务执行次数

    public PEFrame(int tid, int beginTime, int delay, int count, Action<int> callback)
    {
        this.tid = tid;
        this.beginFrame = beginTime;
        this.delay = delay;
        this.count = count;
        this.callback = callback;
    }
}

public enum PETimeUnit
{
    MillionSecond,
    Second,
    Minute,
    Hour,
    Day
}