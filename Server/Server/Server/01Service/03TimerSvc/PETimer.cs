﻿using System;
using System.Collections.Generic;
using System.Timers;

class PETimer
{
    private static readonly object obj = "lock";
    private static readonly object lockTime = "lockTime";
    private int frameTime = 0;
    private Action<string> logAction;  //函数委托
    private double nowTime;     //当前时间
    private Timer srvTime;
    private Action<Action<int>, int> taskHandle;
    //设置起始时间
    private DateTime startTime = new DateTime(1970, 1, 1, 0, 0, 0);

    /*时间定时*/
    private List<PETimerTask> taskTimeLst = new List<PETimerTask>();
    private List<PETimerTask> tmpTimeLst = new List<PETimerTask>();
    /*帧定时*/
    private List<PEFrame> taskFrameLst = new List<PEFrame>();
    private List<PEFrame> tmpFrameLst = new List<PEFrame>();

    private List<int> TidLst = new List<int>();  //存放id的数组
    private List<int> tmpTidLst = new List<int>();  //缓存id数组
    private int tid;

    public PETimer(int interval = 0)
    {
        //定时任务就丢到线程池里面运行
        if (interval != 0)
        {
            srvTime = new Timer(interval)
            {
                AutoReset = true
            };
            srvTime.Elapsed += (object sender, ElapsedEventArgs args) =>
            {
                Update();
            };
            srvTime.Start();
        }
    }

    //重置函数
    public void Reset()
    {
        TidLst.Clear();
        tmpTidLst.Clear();

        taskTimeLst.Clear();
        tmpTimeLst.Clear();

        taskFrameLst.Clear();
        tmpFrameLst.Clear();

        logAction = null;
        srvTime.Stop();
    }
    public void Update()
    {
        CheckTimeTask();
        CheckFrameTask();
        if (tmpTidLst.Count > 0)
        {
            DeleteTid();  //如果缓存id列表的个数大于0，则开始清除
        }
    }

    private void CheckTimeTask()
    {
        if (tmpTimeLst.Count > 0)
        {
            lock (lockTime)
            {
                //将缓存列表中的任务添加到任务列表中
                for (int i = 0; i < tmpTimeLst.Count; i++)
                {
                    taskTimeLst.Add(tmpTimeLst[i]);
                }
            }
        }
        //所有任务添加完以后，清空缓存列表
        tmpTimeLst.Clear();
        nowTime = GetUTCMillisceonds();
        //遍历任务数组，如果当前任务符合条件，则运行
        for (int i = 0; i < taskTimeLst.Count; i++)
        {
            PETimerTask task = taskTimeLst[i];
            if (nowTime.CompareTo(task.beginTime) < 0) { continue; }
            else
            {
                if (taskHandle != null)
                {
                    taskHandle(task.callback, task.tid);
                }
                if (task.callback != null)
                {
                    task.callback(task.tid);//如果委托不为空，则执行
                }

                if (task.count == 1)
                {
                    tmpTidLst.Add(taskTimeLst[i].tid);  //任务执行好了，放入缓存id列表，等待删除
                                                        //移除已经完成的任务,RemoveAt的作用记得
                    taskTimeLst.RemoveAt(i);
                    i--;
                }
                else
                {
                    if (task.count != 0)
                    {
                        task.count--;
                    }
                    //因为要下次继续调用，所以加上间隔时间
                    task.beginTime += task.delay;
                }
            }
        }
    }

    private void CheckFrameTask()
    {
        if (tmpTimeLst.Count > 0)
        {
            lock (lockTime)
            {
                //将缓存列表中的任务添加到任务列表中
                for (int i = 0; i < tmpFrameLst.Count; i++)
                {
                    taskFrameLst.Add(tmpFrameLst[i]);
                }
            }
        }
        //所有任务添加完以后，清空缓存列表
        tmpFrameLst.Clear();

        frameTime += 1;
        //遍历任务数组，如果当前任务符合条件，则运行
        for (int i = 0; i < taskFrameLst.Count; i++)
        {
            PEFrame task = taskFrameLst[i];
            if (frameTime < task.beginFrame) { continue; }
            else
            {
                if (taskHandle != null)
                {
                    taskHandle(task.callback, task.tid);
                }
                if (task.callback != null)
                {
                    task.callback(task.tid);//如果委托不为空，则执行
                }

                if (task.count == 1)
                {
                    tmpTidLst.Add(taskFrameLst[i].tid);  //任务执行好了，放入缓存id列表，等待删除
                                                         //移除已经完成的任务,RemoveAt的作用记得
                    taskFrameLst.RemoveAt(i);
                    i--;
                }
                else
                {
                    if (task.count != 0)
                    {
                        task.count--;
                    }
                    //因为要下次继续调用，所以加上间隔时间
                    task.beginFrame += task.delay;
                }
            }
        }
    }

    #region TimeTask
    //增加任务
    public int AddTimeTask(Action<int> callback, float delay, PETimeUnit timer = PETimeUnit.MillionSecond, int count = 1)
    {
        if (timer != PETimeUnit.MillionSecond)
        {
            switch (timer)
            {
                case PETimeUnit.Second:
                    delay = delay * 1000;
                    break;
                case PETimeUnit.Minute:
                    delay = delay * 1000 * 60;
                    break;
                case PETimeUnit.Hour:
                    delay = delay * 1000 * 60 * 60;
                    break;
                case PETimeUnit.Day:
                    delay = delay * 1000 * 60 * 60 * 24;
                    break;
                default:
                    Log("Add Time Task Type Error....");
                    break;
            }
        }
        int tid = GetTid();
        nowTime = GetUTCMillisceonds();
        lock (lockTime)
        {
            //系统运行时间+间隔时间
            tmpTimeLst.Add(new PETimerTask(tid, nowTime + delay, delay, count, callback));   //将当前任务添加到数组中
        }
        return tid;
    }
    //删除任务
    public bool DeleteTask(int tid)
    {
        bool delete = false;
        //现在任务列表中寻找，找到以后在删除tid列表的那个
        for (int i = 0; i < taskTimeLst.Count; i++)
        {
            if (tid == taskTimeLst[i].tid)
            {
                taskTimeLst.RemoveAt(i);
                for (int j = 0; j < TidLst.Count; j++)
                {
                    if (tid == TidLst[j])
                    {
                        TidLst.RemoveAt(j);
                        delete = true;
                    }
                }
            }
        }
        if (!delete)   //如果没有在任务列表中找到，在缓存列表中找一遍
        {
            for (int i = 0; i < tmpTimeLst.Count; i++)
            {
                if (tid == tmpTimeLst[i].tid)
                {
                    tmpTimeLst.RemoveAt(i);
                    for (int j = 0; j < TidLst.Count; j++)
                    {
                        if (tid == TidLst[j])
                        {
                            TidLst.RemoveAt(j);
                            delete = true;
                        }
                    }
                }
            }
        }
        return delete;
    }
    //替换任务
    public bool ReplaceTask(int tid, Action<int> callback, float delay, PETimeUnit timer = PETimeUnit.MillionSecond, int count = 1)
    {
        if (timer != PETimeUnit.MillionSecond)
        {
            switch (timer)
            {
                case PETimeUnit.Second:
                    delay = delay * 1000;
                    break;
                case PETimeUnit.Minute:
                    delay = delay * 1000 * 60;
                    break;
                case PETimeUnit.Hour:
                    delay = delay * 1000 * 60 * 60;
                    break;
                case PETimeUnit.Day:
                    delay = delay * 1000 * 60 * 60 * 24;
                    break;
                default:
                    break;
            }
        }
        //系统运行时间+间隔时间
        nowTime = GetUTCMillisceonds() + delay;
        PETimerTask petimer = new PETimerTask(tid, nowTime, delay, count, callback);

        bool isRep = false;
        //在任务列表中查找
        for (int i = 0; i < taskTimeLst.Count; i++)
        {
            if (taskTimeLst[i].tid == tid)
            {
                taskTimeLst[i] = petimer;
                isRep = true;
                break;
            }
        }
        //在临时列表中查找
        if (!isRep)
        {
            for (int i = 0; i < tmpTimeLst.Count; i++)
            {
                if (tmpTimeLst[i].tid == tid)
                {
                    tmpTimeLst[i] = petimer;
                    isRep = true;
                    break;
                }
            }
        }
        return isRep;
    }
    #endregion

    #region FrameTask
    //增加任务
    public int AddFrameTask(Action<int> callback, int delay, int count = 1)
    {
        int tid = GetTid();
        lock (lockTime)
        {
            //系统运行时间+间隔时间
            int beginFrame = frameTime + delay;
            tmpFrameLst.Add(new PEFrame(tid, beginFrame, delay, count, callback));   //将当前任务添加到数组中
        }
        return tid;
    }
    //删除任务
    public bool DeleteFrame(int tid)
    {
        bool delete = false;
        //现在任务列表中寻找，找到以后在删除tid列表的那个
        for (int i = 0; i < taskFrameLst.Count; i++)
        {
            if (tid == taskFrameLst[i].tid)
            {
                taskFrameLst.RemoveAt(i);
                for (int j = 0; j < TidLst.Count; j++)
                {
                    if (tid == TidLst[j])
                    {
                        TidLst.RemoveAt(j);
                        delete = true;
                    }
                }
            }
        }
        if (!delete)   //如果没有在任务列表中找到，在缓存列表中找一遍
        {
            for (int i = 0; i < tmpFrameLst.Count; i++)
            {
                if (tid == tmpFrameLst[i].tid)
                {
                    tmpFrameLst.RemoveAt(i);
                    for (int j = 0; j < TidLst.Count; j++)
                    {
                        if (tid == TidLst[j])
                        {
                            TidLst.RemoveAt(j);
                            delete = true;
                        }
                    }
                }
            }
        }
        return delete;
    }
    //替换任务
    public bool ReplaceFrame(int tid, Action<int> callback, int delay, int count = 1)
    {
        //系统运行时间+间隔时间
        int beginTime = frameTime + delay;
        PEFrame peFrame = new PEFrame(tid, beginTime, delay, count, callback);

        bool isRep = false;
        //在任务列表中查找
        for (int i = 0; i < taskFrameLst.Count; i++)
        {
            if (taskFrameLst[i].tid == tid)
            {
                taskFrameLst[i] = peFrame;
                isRep = true;
                break;
            }
        }
        //在临时列表中查找
        if (!isRep)
        {
            for (int i = 0; i < tmpFrameLst.Count; i++)
            {
                if (tmpFrameLst[i].tid == tid)
                {
                    tmpFrameLst[i] = peFrame;
                    isRep = true;
                    break;
                }
            }
        }
        return isRep;
    }
    #endregion

    #region GameTools
    //获得全局唯一ID
    public int GetTid()
    {
        lock (obj)
        {
            tid += 1;
            while (true)
            {
                if (tid == int.MaxValue)
                {
                    tid = 0;
                }
                bool used = false;
                for (int i = 0; i < TidLst.Count; i++)
                {
                    if (tid == TidLst[i])
                    {
                        used = true;
                        break;
                    }
                }
                if (!used)
                {
                    break;  //跳出while循环
                }
                else
                {
                    tid += 1;  //否则在向后延申
                }
            }
        }
        TidLst.Add(tid);  //将这个tid号存放到列表中
        return tid;
    }
    //清理定时任务的全局ID
    public void DeleteTid()
    {
        for (int i = 0; i < tmpTidLst.Count; i++)
        {
            for (int j = 0; j < TidLst.Count; i++)
            {
                if (tmpTidLst[i] == TidLst[j])
                {
                    TidLst.RemoveAt(j);  //删除第j个任务
                    break;
                }
            }
        }
    }
    //设置委托函数
    public void SetLog(Action<string> log)
    {
        logAction = log;
    }
    //调用委托函数，执行相应操作
    private void Log(String info)
    {
        logAction?.Invoke(info);
    }

    public double GetUTCMillisceonds()
    {
        //TimeSpan用来计算时间间隔的
        TimeSpan ts = DateTime.UtcNow - startTime;
        return ts.TotalMilliseconds;  //返回的总间隔时间的秒数
    }
    public void SetHandle(Action<Action<int>, int> handle)
    {
        taskHandle = handle;
    }
    #endregion
}