﻿/****************************************************
	文件：TimerSvc.cs
	作者：Ling
	邮箱: 1759147969@qq.com
	日期：2019/08/17 13:48   	
	功能：服务器端计时器
*****************************************************/
using System;
using System.Collections.Generic;

class TimerSvc
{
    class TaskPack
    {
        public int tid;
        public Action<int> cb;
        public TaskPack(int tid,Action<int> cb)
        {
            this.tid = tid;
            this.cb = cb;
        }
    }
    private static TimerSvc _instance = null;
    public static TimerSvc Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = new TimerSvc();
            }
            return _instance;
        }
    }

    private PETimer _peTimer = null;
    private readonly object obj = "TaskQueue";
    private Queue<TaskPack> _TaskQueue = new Queue<TaskPack>();

    public void Init()
    {
        _peTimer = new PETimer(100);
        _TaskQueue.Clear();
        _peTimer.SetLog((string info) =>
        {
            PECommon.Log(info);
        });

        _peTimer.SetHandle((Action<int> cb, int tid) =>
        {
            if(cb != null)
            {
                lock (obj)
                {
                    _TaskQueue.Enqueue(new TaskPack(tid, cb));
                }
            }
        });
        PECommon.Log("TiemrSvc Init Done");
    }

    public void Update()
    {
        //while (_TaskQueue.Count > 0)
        //{
        //    TaskPack tp = null;
        //    lock (obj)
        //    {
        //        tp = _TaskQueue.Dequeue();
                
        //    }
        //    if(tp != null)
        //    {
        //        tp.cb(tp.tid);
        //    }
        //}
    }

    //添加定时任务
    public int AddTimeTask(Action<int> callback, float delay, PETimeUnit unit = PETimeUnit.MillionSecond, int count = 1)
    {
        return _peTimer.AddTimeTask(callback, delay, unit, count);
    }
    //获得当前时间
    public long GetNowTimer()
    {
        return (long)_peTimer.GetUTCMillisceonds();
    }
}

