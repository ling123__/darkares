﻿/****************************************************
	文件：Program.cs
	作者：Ling
	邮箱: 1759147969@qq.com
	日期：2019/01/11 14:35   	
	功能：服务器入口
*****************************************************/


using System.Threading;

class ServerStart
{
    static void Main(string[] args)
    {
        ServerRoot.Instance.Init();

        //这个就和电脑的性能有关了
        while (true)
        {
            ServerRoot.Instance.Update(); //手动调用
            Thread.Sleep(20);  //休眠20毫秒，降低CPU占有率，很有用
        }
    }
}

