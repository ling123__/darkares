﻿/****************************************************
	文件：ServerRoot.cs
	作者：Ling
	邮箱: 1759147969@qq.com
	日期：2019/01/11 14:36   	
	功能：服务器初始化
*****************************************************/

using Server._01Service._02CfgSvc;
using Server._02System._02Guide;

public class ServerRoot
{
    private static ServerRoot _instance = null;
    public static ServerRoot Instance
    {
        get{
            if (_instance == null)
            {
                _instance = new ServerRoot();
            }
            return _instance;
        }
    }

    public void Init()
    {
        //数据层
        DBMgr.Instance.Init();

        //服务层
        CacheSvc.Instance.Init();   //缓存层
        NetService.Instance.Init();
        CfgSvc.Instance.Init();    //数据文件层
        TimerSvc.Instance.Init();

        //业务系统层
        LogSys.Instance.Init();
        GuideSys.Instance.Init();
        EquipSys.Instance.Init();
        ChatSys.Instance.Init();
        BuySys.Instance.Init();
        PowerSys.Instance.Init();
        TaskSys.Instance.Init();
        DungeonSys.Instance.Init();
    }

    public void Update()
    {
        NetService.Instance.Update();   //因为没有继承MonoBehaviour，所以需要手动调用
    }

    private int sessionID = 0;
    public int GetSessionID()
    {
        if(sessionID == int.MaxValue)
        {
            sessionID = 0;
        }
        return sessionID + 1;   //返回sessionID+1值
    }
}

