﻿/****************************************************
	文件：Singleton.cs
	作者：Ling
	邮箱: 1759147969@qq.com
	日期：2019/08/17 9:24   	
	功能：单例模板
*****************************************************/
public class Singleton<T>where T : class
{
    private static T _instance = null;
    public static T Instance => _instance;
    private static readonly object obj = "locker";
    public virtual void Init()
    {
        if(_instance == null)
        {
            lock (obj)
            {
                if(_instance == null)
                    _instance = this as T;
            }
        }
    }
}

