﻿/****************************************************
	文件：CacheSvc.cs
	作者：Ling
	邮箱: 1759147969@qq.com
	日期：2019/01/12 10:37   	
	功能：数据缓存层
*****************************************************/


using PEProtocal;
using System.Collections.Generic;

class CacheSvc
{
    private static CacheSvc _instance = null;
    public static CacheSvc Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = new CacheSvc();
            }
            return _instance;
        }
    }
    private DBMgr dbMgr;
    //字典，保存登录的账号，和其对应的session
    private Dictionary<string, ServerSession> onLineAcct = new Dictionary<string, ServerSession>();
    //保存玩家的数据
    private Dictionary<ServerSession, PlayerData> onLineSessioin = new Dictionary<ServerSession, PlayerData>();

    public void Init()
    {
        dbMgr = DBMgr.Instance;   
    }
    //判断该账户是否登录了
    public bool isAcctOnLine(string acct)
    {
        return onLineAcct.ContainsKey(acct);
    }
    //获得玩家的数据
    public PlayerData GetPlayerData(string acct,string pass)
    {
        //返回数据库查询的结构
        return dbMgr.QueryPlayerData(acct,pass);
    }
    //缓存登录的玩家信息
    public void AddAcctOnLine(string acct,ServerSession session,PlayerData playerData)
    {
        onLineAcct.Add(acct, session);
        onLineSessioin.Add(session, playerData);
    }
    //名字是否存在
    public bool IsNameExist(string name)
    {
        return dbMgr.QueryNameData(name);
    }

    //获得写入到PlayerData中的数据
    public PlayerData GetPlayerDataBySession(ServerSession session)
    {
        if(onLineSessioin.TryGetValue(session,out PlayerData playerData))
        {
            return playerData;
        }
        else
        {
            return null;
        }
    }

    //调用DBMgr中的更新数据库函数，来更新数据库数据
    public bool UpdatePlayerData(int id,PlayerData playerData)
    {
        //TODO
        return dbMgr.UpdatePlayerData(id,playerData);
    }

    //用户下线的清理数据
    public void RemoveAcctOutLine(ServerSession session)
    {
        foreach (var item in onLineAcct)
        {
            if(item.Value == session)
            {
                onLineAcct.Remove(item.Key);  //值相等，移除键值对
            }
        }
        //直接移除session
        onLineSessioin.Remove(session);
        PECommon.Log("Offline Result SessionID:" + session.sessionID + " sucess!");
    }

    public ServerSession GetOnLineServerSession(int id)
    {
        ServerSession session = null;
        foreach (var item in onLineSessioin)
        {
            if(item.Value.id == id)
            {
                session = item.Key;
                break;
            }
        }
        return session;
    }

    //得到所有在线用户的Session
    public List<ServerSession> GetOnLineAcctSession()
    {
        List<ServerSession> list = new List<ServerSession>();
        foreach (var item in onLineSessioin)
        {
            list.Add(item.Key);
        }
        return list;
    }

    public Dictionary<ServerSession,PlayerData> GetOnLineCache()
    {
        return onLineSessioin;
    }
}

