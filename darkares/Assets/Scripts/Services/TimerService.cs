/****************************************************
    文件：TimerService.cs
	作者：Ling
    邮箱: 1759147969@qq.com
    日期：2019/8/17 13:27:15
	功能：定时系统
*****************************************************/

using System;
using UnityEngine;

public class TimerService : SystemRoot 
{
	private static TimerService _instance;
	public static TimerService Instance
	{
		get { return _instance; }
	}

	private PETimer _peTimer;
	
	public void InitTimerSvc()
	{
		_instance = this;
		_peTimer = new PETimer();
		_peTimer.SetLog((string info) =>
		{
			PECommon.Log(info);
		});
		PECommon.Log("TimerSvc Init Done!");
	}

	private void Update()
	{
		_peTimer.Update();
	}

	//添加定时任务
	public int AddTimeTask(Action<int> callback,float delay,PETimeUnit unit = PETimeUnit.Millisecond,int count = 1)
	{
		return _peTimer.AddTimeTask(callback, delay, unit, count);
	}

	public double GetNowTime()
	{
		return _peTimer.GetUTCMilliseconds();
	}
}