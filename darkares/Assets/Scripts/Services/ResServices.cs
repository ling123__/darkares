/****************************************************
    文件：ResServices.cs
	作者：Ling
    邮箱: 1759147969@qq.com
    日期：2019/1/8 21:31:22
	功能：资源服务
*****************************************************/

using System;
using System.Collections.Generic;
using System.Xml;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ResServices : MonoBehaviour 
{
    /*单例模式*/
    private static ResServices _instance;
    public static ResServices Instance
    {
        get { return _instance; }
    }

    public void InitService()
    {
        _instance = this;

        InitRdNameCfg(PathDefind.RdName);
        InitMonsterCfg(PathDefind.MonsterCfg);
        InitMapCfg(PathDefind.Map);
        InitGuide(PathDefind.Guide);
        InitEquip(PathDefind.Equip);
        InitTaskCfg(PathDefind.TaskReawar);
        InitSkillCfg(PathDefind.Skill);
        InitSkillActionCfg(PathDefind.SkillAction);
        InitSkillMoveCfg(PathDefind.SkillMove);
        PECommon.Log("ResSvc Init Done...");
    }

    private Action proCB = null;   //委托
    public void AsyncLoadScence(string sceneName,Action loaded)  //回调函数，返回去调用前一个脚本的函数
    {
        GameRoot.Instance.loadingWind.SetWindState();  //显示进度条界面

        AsyncOperation sceneAsync = SceneManager.LoadSceneAsync(sceneName);  //得到异步加载的进度信息

        proCB = () =>
        {
            GameRoot.Instance.loadingWind.SetProgress(sceneAsync.progress);  //设置loadingWind界面上的进度值

            if (sceneAsync.progress == 1)   //如果=1，代表界面加载完成，需要隐藏加载界面
            {
                if(loaded != null)
                {
                    loaded();        //回调函数，可复用性高
                }
                proCB = null;
                sceneAsync = null;
                GameRoot.Instance.loadingWind.SetWindState(false);
            }
        };     
    }

    private void Update()
    {
        if(proCB != null)
        {
            proCB();
        }
    }

    //字典,保存音效的
    private Dictionary<string, AudioClip> auDic = new Dictionary<string, AudioClip>();
    public AudioClip LoadAudio(string path,bool cache = false)
    {
        AudioClip au = null;
        //TryGetValue函数的功能是如果有对应键的值，则out出去
        //否则返回假
        if (!auDic.TryGetValue(path,out au))
        {
            //加载资源，资源类型是AudioClip
            au = Resources.Load<AudioClip>(path);
            if (cache)   //如果需要缓存，则将其放入到字典中
            {
                auDic.Add(path, au);
            }
        }
        return au;
    }

    //加载图片
    private Dictionary<string, Sprite> spDic = new Dictionary<string, Sprite>();
    public Sprite LoadSprite(string path,bool cache = false)
    {
        Sprite sp = null;
        if(!spDic.TryGetValue(path,out sp))
        {
            sp = Resources.Load<Sprite>(path);
            //缓存
            if (cache)
            {
                spDic.Add(path, sp);
            }
        }
        return sp;
    }

    //该字典用来存储哪些需要保存的prefab
    private Dictionary<string, GameObject> goDic = new Dictionary<string, GameObject>();
    public GameObject LoadPrefab(string path,bool cache = false)
    {
        GameObject prefab = null;
        if(!goDic.TryGetValue(path,out prefab))
        {
            prefab = Resources.Load<GameObject>(path);
            //如果需要缓存，则保存下来
            if (cache)
            {
                goDic.Add(path, prefab);
            }
        }
        GameObject go = null;
        if(prefab != null)
        {
            go = Instantiate(prefab);
        }
        return go;
    }

    #region InitCfgs
    #region 随机名字
    private System.Collections.Generic.List<string> surNameList = new System.Collections.Generic.List<string>();  //存放姓氏
    private System.Collections.Generic.List<string> manNameList = new System.Collections.Generic.List<string>();  //存放man名字
    private System.Collections.Generic.List<string> womanNameList = new System.Collections.Generic.List<string>();  //存放woman名字

    public void InitRdNameCfg(string path)
    {
        TextAsset xml = Resources.Load<TextAsset>(path);
        if(xml == null)
        {
            PECommon.Log("xml file:" + path + "not exist");
        }
        else
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xml.text);

            //root取决于xml文件中的root名称
            XmlNodeList nodLst = doc.SelectSingleNode("root").ChildNodes;

            for(int i = 0;i < nodLst.Count; i++)
            {
                XmlElement ele = nodLst[i] as XmlElement;
                //如果不存在这个ID
                if(ele.GetAttributeNode("ID") == null)
                {
                    continue;
                }
                //获得文本中的ID数值
                int ID = Convert.ToInt32(ele.GetAttributeNode("ID").InnerText);
                //拿到id项中对应的子节点
                foreach (XmlElement e in nodLst[i].ChildNodes)
                {
                    //e.Name就是尖括号中的
                    switch (e.Name)
                    {
                        case "surname":
                            surNameList.Add(e.InnerText);   //增加其值
                            break;
                        case "man":
                            manNameList.Add(e.InnerText);
                            break;
                        case "woman":
                            womanNameList.Add(e.InnerText);
                            break;
                    }
                }
            }
        }
    }
    //获得随机姓名
    public string GetRdNameData(bool man = true)
    {
        string rdName = surNameList[PeTools.RdInt(0, surNameList.Count - 1)];
        if (man)
        {
            rdName += manNameList[PeTools.RdInt(0, manNameList.Count - 1)];
        }
        else
        {
            rdName += womanNameList[PeTools.RdInt(0, womanNameList.Count - 1)];
        }
        return rdName;
    }
    #endregion

    #region  地图
    //字典，用来存放地图的信息
    private Dictionary<int, MapCfg> mapCfgDataDic = new Dictionary<int, MapCfg>();
    public void InitMapCfg(string path)
    {
        TextAsset xml = Resources.Load<TextAsset>(path);
        if (xml == null)
        {
            PECommon.Log("xml file:" + path + "not exist");
        }
        else
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xml.text);

            //root取决于xml文件中的root名称
            XmlNodeList nodLst = doc.SelectSingleNode("root").ChildNodes;

            for (int i = 0; i < nodLst.Count; i++)
            {
                XmlElement ele = nodLst[i] as XmlElement;
                //如果不存在这个ID
                if (ele.GetAttributeNode("ID") == null)
                {
                    continue;
                }
                //获得文本中的ID数值
                int ID = Convert.ToInt32(ele.GetAttributeNode("ID").InnerText);
                MapCfg mc = new MapCfg
                {
                    ID = ID,
                    MonsterLst = new System.Collections.Generic.List<MonsterData>()
                };
                //拿到id项中对应的子节点
                foreach (XmlElement e in nodLst[i].ChildNodes)
                {
                    switch (e.Name)
                    {
                        case "mapName":
                            mc.MapName = e.InnerText;
                            break;
                        case "sceneName":
                            mc.MapName = e.InnerText;
                            break;
                        case "power":
                            mc.Power = int.Parse(e.InnerText);
                            break;
                        case "mainCamPos":
                        {
                            string[] arr = e.InnerText.Split(',');
                            mc.MainCamPos = new Vector3(float.Parse(arr[0]), float.Parse(arr[1]), float.Parse(arr[2]));
                        }
                            break;
                        case "mainCamRote":
                        {
                            string[] arr = e.InnerText.Split(',');
                            mc.MainCamRote = new Vector3(float.Parse(arr[0]), float.Parse(arr[1]), float.Parse(arr[2]));
                        }
                            break;
                        case "playerBornPos":
                        {
                            string[] arr = e.InnerText.Split(',');
                            mc.PlayerBornPos = new Vector3(float.Parse(arr[0]), float.Parse(arr[1]),
                                float.Parse(arr[2]));
                        }
                            break;
                        case "playerBornRote":
                        {
                            string[] arr = e.InnerText.Split(',');
                            mc.PlayerBornRote = new Vector3(float.Parse(arr[0]), float.Parse(arr[1]),
                                float.Parse(arr[2]));
                        }
                            break;
                        case "monsterLst":
                            string[] waveArr = e.InnerText.Split('#');
                            for (int wave = 0; wave < waveArr.Length; wave++)
                            {
                                if (waveArr[wave] != "")
                                {
                                    string[] tempArr = waveArr[wave].Split('|');
                                    for (int j = 0; j < tempArr.Length; j++)
                                    {
                                        if (tempArr[j] != "")
                                        {
                                            string[] arr = tempArr[j].Split(',');
                                            MonsterData md = new MonsterData
                                            {
                                                ID = int.Parse(arr[0]),
                                                MWave = wave,
                                                MIndex = j,
                                                MBornPos = new Vector3(float.Parse(arr[1]), float.Parse(arr[2]),
                                                    float.Parse(arr[3])),
                                                MBornRot = new Vector3(0, float.Parse(arr[4]), 0),
                                                MonsterCfg = GetMonsterCfg(int.Parse(arr[0])),
                                                MLevel = int.Parse(arr[5])
                                            };
                                            mc.MonsterLst.Add(md);
                                        }
                                    }
                                }
                            }

                            break;
                    }
                }

                mapCfgDataDic.Add(ID, mc);  //将id号和对应的数据存放到字典中
            }
        }
        PECommon.Log("MapCfg Init Donw...");
    }
    
    //获取地图配置数据
    public MapCfg GetMapCfg(int id)
    {
        MapCfg data;
        //如果有相应的地图数据，则返回，否则返回空
        if(mapCfgDataDic.TryGetValue(id,out data))
        {
            return data;
        }
        return null;
    }
    #endregion

    #region 自动引导配置
    private Dictionary<int, AutoGuideCfg> _guideCfgDataDic = new Dictionary<int, AutoGuideCfg>();
    private void InitGuide(string path)
    {
        TextAsset xml = Resources.Load<TextAsset>(path);
        if (xml == null)
        {
            PECommon.Log("xml file:" + path + "not exist");
        }
        else
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xml.text);

            //root取决于xml文件中的root名称
            XmlNodeList nodLst = doc.SelectSingleNode("root").ChildNodes;

            for (int i = 0; i < nodLst.Count; i++)
            {
                XmlElement ele = nodLst[i] as XmlElement;
                //如果不存在这个ID
                if (ele.GetAttributeNode("ID") == null)
                {
                    continue;
                }
                //获得文本中的ID数值
                int ID = Convert.ToInt32(ele.GetAttributeNode("ID").InnerText);
                AutoGuideCfg mc = new AutoGuideCfg
                {
                    ID = ID
                };
                //拿到id项中对应的子节点
                foreach (XmlElement e in nodLst[i].ChildNodes)
                {
                    switch (e.Name)
                    {
                        case "npcID":
                            mc.NpcId = int.Parse(e.InnerText);
                            break;
                        case "dilogArr":
                            mc.DilogArr = e.InnerText;
                            break;
                        case "actID":
                            mc.ActId = int.Parse(e.InnerText);
                            break;
                        case "coin":
                            mc.Coin = int.Parse(e.InnerText);
                            break;
                        case "exp":
                            mc.Exp = int.Parse(e.InnerText);
                            break;
                    }
                }
                _guideCfgDataDic.Add(ID, mc);  //将id号和对应的数据存放到字典中
            }
        }
        PECommon.Log("GuideCfg Init Done...");
    }

    public AutoGuideCfg GetAutoGuideData(int id)
    {
        AutoGuideCfg agc = null;
        if(_guideCfgDataDic.TryGetValue(id,out agc))
        {
            return agc;
        }
        return null;
    }
    #endregion

    #region 装备升级信息

    private Dictionary<int,Dictionary<int,EquipCfg>> _equipDict = new Dictionary<int, Dictionary<int,EquipCfg>>();
    private void InitEquip(string path)
    {
        TextAsset xml = Resources.Load<TextAsset>(path);
        if (xml == null)
        {
            PECommon.Log("xml file:" + path + "not exist");
        }
        else
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xml.text);

            //root取决于xml文件中的root名称
            XmlNodeList nodLst = doc.SelectSingleNode("root").ChildNodes;

            for (int i = 0; i < nodLst.Count; i++)
            {
                XmlElement ele = nodLst[i] as XmlElement;
                //如果不存在这个ID
                if (ele.GetAttributeNode("ID") == null)
                {
                    continue;
                }
                //获得文本中的ID数值
                int ID = Convert.ToInt32(ele.GetAttributeNode("ID").InnerText);
                EquipCfg equipCfg = new EquipCfg
                {
                    ID = ID
                };
                //拿到id项中对应的子节点
                foreach (XmlElement e in nodLst[i].ChildNodes)
                {
                    int val = int.Parse(e.InnerText);
                    switch (e.Name)
                    {
                        case "pos":
                            equipCfg.Pos = val;
                            break;
                        case "starlv":
                            equipCfg.StartLv = val;
                            break;
                        case "addhp":
                            equipCfg.AddHp = val;
                            break;
                        case "addhurt":
                            equipCfg.AddHurt = val;
                            break;
                        case "adddef":
                            equipCfg.AddDef = val;
                            break;
                        case "minlv":
                            equipCfg.MinLv = val;
                            break;
                        case "coin":
                            equipCfg.Coin = val;
                            break;
                        case "crystal":
                            equipCfg.Crystal = val;
                            break;
                    }
                }
                //因为每个武器有不同的星级，所以每个pos下保存九个字典
                Dictionary<int, EquipCfg> dict = null;
                if (_equipDict.TryGetValue(equipCfg.Pos, out dict))
                {
                    dict.Add(equipCfg.StartLv,equipCfg);
                }
                else
                {
                    dict = new Dictionary<int, EquipCfg>();
                    dict.Add(equipCfg.StartLv,equipCfg);
                    _equipDict.Add(equipCfg.Pos,dict);
                }
            }
        }
        PECommon.Log("EquipCfg Init Done...");
    }
    
    //根据位置和星级得到
    public EquipCfg GetEquipData(int pos, int startlv)
    {
        EquipCfg equipCfg = null;
        Dictionary<int, EquipCfg> dict = null;
        if (_equipDict.TryGetValue(pos, out dict))
        {
            if (dict.ContainsKey(startlv))
            {
                equipCfg = dict[startlv];
            }
        }
        return equipCfg;
    }

    public int GetStarAllProp(int pos, int starLv, int type)
    {
        Dictionary<int, EquipCfg> equipCfgs = null;
        int value = 0;
        if (_equipDict.TryGetValue(pos, out equipCfgs))
        {
            //遍历当前所有的星级
            for (int i = 0; i < starLv;i++)
            {
                EquipCfg equipCfg = null;
                if (equipCfgs.TryGetValue(i, out equipCfg))
                {
                    //按照类型累加
                    switch (type)
                    {
                        case 0:  //生命
                            value += equipCfg.AddHp;
                            break;
                        case 1:  //伤害
                            value += equipCfg.AddHurt;
                            break;
                        case 2:  //放于
                            value += equipCfg.AddDef;
                            break;
                    }
                }
            }
        }
        return value;
    }

    #endregion
    
    #region     任务配置文件
    private Dictionary<int, TaskRewardCfg> _taskCfg = new Dictionary<int, TaskRewardCfg>();
    private void InitTaskCfg(string path)
    {
        TextAsset xml = Resources.Load<TextAsset>(path);
        if (xml == null)
        {
            PECommon.Log("xml file:" + path + "not exist");
        }
        else
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xml.text);

            XmlNodeList nodList = doc.SelectSingleNode("root").ChildNodes;

            for (int i = 0; i < nodList.Count; i++)
            {
                XmlElement ele = nodList[i] as XmlElement; //得到根目录节点之后再一次获取里面的数值
                //如果不存在这个ID
                if (ele.GetAttributeNode("ID") == null)
                {
                    continue;
                }

                int ID = Convert.ToInt32(ele.GetAttributeNode("ID").InnerText);
                TaskRewardCfg trc = new TaskRewardCfg
                {
                    ID = ID,
                };
                foreach (XmlElement e in nodList[i].ChildNodes)
                {
                    switch (e.Name)
                    {
                        case "taskName":
                            trc.TaskName = e.InnerText;
                            break;
                        case "count":
                            trc.Count = int.Parse(e.InnerText);
                            break;
                        case "exp":
                            trc.Exp = int.Parse(e.InnerText);
                            break;
                        case "coin":
                            trc.Coin = int.Parse(e.InnerText);
                            break;
                    }
                }
                _taskCfg.Add(ID, trc);
            }
        }
        PECommon.Log("TaskCfg Init Done");
    }
    public TaskRewardCfg GetTaskCfg(int id)
    {
        TaskRewardCfg trc = null;
        if(_taskCfg.TryGetValue(id,out trc))
        {
            return trc;
        }
        return null;
    }
    #endregion
    
    #region     技能配置文件
    private Dictionary<int, SkillCfg> _skillCfg = new Dictionary<int, SkillCfg>();
    private void InitSkillCfg(string path)
    {
        TextAsset xml = Resources.Load<TextAsset>(path);
        if (xml == null)
        {
            PECommon.Log("xml file:" + path + "not exist");
        }
        else
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xml.text);

            XmlNodeList nodList = doc.SelectSingleNode("root").ChildNodes;

            for (int i = 0; i < nodList.Count; i++)
            {
                XmlElement ele = nodList[i] as XmlElement; //得到根目录节点之后再一次获取里面的数值
                //如果不存在这个ID
                if (ele.GetAttributeNode("ID") == null)
                {
                    continue;
                }

                int id = Convert.ToInt32(ele.GetAttributeNode("ID").InnerText);
                SkillCfg sk = new SkillCfg
                {
                    ID = id,
                    SkillMoveLst = new List<int>(),
                    SkillActionLst = new List<int>(),
                    SkillDamageLst = new List<int>()
                };
                foreach (XmlElement e in nodList[i].ChildNodes)
                {
                    switch (e.Name)
                    {
                        case "skillName":
                            sk.SkillName = e.InnerText;
                            break;
                        case "cdTime":
                            sk.CdTime = int.Parse(e.InnerText);
                            break;
                        case "skillTime":
                            sk.SkillTime = int.Parse(e.InnerText);
                            break;
                        case "aniAction":
                            sk.AniAction = int.Parse(e.InnerText);
                            break;
                        case "fx":
                            sk.SkillAni = e.InnerText;
                            break;
                        case "isCombo":
                            sk.IsCombo = e.InnerText.Equals("1");
                            break;
                        case "dmgType":
                            if (e.InnerText.Equals("1"))
                                sk.DmgType = DmgType.Ad;
                            else if(e.InnerText.Equals("2"))
                                sk.DmgType = DmgType.Ap;
                            break;
                        case "skillMoveLst":
                            string[] skillMove = e.InnerText.Split('|');
                            for (int j = 0; j < skillMove.Length; j++)
                            {
                                if (skillMove[j] != "")
                                {
                                    sk.SkillMoveLst.Add(int.Parse(skillMove[j]));
                                }
                            }
                            break;
                        case "skillActionLst":
                            string[] skillAction = e.InnerText.Split('|');
                            for (int j = 0; j < skillAction.Length; j++)
                            {
                                if (skillAction[j] != "")
                                {
                                    sk.SkillActionLst.Add(int.Parse(skillAction[j]));
                                }
                            }
                            break;
                        case "skillDamageLst":
                            string[] skillDamage = e.InnerText.Split('|');
                            for (int j = 0; j < skillDamage.Length; j++)
                            {
                                if (skillDamage[j] != "")
                                {
                                    sk.SkillDamageLst.Add(int.Parse(skillDamage[j]));
                                }
                            }
                            break;
                    }
                }
                _skillCfg.Add(id, sk);
            }
        }
        PECommon.Log("SkillCfg Init Done");
    }
    public SkillCfg GetSkillCfg(int id)
    {
        SkillCfg sk = null;
        if(_skillCfg.TryGetValue(id,out sk))
        {
            return sk;
        }
        return null;
    }
    #endregion
    
    #region     技能动作配置文件
    private Dictionary<int, SKillActionCfg> _skillActionCfg = new Dictionary<int, SKillActionCfg>();
    private void InitSkillActionCfg(string path)
    {
        TextAsset xml = Resources.Load<TextAsset>(path);
        if (xml == null)
        {
            PECommon.Log("xml file:" + path + "not exist");
        }
        else
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xml.text);

            XmlNodeList nodList = doc.SelectSingleNode("root").ChildNodes;

            for (int i = 0; i < nodList.Count; i++)
            {
                XmlElement ele = nodList[i] as XmlElement; //得到根目录节点之后再一次获取里面的数值
                //如果不存在这个ID
                if (ele.GetAttributeNode("ID") == null)
                {
                    continue;
                }

                int id = Convert.ToInt32(ele.GetAttributeNode("ID").InnerText);
                SKillActionCfg sac = new SKillActionCfg
                {
                    ID = id,
                };
                foreach (XmlElement e in nodList[i].ChildNodes)
                {
                    switch (e.Name)
                    {
                        case "delayTime":
                            sac.DelayTime = int.Parse(e.InnerText);
                            break;
                        case "radius":
                            sac.Radius = float.Parse(e.InnerText);
                            break;
                        case "angle":
                            sac.Angle = int.Parse(e.InnerText);
                            break;
                    }
                }
                _skillActionCfg.Add(id, sac);
            }
        }
        PECommon.Log("SkillActionCfg Init Done");
    }
    public SKillActionCfg GetSkillActionCfg(int id)
    {
        SKillActionCfg sk = null;
        if(_skillActionCfg.TryGetValue(id,out sk))
        {
            return sk;
        }
        return null;
    }
    #endregion
    
    #region     技能位移距离配置文件
    private Dictionary<int, SkillMoveCfg> _skillMoveCfg = new Dictionary<int, SkillMoveCfg>();
    private void InitSkillMoveCfg(string path)
    {
        TextAsset xml = Resources.Load<TextAsset>(path);
        if (xml == null)
        {
            PECommon.Log("xml file:" + path + "not exist");
        }
        else
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xml.text);

            XmlNodeList nodList = doc.SelectSingleNode("root").ChildNodes;

            for (int i = 0; i < nodList.Count; i++)
            {
                XmlElement ele = nodList[i] as XmlElement; //得到根目录节点之后再一次获取里面的数值
                //如果不存在这个ID
                if (ele.GetAttributeNode("ID") == null)
                {
                    continue;
                }

                int ID = Convert.ToInt32(ele.GetAttributeNode("ID").InnerText);
                SkillMoveCfg skm = new SkillMoveCfg
                {
                    ID = ID,
                };
                foreach (XmlElement e in nodList[i].ChildNodes)
                {
                    switch (e.Name)
                    {
                        case "delayTime":
                            skm.DelayTime = int.Parse(e.InnerText);
                            break;
                        case "moveTime":
                            skm.MoveTime = int.Parse(e.InnerText);
                            break;
                        case "moveDis":
                            skm.MoveDis = float.Parse(e.InnerText);
                            break;
                    }
                }
                _skillMoveCfg.Add(ID, skm);
            }
        }
        PECommon.Log("SkillMoveCfg Init Done");
    }
    public SkillMoveCfg GetSkillMoveCfg(int id)
    {
        SkillMoveCfg sk = null;
        if(_skillMoveCfg.TryGetValue(id,out sk))
        {
            return sk;
        }
        return null;
    }
    #endregion
    
    #region  敌人
    //字典，用来存放地图的信息
    private Dictionary<int, MonsterCfg> monsterCfgDic = new Dictionary<int, MonsterCfg>();
    public void InitMonsterCfg(string path)
    {
        TextAsset xml = Resources.Load<TextAsset>(path);
        if (xml == null)
        {
            PECommon.Log("xml file:" + path + "not exist");
        }
        else
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xml.text);

            //root取决于xml文件中的root名称
            XmlNodeList nodLst = doc.SelectSingleNode("root").ChildNodes;

            for (int i = 0; i < nodLst.Count; i++)
            {
                XmlElement ele = nodLst[i] as XmlElement;
                //如果不存在这个ID
                if (ele.GetAttributeNode("ID") == null)
                {
                    continue;
                }
                //获得文本中的ID数值
                int id = Convert.ToInt32(ele.GetAttributeNode("ID").InnerText);
                MonsterCfg mc = new MonsterCfg
                {
                    ID = id,
                    MProps = new BattleProps()
                };
                //拿到id项中对应的子节点
                foreach (XmlElement e in nodLst[i].ChildNodes)
                {
                    switch (e.Name)
                    {
                        case "mName":
                            mc.MName = e.InnerText;
                            break;
                        case "resPath":
                            mc.ResPath = e.InnerText;
                            break;
                        case "skillID":
                            mc.SkillId = int.Parse(e.InnerText);
                            break;
                        case "atkDis":
                            mc.AtkDis = float.Parse(e.InnerText);
                            break;
                        case "hp":
                            mc.MProps.Hp = int.Parse(e.InnerText);
                            break;
                        case "ad":
                            mc.MProps.Ad = int.Parse(e.InnerText);
                            break;
                        case "ap":
                            mc.MProps.Ap = int.Parse(e.InnerText);
                            break;
                        case "addef":
                            mc.MProps.Addef = int.Parse(e.InnerText);
                            break;
                        case "apdef":
                            mc.MProps.Apdef = int.Parse(e.InnerText);
                            break;
                        case "dodge":
                            mc.MProps.Dodge = int.Parse(e.InnerText);
                            break;
                        case "pierce":
                            mc.MProps.Pierce = int.Parse(e.InnerText);
                            break;
                        case "critical":
                            mc.MProps.Critical = int.Parse(e.InnerText);
                            break;
                    }
                }
                monsterCfgDic.Add(id, mc);  //将id号和对应的数据存放到字典中
            }
        }
        PECommon.Log("MonsterCfg Init Donw...");
    }
    
    //获取地图配置数据
    public MonsterCfg GetMonsterCfg(int id)
    {
        MonsterCfg data;
        //如果有相应的地图数据，则返回，否则返回空
        if(monsterCfgDic.TryGetValue(id,out data))
        {
            return data;
        }
        return null;
    }
    #endregion

    #endregion
}