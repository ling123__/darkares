/****************************************************
    文件：AudioServices.cs
	作者：Ling
    邮箱: 1759147969@qq.com
    日期：2019/1/9 14:57:10
	功能：音效模块
*****************************************************/

using UnityEngine;

public class AudioServices : MonoBehaviour 
{
    private static AudioServices _instance;
    public static AudioServices Instance
    {
        get { return _instance; }
    }

    public AudioSource bgAudio;
    public AudioSource uiAudio;

    public void InitAudioSvc()
    {
        _instance = this;
        PECommon.Log("AudioSvc Init Donw...");
    }

    /*播放背景音乐*/
    public void PlayBgAudio(string name,bool isLoop = true)
    {
        AudioClip audio = ResServices.Instance.LoadAudio("ResAudio/" + name,true);
        if(bgAudio == null 
            || bgAudio.name != audio.name)
        {
            bgAudio.clip = audio;
            bgAudio.loop = isLoop;
            bgAudio.Play();
        }
    }
    /*播放ui音乐*/
    public void PlayeUiAudio(string name)
    {
        AudioClip audio = ResServices.Instance.LoadAudio("ResAudio/" + name);
        uiAudio.clip = audio;
        uiAudio.Play();
    }
}