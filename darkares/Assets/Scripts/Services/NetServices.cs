/****************************************************
    文件：NetServices.cs
	作者：Ling
    邮箱: 1759147969@qq.com
    日期：2019/1/11 15:28:58
	功能：网络服务
*****************************************************/

using UnityEngine;
using PENet;
using PEProtocal;
using System.Collections.Generic;

public class NetServices : MonoBehaviour 
{
    private static NetServices _instance;
    public static NetServices Instance
    {
        get { return _instance; }
    }
    PESocket<ClientSession, GameMsg> client = null;

    public static readonly string obj = "lock";
    private Queue<GameMsg> msgQueue = new Queue<GameMsg>();


    public void InitSvc()
    {
        _instance = this;
        client = new PESocket<ClientSession, GameMsg>();
       
        client.SetLog(true,(string msg,int lv) =>{
            switch(lv){
                case 0:
                    msg = "Log:" + msg;
                    Debug.Log(msg);
                    break;
                case 1:
                    msg = "Warn:" + msg;
                    Debug.LogWarning(msg);
                    break;
                case 2:
                    msg = "Error:" + msg;
                    Debug.LogError(msg);
                    break;
                case 3:
                    msg = "Info:" + msg;
                    Debug.Log(msg);
                    break;
            }
        });
        client.StartAsClient(SrvCfg.srvIP, SrvCfg.srvPort);  //启动
        PECommon.Log("NetSvc Init Done...");
    }

    public void SendMsg(GameMsg msg)
    {
        if(client.session != null){
            client.session.SendMsg(msg);
        }
        else
        {
            GameRoot.AddTips("服务器未连接");
            InitSvc();   //重新连接
        }
    }

    public void AddNetMsg(GameMsg msg)
    {
        lock (obj)
        {
            msgQueue.Enqueue(msg);
        }
    }

    public void Update()
    {
       if(msgQueue.Count > 0)
        {
            lock (obj)
            {
                GameMsg msg = msgQueue.Dequeue();
                ProcessMsg(msg);   //对数据进行处理
                
            }
        }
    }

    //消息分发
    public void ProcessMsg(GameMsg msg)
    {
        if(msg.err != (int)Error.None)
        {
            switch ((Error)msg.err)
            {
                case Error.AcctIsOnline:
                    GameRoot.AddTips("账号已经在线");
                    break;
                case Error.WrongPass:
                    GameRoot.AddTips("密码错误");
                    break;
                case Error.UpdateDBError:  //啧啧
                    PECommon.Log("数据库更新失败", LogType.Error);
                    GameRoot.AddTips("当前网络不稳定");
                    break;
                case Error.ServerDataError:
                    PECommon.Log("服务器客户端数据匹配",LogType.Error);
                    GameRoot.AddTips("客户端数据错误");
                    break;     
                case Error.LockStar:
                    GameRoot.AddTips("星级已满");
                    break;
                case Error.LockLv:
                    GameRoot.AddTips("等级不够");
                    break;
                case Error.LockCoin:
                    GameRoot.AddTips("金币不够");
                    break;
                case Error.LockCrystal:
                    GameRoot.AddTips("水晶不够");
                    break;
                case Error.LockDiamond:
                    GameRoot.AddTips("钻石不够");
                    break;
                case Error.LockPower:
                    GameRoot.AddTips("体力不足");
                    break;
                case Error.DungeonId:
                    GameRoot.AddTips("关卡未解锁");
                    break;
            }
            return;
        }
        //处理服务器发过来的数据逻辑
        switch ((CMD)msg.cmd)
        {
            case CMD.RspLogin:
                LogSys.Instance.RspLogin(msg);
                break;
            case CMD.RspRename:
                LogSys.Instance.RspRename(msg);
                break;
            case CMD.RspGuide:
                MainCitySys.Instance.RspGuide(msg);
                break;
            case CMD.RspEquip:
                MainCitySys.Instance.RspEquip(msg);
                break;
            case CMD.PushChat:
                MainCitySys.Instance.PushChat(msg);
                break;
            case CMD.RspBuy:
                MainCitySys.Instance.RspBuy(msg);
                break;
           case CMD.RspPower:
               MainCitySys.Instance.RspPower(msg);
               break;
           case CMD.RspTask:
               MainCitySys.Instance.RspRTask(msg);
               break;
           case CMD.PshTaskPro:
               MainCitySys.Instance.PshTaskPro(msg);
               break;
           case CMD.RspDungeon:
               DungeonSys.Instance.RspDungeon(msg);
               break;
        }
    }
}