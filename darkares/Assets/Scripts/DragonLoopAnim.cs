/****************************************************
    文件：DragonLoopAnim.cs
	作者：Ling
    邮箱: 1759147969@qq.com
    日期：2019/1/9 16:9:45
	功能：飞龙的动画，循环
*****************************************************/

using UnityEngine;

public class DragonLoopAnim : MonoBehaviour 
{
    public Animation anim;

    private void Awake()
    {
        anim = GetComponent<Animation>();
        if (anim != null)
        {
            InvokeRepeating("PlayDragonAnim", 0, 20);
        }
    }

    private void PlayDragonAnim()
    {
        anim.Play();
    }

}