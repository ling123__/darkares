/****************************************************
    文件：SystemRoot.cs
	作者：Ling
    邮箱: 1759147969@qq.com
    日期：2019/1/9 15:30:15
	功能：业务服务基类
*****************************************************/

using UnityEngine;

public class SystemRoot : MonoBehaviour 
{
    protected ResServices resSvc;
    protected AudioServices audioSvc;
    protected NetServices netSvc;

    public virtual void InitSys()
    {
        resSvc = ResServices.Instance;
        audioSvc = AudioServices.Instance;
        netSvc = NetServices.Instance;
    }
}