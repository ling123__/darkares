/****************************************************
    文件：MainCitySys.cs
	作者：Ling
    邮箱: 1759147969@qq.com
    日期：2019/1/13 12:59:53
	功能：主城业务系统
*****************************************************/

using PEProtocal;
using UnityEngine;
using UnityEngine.AI;

public class MainCitySys : SystemRoot 
{
    private static MainCitySys _instance;
    public static MainCitySys Instance
    {
        get { return _instance; }
    }

    public MainCityWind mainCityWind;   //主城界面
    public InfoWind infoWind;           //人物属性
    public StrongWind strongWind;       //强化界面
    public GuideWind guideWind;         //导航
    public ChatWind ChatWind;           //对话框
    public BuyWind BuyWind;             //购物窗口
    public TaskWind TaskWind;           //任务窗口
    public PlayerCtrlWind PlayerCtrlWind; //战斗窗口
    
    private Transform charShowCam;        //拍摄角色的照相机
    private AutoGuideCfg curTaskData;     //任务数据
    private GameObject player;

    private PlayerController playerCtrl;
    private Transform[] NpcTrans;
    private NavMeshAgent nav;
    private bool isNav = false;

    public override void InitSys()
    {
        base.InitSys();

        _instance = this;
        PECommon.Log("MainCitySys Init Done...");
    }

    public void EnterMainCity()
    {
        //加载主城的场景,并打开场景UI
        MapCfg mapData = resSvc.GetMapCfg(Constants.MainCityMapId);
        resSvc.AsyncLoadScence(mapData.MapName, () =>
        {
            //加载游戏主角
            LoadPlayer(mapData);
            //加载界面
            mainCityWind.SetWindState();

            //加载场景音乐
            audioSvc.PlayBgAudio(Constants.BgMainCity);

            /*获得保存地图NPC位置信息的数组*/
            GameObject map = GameObject.FindGameObjectWithTag("MapRoot");
            NpcTrans = map.GetComponent<MainCityMap>().NpcTrans;

            //设置相关的配置，例如摄像机。。。
            if (charShowCam != null)
            {
                charShowCam.gameObject.SetActive(false);  //不显示
            }
        }); 
    }

    //初始化人物
    private void LoadPlayer(MapCfg mapData)
    {
        player = resSvc.LoadPrefab(PathDefind.AssissnCityPlayerPrefab);  //人物资源加载
        player.transform.position = mapData.PlayerBornPos;  //人物位置设置
        player.transform.localEulerAngles = mapData.PlayerBornRote;  //人物旋转设置
        player.transform.localScale = new Vector3(1,1,1);

        //相机初始化
        Camera.main.transform.position = mapData.MainCamPos;
        Camera.main.transform.localEulerAngles = mapData.MainCamRote;

        //获得角色身上的导航组件
        nav = GameObject.FindGameObjectWithTag("Player").GetComponent<NavMeshAgent>();

        playerCtrl = player.GetComponent<PlayerController>();
        playerCtrl.Init();
    }

    public void SetMoveDir(Vector2 dir)
    {
        StopNavGuide();    //玩家控制角色时，关闭导航
        //设置角色的动画
        if (dir == Vector2.zero)
        {
            playerCtrl.SetBlend(Constants.BlendIdle);
        }
        else
        {
            playerCtrl.SetBlend(Constants.BlendMove);
        }
        playerCtrl.Dir = dir;   //设置方向
    }

    private float startRotate = 0;  //起始旋转角度
    public void SetStartRotate()
    {
        startRotate = playerCtrl.transform.localEulerAngles.y;
    }
    //控制人物旋转
    public void SetPlayerRotate(float rotate)
    {
        playerCtrl.transform.localEulerAngles = new Vector3(0, startRotate + rotate, 0);
    }
    //使人物旋转位置还原
    public void SetPlayerInit()
    {
        playerCtrl.transform.localEulerAngles = new Vector3(0, startRotate, 0);
    }

    //执行任务
    public void RunTask(AutoGuideCfg agc)
    {
        if(agc != null){
            curTaskData = agc;
        }

        nav.enabled = true;  //只要有任务就激活
        //解析任务数据
        if(curTaskData.NpcId != -1)
        {
            float dis = Vector3.Distance(playerCtrl.transform.position, NpcTrans[agc.NpcId].position);
            if(dis < 0.5f)
            {
                isNav = false;   
                playerCtrl.SetBlend(Constants.BlendIdle);
                nav.isStopped = true;
                nav.enabled = false;
                OpenGuideWnd();
            }
            else
            {
                isNav = true;
                nav.enabled = true;
                nav.speed = Constants.PlayerMoveSpeed;
                nav.SetDestination(NpcTrans[agc.NpcId].position);
                playerCtrl.SetBlend(Constants.BlendMove);
            }
        }
        else
        {
            OpenGuideWnd();
        }
    }
    private void Update()
    {
        if (isNav)
        {
            isArriveNavPos();
            playerCtrl.SetCamera();  //设置跟随相机的位置
        }
    }

    //判断角色是否到达了目标点
    private void isArriveNavPos()
    {
        float dis = Vector3.Distance(playerCtrl.transform.position, NpcTrans[curTaskData.NpcId].position);
        if(dis < 0.5f)
        {
            isNav = false;
            playerCtrl.SetBlend(Constants.BlendIdle);
            nav.isStopped = true;
            nav.enabled = false;
            OpenGuideWnd();
        }
    }

    //停止导航
    private void StopNavGuide()
    {
        if (isNav)
        {
            isNav = false;
            nav.isStopped = true;
            nav.enabled = false;
            playerCtrl.SetBlend(Constants.BlendIdle);
        }
    }

    #region OpenWind

    //打开装备强化界面
    public void OpenStrongWind()
    {
        StopNavGuide();
        strongWind.SetWindState();
    }
    //打开导航窗口
    private void OpenGuideWnd()
    {
        guideWind.SetWindState();  //激活对话框
    }
    //打开聊天窗口
    public void OpenChatWind()
    {
        StopNavGuide();
        ChatWind.SetWindState(true);
    }
    //打开购物窗口
    public void OpenBuyWind(int type)
    {
        StopNavGuide();
        BuyWind.SetBuyType(type);
        BuyWind.SetWindState(true);
    }
    //打开任务窗口
    public void OpenTaskWind()
    {
        StopNavGuide();
        TaskWind.SetWindState(true);
    }

    public void SetPlayerCtrlWindState(bool active = true)
    {
        PlayerCtrlWind.SetWindState(active);
    }
    
    //打开角色属性面板
    public void OpenInfoWind()
    {
        StopNavGuide();   //玩家打开面板时停止导航。其实不停止比较友好
        if (charShowCam == null)
        {
            charShowCam = GameObject.FindGameObjectWithTag("CharShow").transform;
        }
        //设置展示相机相对于人物的位置
        charShowCam.localPosition = playerCtrl.transform.position + playerCtrl.transform.forward * 2.8f + new Vector3(0, 1.2f, 0);
        charShowCam.localEulerAngles = new Vector3(0, 180f + playerCtrl.transform.localEulerAngles.y, 0);
        charShowCam.gameObject.SetActive(true);
        infoWind.SetWindState();
    }

    public void EnterDungeonSys()
    {
        StopNavGuide();
        DungeonSys.Instance.EnterDungeonSys();
    }

    //关闭info窗口，摄像机也需要隐藏
    public void CloseInfoWind()
    {
        if(charShowCam != null)
        {
            SetPlayerInit();  //设置人物旋转角度还原
            charShowCam.gameObject.SetActive(false);
            infoWind.SetWindState(false);
        }
    }

    #endregion

    public AutoGuideCfg GetCurtTaskData()
    {
        return curTaskData;
    }
    public void RspGuide(GameMsg msg)
    {
        RspGuide data = msg.rspGuide;
        GameRoot.AddTips(Constants.color("任务奖励金币" + curTaskData.Coin + " 任务奖励经验" + curTaskData.Exp, ColorText.Blue));
        //判断下一个行为
        switch (curTaskData.ActId)
        {
            case 0:
                //和智者对话
                break;
            case 1:
                //进入副本
                EnterDungeonSys();
                break;
            case 2:
                //强化界面
                OpenStrongWind();
                break;
            case 3:
                //体力界面
                OpenBuyWind(0);
                break;
            case 4:
                //金币界面
                OpenBuyWind(1);
                break;
            case 5:
                //世界聊天
                OpenChatWind();
                break;
        }
        GameRoot.Instance.UpdatePlayerDataByGuide(data);  //更新用户数据
        mainCityWind.RefreshUI();   //更新面板上的用户数据
    }
    
    //相应升级操作
    public void RspEquip(GameMsg msg)
    {
        int fightingAgo = PECommon.GetFightByProps(GameRoot.Instance.playerData);
        GameRoot.Instance.UpdatePlayerDataByEquip(msg.rspEquip);
        int fightingNow = PECommon.GetFightByProps(GameRoot.Instance.playerData);
        GameRoot.AddTips(Constants.color("战力提升"+(fightingNow - fightingAgo),ColorText.Green));
        strongWind.UpdateUI();
        mainCityWind.RefreshUI();
    }
    
    //聊天信息相关操作
    public void PushChat(GameMsg msg)
    {
        ChatWind.PushMsg(msg);
    }
    
    //购买商品相关
    public void RspBuy(GameMsg msg)
    {
        BuyWind.RspBuy(msg);

        if (msg.pshTaskPro != null)
        {
            PshTaskPro(msg);
        }
        mainCityWind.RefreshUI();
    }
    
    //体力恢复相关
    public void RspPower(GameMsg msg)
    {
        GameRoot.Instance.UpdatePower(msg.rspPower.power);
        if(mainCityWind.gameObject.activeSelf)
            mainCityWind.RefreshUI();
    }
    
    //任务奖励相关
    public void RspRTask(GameMsg msg)
    {
        GameRoot.Instance.UpdateExpAndCoin(msg.rspTask);
        if(TaskWind.gameObject.activeSelf)
            TaskWind.RefreshUi();
        mainCityWind.RefreshUI();
    }
    //任务进度推送
    public void PshTaskPro(GameMsg msg)
    {
        GameRoot.Instance.UpdateTaskPro(msg.pshTaskPro);
        if(TaskWind.gameObject.activeSelf)
            TaskWind.RefreshUi();
    }
}