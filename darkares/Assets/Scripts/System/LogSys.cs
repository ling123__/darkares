/****************************************************
    文件：LogSys.cs
	作者：Ling
    邮箱: 1759147969@qq.com
    日期：2019/1/8 21:37:22
	功能：登陆模块
*****************************************************/

using PEProtocal;
using UnityEngine;

public class LogSys : SystemRoot 
{

    private static LogSys _instance;
    public static LogSys Instance
    {
        get { return _instance; }
    }

    public LoginWind loginWind;
    public CreateWnd createWnd;


    public override void InitSys()
    {
        base.InitSys();

        _instance = this;
        PECommon.Log("LogSys Init Done...");
    }

    /*进入登陆场景*/
    public void EnterLoading()
    {
        //异步加载登陆场景，并显示加载进度，并在之后打开登陆界面
        resSvc.AsyncLoadScence(Constants.sceneLogin,
            () =>
            {
                //进入登陆界面
                loginWind.SetWindState();
                audioSvc.PlayBgAudio(Constants.BgLogin);
                GameRoot.AddTips("场景加载完成!");
            });
    }

    public void RspLogin(GameMsg msg)
    {
        GameRoot.AddTips("登陆成功");

        GameRoot.Instance.SetPlayerData(msg.rspLogin);  //客户端得到玩家数据，并且保存

        //如果玩家数据是空的，则跳转到角色创建面板
        if(msg.rspLogin.playerData.name == "")
        {
            createWnd.SetWindState();
        }
        else
        {
            //进入主城
            MainCitySys.Instance.EnterMainCity();
        }

        //关闭登录界面
        loginWind.SetWindState(false);
    }

    public void RspRename(GameMsg msg)
    {
        GameRoot.Instance.SetPlayerName(msg.rspRename.name);

        //跳转场景进入主城

        //打开主城的界面
        MainCitySys.Instance.EnterMainCity();
        //关闭创建界面
        createWnd.SetWindState(false);
    }
}