/****************************************************
    文件：TransScriptSys.cs
	作者：Ling
    邮箱: 1759147969@qq.com
    日期：2019/8/22 12:39:16
	功能：副本系统
*****************************************************/

using PEProtocal;
using UnityEngine;

public class DungeonSys : SystemRoot
{
	public DunGeonWind DunGeonWind;
	
	private static DungeonSys _instance;
	public static DungeonSys Instance
	{
		get { return _instance; }
	}

	public override void InitSys()
	{
		base.InitSys();
		_instance = this;
		PECommon.Log("DungeonSys Init Done...");
	}

	public void EnterDungeonSys()
	{
		SetDungeonWindState();
	}

	public void RspDungeon(GameMsg msg)
	{
		GameRoot.Instance.UpdatePower(msg.rspDungeon.power);
		MainCitySys.Instance.mainCityWind.SetWindState(false);
		SetDungeonWindState(false);
		BattleSys.Instance.StartBattle(msg.rspDungeon.tid);
	}

	public void SetDungeonWindState(bool active = true)
	{
		DunGeonWind.SetWindState(active);
	}
}