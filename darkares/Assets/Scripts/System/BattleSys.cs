/****************************************************
    文件：BattleSys.cs
	作者：Ling
    邮箱: 1759147969@qq.com
    日期：2019/8/22 18:35:26
	功能：战斗业务系统
*****************************************************/

using UnityEngine;

public class BattleSys : SystemRoot
{
	public PlayerCtrlWind PlayerCtrlWind;
	private BattleMgr battleMgr;
	
	private static BattleSys _instance;
	public static BattleSys Instance
	{
		get { return _instance; }
	}
	
	public override void InitSys()
	{
		base.InitSys();
		_instance = this;
		PECommon.Log("BattleSys Init Done...");
	}

	public void StartBattle(int mapid)
	{
		GameObject go = new GameObject("BattleRoot");
		go.transform.SetParent(GameRoot.Instance.transform);
		battleMgr = go.AddComponent<BattleMgr>();
		battleMgr.Init(mapid);
	}
	
	public void SetMoveDir(Vector2 dir)
	{
		battleMgr.SetMoveDir(dir);
	}

	public void ReleaseSKill(int index)
	{
		battleMgr.ReleaseSKill(index);
	}

	public Vector2 GetDirInput()
	{
		if (PlayerCtrlWind == null) return Vector2.zero;
		return PlayerCtrlWind.CurrentDir;
	}
}