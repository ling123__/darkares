/****************************************************
    文件：PathDefind.cs
	作者：Ling
    邮箱: 1759147969@qq.com
    日期：2019/1/10 13:39:54
	功能：文件路径常量
*****************************************************/

using UnityEngine;

public class PathDefind 
{
    //文件路径常量定义
    public const string RdName = "ResCfgs/rdname";
    public const string Map = "ResCfgs/map";
    public const string Guide = "ResCfgs/guide";
    public const string Equip = "ResCfgs/strong";
    public const string TaskReawar = "ResCfgs/TaskReward";
    public const string Skill = "ResCfgs/skill";
    public const string SkillAction = "ResCfgs/skillaction";
    public const string SkillMove = "ResCfgs/skillMove";
    public const string MonsterCfg = "ResCfgs/monster";

    //角色路径
    public const string AssissnCityPlayerPrefab = "PrefabPlayer/AssassinCity";
    public const string AssissnBattlePlayerPrefab = "PrefabPlayer/AssassinBattle";
    
    //任务Item路径
    public const string TaskItemPrefab = "PrefabsUI/TaskItem";
    public const string EntityItemPrefab = "PrefabsUI/ItemEntityHp";
    
    /*物品图片路径*/
    public const string ItemToukui = "ResImages/Res/toukui";
    public const string ItemBody = "ResImages/Res/body";
    public const string ItemYaobu = "ResImages/Res/yaobu";
    public const string ItemHand = "ResImages/Res/hand";
    public const string ItemLeg = "ResImages/Res/leg";
    public const string ItemFoot = "ResImages/Res/foot";
    public const string ItemBtnType1 = "ResImages/Res/btntype1";
    public const string ItemBtnType2 = "ResImages/Res/btntype2";
    
    /*星星图片路径*/
    public const string Star1 = "ResImages/Res/star1";
    public const string Star2 = "ResImages/Res/star2";
    /*领取奖励图片路径*/
    public const string Box1 = "ResImages/Res/box1";
    public const string Box2 = "ResImages/Res/box2";

    /*人物头像图片路径*/
    public const string TaskHead = "ResImages/Res/task";
    public const string WiseHead = "ResImages/Res/wiseman";
    public const string GeneralHead = "ResImages/Res/general";
    public const string ArtisanHead = "ResImages/Res/artisan";
    public const string TraderHead = "ResImages/Res/trader";
    public const string GuideHead = "ResImages/Res/npcguide";

    //人物图片
    public const string Assassin = "ResImages/Res/assassin";
    public const string WiseInfo = "ResImages/Res/WiseInfo";
    public const string GeneralInfo = "ResImages/Res/GeneralInfo";
    public const string ArtisanInfo = "ResImages/Res/ArtisanInfo";
    public const string TraderInfo = "ResImages/Res/TraderInfo";
    public const string GuideInfo= "ResImages/Res/GuideInfo";
}