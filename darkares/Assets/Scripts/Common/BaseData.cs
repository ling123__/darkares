/****************************************************
    文件：BaseData.cs
	作者：Ling
    邮箱: 1759147969@qq.com
    日期：2019/1/19 17:45:29
	功能：存储地图基本信息
*****************************************************/

using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Security.Permissions;
using UnityEngine;
using UnityEngine.Experimental.UIElements;

//存储地图的基本信息
public class MapCfg : BaseData<MapCfg>
{
    public string MapName;          //地图名称
    public string SceneName;        //场景名称
    public int Power;               //进入场景所需体力
    public Vector3 MainCamPos;      //主摄像机位置
    public Vector3 MainCamRote;     //主摄像机旋转
    public Vector3 PlayerBornPos;   //人物出生位置
    public Vector3 PlayerBornRote;  //人物出生旋转
    public List<MonsterData> MonsterLst;  //敌人属性配置
}

public class MonsterData : BaseData<MonsterData>
{
    public int MWave;
    public int MIndex;
    public MonsterCfg MonsterCfg;
    public Vector3 MBornPos;
    public Vector3 MBornRot;
    public int MLevel;
}

public class MonsterCfg : BaseData<MonsterCfg>
{
    public string MName;
    public string ResPath;
    public int SkillId;
    public float AtkDis;
    public BattleProps MProps;
}

public class BaseData<T> 
{
    public int ID;
}

public class AutoGuideCfg : BaseData<AutoGuideCfg>
{
    public int NpcId;
    public string DilogArr;
    public int ActId;
    public int Coin;
    public int Exp;
}

//任务属性配置
public class TaskRewardCfg : BaseData<TaskRewardCfg>
{
    public string TaskName;
    public int Count;
    public int Exp;
    public int Coin;
}

public class TaskRewardData : BaseData<TaskRewardData>
{
    public int Count;
    public bool Finish;
}

public class EquipCfg : BaseData<EquipCfg>
{
    public int Pos;
    public int StartLv;
    public int AddHp;
    public int AddHurt;
    public int AddDef;
    public int MinLv;
    public int Coin;
    public int Crystal;
}

public class SkillCfg : BaseData<SkillCfg>
{
    public string SkillName;
    public int CdTime;
    public int SkillTime;
    public int AniAction;
    public string SkillAni;
    public bool IsCombo;
    public DmgType DmgType;
    public List<int> SkillMoveLst;
    public List<int> SkillActionLst;
    public List<int> SkillDamageLst;
}

public class SKillActionCfg : BaseData<SKillActionCfg>
{
    public int DelayTime;
    public float Radius;
    public int Angle;
}
public class SkillMoveCfg : BaseData<SkillMoveCfg>
{
    public int DelayTime;
    public int MoveTime;
    public float MoveDis;
}

public class BattleProps
{
    public int Hp;
    public int Ad;
    public int Ap;
    public int Addef;
    public int Apdef;
    public int Dodge;
    public int Pierce;
    public int Critical;
}
