/****************************************************
    文件：WindowRoot.cs
	作者：Ling
    邮箱: 1759147969@qq.com
    日期：2019/1/9 12:10:14
	功能：UI界面基类
*****************************************************/

using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using Object = System.Object;

public class WindowRoot : MonoBehaviour 
{
    protected ResServices _resSvc = null;
    protected AudioServices _audioSvc = null;
    protected NetServices _netSvc = null;
    protected TimerService _timerSvc = null;

    public void SetWindState(bool isActive = true)
    {
        //设置窗口状态
        if(gameObject.activeSelf != isActive)
        {
            SetActive(gameObject,isActive);
        }
        //判断窗口状态，并执行相应方法
        if (isActive)
        {
            InitWnd();
        }
        else
        {
            CloseWnd();
        }
    }

    protected virtual void InitWnd()
    {
        _resSvc = ResServices.Instance;  //拿到资源模块的引用
        _audioSvc = AudioServices.Instance;
        _netSvc = NetServices.Instance;
        _timerSvc = TimerService.Instance;
    }

    protected virtual void CloseWnd()
    {
        _resSvc = null;
        _audioSvc = null;
        _netSvc = null;
        _timerSvc = null;
    }

    #region Tool Function
    /*对物体的激活*/
    protected void SetActive(GameObject go,bool isActive = true)
    {
        go.SetActive(isActive);
    }
    protected void SetActive(Transform trans,bool state = true)
    {
        trans.gameObject.SetActive(state);
    }
    protected void SetActive(RectTransform rectTrans, bool state = true)
    {
        rectTrans.gameObject.SetActive(state);
    }
    protected void SetActive(Image img, bool state = true)
    {
        img.gameObject.SetActive(state);
    }
    protected void SetActive(Text txt, bool state = true)
    {
        txt.gameObject.SetActive(state);
    }

    /*设置文本*/
    protected void SetText(Text txt,string context = "")
    {
        txt.text = context;
    }
    protected void SetText(Transform trans, string context = "")
    {
        SetText(trans.GetComponent<Text>(), context);
    }

    protected void SetText(Text txt, int num = 0)
    {
        SetText(txt, num.ToString());
    }
    protected void SetText(Transform trans,int num = 0)
    {
        SetText(trans.GetComponent<Text>(), num);
    }

    protected void SetSprite(Image img,string path)
    {
        Sprite sp = _resSvc.LoadSprite(path, true);
        img.sprite = sp;   //图片的sprite变为拿到的资源
    }

    //判断当前物体是否有了的Component
    protected T GetOrAddComponent<T>(GameObject go) where T : Component
    {
        T t = go.GetComponent<T>();
        if(t == null)                     //如果物体上没有相应的COmponent，则添加
        {
            t = go.AddComponent<T>();
        }
        return t;
    }
    #endregion

    #region ClickEvent
    protected void OnClick(GameObject go,Action<Object> cb,Object args)
    {
        PEListener listener = GetOrAddComponent<PEListener>(go);
        listener.OnClick = cb;   //等于cb的回调函数
        listener.args = args;
    }
    
    protected void OnClickDown(GameObject go,Action<PointerEventData> cb)
    {
        PEListener listener = GetOrAddComponent<PEListener>(go);
        listener.OnClickDown = cb;   //等于cb的回调函数
    }

    protected void OnClickUp(GameObject go,Action<PointerEventData> cb)
    {
        PEListener listener = GetOrAddComponent<PEListener>(go);
        listener.OnClickUp = cb;   //等于cb的回调函数
    }

    protected void OnClickDrag(GameObject go, Action<PointerEventData> cb)
    {
        PEListener listener = GetOrAddComponent<PEListener>(go);
        listener.OnClickDrag = cb;   //等于cb的回调函数
    }
    #endregion
}