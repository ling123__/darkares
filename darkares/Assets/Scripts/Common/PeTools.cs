/****************************************************
    文件：PeTools.cs
	作者：Ling
    邮箱: 1759147969@qq.com
    日期：2019/1/10 14:8:35
	功能：工具类
*****************************************************/

using UnityEngine;
using System;

public class PeTools 
{
    
    public static int RdInt(int min,int max, System.Random rd = null)
    {
        if(rd == null)
        {
            rd = new System.Random();
        }
        int val = rd.Next(min, max + 1);   //产生新的数值
        return val;
    }
}