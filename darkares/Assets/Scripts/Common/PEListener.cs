/****************************************************
    文件：PEListener.cs
	作者：Ling
    邮箱: 1759147969@qq.com
    日期：2019/1/19 10:37:24
	功能：UI事件监听
*****************************************************/

using System;
using UnityEngine;
using UnityEngine.EventSystems;
using Object = System.Object;

//Unity中自带的接口，
public class PEListener : MonoBehaviour, IPointerDownHandler,IPointerClickHandler, IPointerUpHandler, IDragHandler
{
    public Action<Object> OnClick;
    public Action<PointerEventData> OnClickDown;   //PointerEventData类型的委托事件
    public Action<PointerEventData> OnClickUp;
    public Action<PointerEventData> OnClickDrag;

    public Object args;
    //这些函数和button的click差不多，挂载的物体被点击以后，这些函数就会响应
    public void OnPointerClick(PointerEventData eventData)
    {
        if(OnClick != null)    //如果事件不为空，执行事件
        {
            OnClick(args);
        }
    }
    
    public void OnDrag(PointerEventData eventData)
    {
        if (OnClickDrag != null)    //如果事件不为空，执行事件
        {
            OnClickDrag(eventData);
        }
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        if(OnClickDown != null)    //如果事件不为空，执行事件
        {
            OnClickDown(eventData);
        }
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        if (OnClickUp != null)    //如果事件不为空，执行事件
        {
            OnClickUp(eventData);
        }
    }
}