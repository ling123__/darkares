/****************************************************
    文件：Enums.cs
	作者：Ling
    邮箱: 1759147969@qq.com
    日期：2019/8/27 20:35:15
	功能：枚举
*****************************************************/

//文本颜色枚举
public enum ColorText
{
	Red,
	Blue,
	Yellow,
	Green
}
//角色动作枚举
public enum MotionsEnum
{
	SKill1 = 101,
	Skill2 = 102,
	Skill3 = 103,
	NormalAtk1 = 111,
	NormalAtk2 = 112,
	NormalAtk3 = 113,
	NormalAtk4 = 114,
	NormalAtk5 = 115,
}
//伤害类型
public enum DmgType
{
	None,
	Ad,
	Ap,
}
//动画状态
public enum AniState
{
	None,
	Born,
	Idle,
	Move,
	Attack,
	Hit,
	Die
}

//技能
public enum SkillEnum
{
	Tuxi = 101,
	Chuanci = 102,
	Xuanfeng = 103,
	NormalAtk1 = 111,
	NormalAtk2 = 112,
	NormalAtk3 =113,
	NormalAtk4 =114,
	NormalAtk5 =115,
}

//实体类型
public enum EntityType
{
	None,
	Player,
	Monster
}