/****************************************************
    文件：Constants.cs
	作者：Ling
    邮箱: 1759147969@qq.com
    日期：2019/1/9 10:8:24
	功能：常量配置
*****************************************************/

using UnityEngine;

public class Constants 
{
    /*场景名称*/
    public const string sceneLogin = "SceneLogin";   //登陆场景
    public const string sceneMainCity = "SceneMainCity";  //主城场景
    public const int MainCityMapId = 10000;

    /*音效音乐*/
    public const string BgLogin = "bgLogin";
    public const string BgMainCity = "bgMainCity";
    public const string BgHuangye = "bgHuangYe";
    public const string GameLogin = "uiLoginBtn";
    public const string UiClick = "uiClickBtn";
    public const string UiMenuClick = "uiExtenBtn";
    public const string UiOpenPage = "uiOpenPage";
    public const string EquipStrong = "fbitem";

    /*设置默认的屏幕宽高*/
    public const int SceneWidth = 1920;
    public const int SceneHeight = 1080;
    //屏幕遥感的操作范围
    public const int RouletteDir = 90;
    //星星最大数量
    public const int StarCount = 10;

    //移动速度
    public const float PlayerMoveSpeed = 8f;
    public const float MonsterMoveSpeed = 3f;

    //运动平滑加速度
    public const float AccelerSpeed = 5;
    public const float AccelerHpSpeed = 0.3f;
    //连击间隔时间
    public const float CombTime = 500;

    //Action触发参数
    public const int IdleAction = -1;
    public const int BornAction = 0;
    public const int DieAction = 100;
    public const int HitAction = 101;

    public const int DieTime = 5000;
    //混合参数
    public const int BlendIdle = 0;
    public const int BlendMove = 1;

    /*npc的id*/
    public const int NpcWiseMan = 0;
    public const int NpcGeneralMan = 1;
    public const int NpcArtisanWoman = 2;
    public const int NpcTraderWoman = 3;

    /*颜色*/
    private const string ColorRed = "<color=#FF0000FF>";
    private const string ColorBlue = "<color=#00B4FFFF>";
    private const string ColorYellow = "<color=#FFFF00FF>";
    private const string ColorGreen = "<color=#00FF00FF>";
    private const string ColorEnd = "</color>";

    public static string color(string str,ColorText c)
    {
        string result = "";
        switch (c)
        {
            case ColorText.Red:
                result = ColorRed + str + ColorEnd;
                break;
            case ColorText.Blue:
                result = ColorBlue + str + ColorEnd;
                break;
            case ColorText.Yellow:
                result = ColorYellow + str + ColorEnd;
                break;
            case ColorText.Green:
                result = ColorGreen + str + ColorEnd;
                break;
        }
        return result;
    }
}