/****************************************************
    文件：ClientSession.cs
	作者：Ling
    邮箱: 1759147969@qq.com
    日期：2019/1/11 15:40:38
	功能：客户端网络
*****************************************************/

using PENet;
using PEProtocal;

public class ClientSession : PESession<GameMsg> 
{
    protected override void OnConnected()
    {
        GameRoot.AddTips("服务器连接");
        PECommon.Log("Connect To Server success");
    }

    protected override void OnReciveMsg(GameMsg msg)
    {
        PECommon.Log("Recive CMD:" + ((CMD)msg.cmd).ToString());
        NetServices.Instance.AddNetMsg(msg);    //调用，保存服务端的信息
    }

    protected override void OnDisConnected()
    {
        GameRoot.AddTips("服务器断开连接");
        PECommon.Log("DisConnect To Client");
    }
}