/****************************************************
    文件：StateMove.cs
	作者：Ling
    邮箱: 1759147969@qq.com
    日期：2019/8/23 9:16:13
	功能：移动状态
*****************************************************/

using UnityEngine;

public class StateMove : IState 
{
	public void Enter(EntityBase entity,params object[] args)
	{
		entity.CurrentAniState = AniState.Move;
	}

	public void Process(EntityBase entity,params object[] args)
	{
		entity.SetBlend(Constants.BlendMove);
	}

	public void Exit(EntityBase entity,params object[] args)
	{
	}
}