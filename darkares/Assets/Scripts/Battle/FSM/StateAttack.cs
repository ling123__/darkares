/****************************************************
    文件：StateAttack.cs
	作者：Ling
    邮箱: 1759147969@qq.com
    日期：2019/8/23 13:38:53
	功能：战斗状态
*****************************************************/

using UnityEngine;

public class StateAttack : IState 
{
	public void Enter(EntityBase entity,params object[] args)
	{
		entity.CurrentAniState = AniState.Attack;
		entity._CurtSkillCfg = ResServices.Instance.GetSkillCfg((int) args[0]);
	}

	public void Process(EntityBase entity,params object[] args)
	{
		//技能伤害
		entity.SkillAttack((int)args[0]);
	}

	public void Exit(EntityBase entity,params object[] args)
	{
		entity.ExitCurrentSkill();
	}
}