/****************************************************
    文件：StateBorn.cs
	作者：Ling
    邮箱: 1759147969@qq.com
    日期：2019/8/29 10:20:48
	功能：出生状态
*****************************************************/

using UnityEngine;

public class StateBorn : IState 
{
	public void Enter(EntityBase entity, params object[] args)
	{
		entity.CurrentAniState = AniState.Born;
	}

	public void Process(EntityBase entity, params object[] args)
	{
		entity.SetAction(Constants.BornAction);
		//500毫秒后将Blend数值改为-1，也就是待机状态
		TimerService.Instance.AddTimeTask((int tid) => { entity.SetAction(Constants.IdleAction); }, 500);
	}

	public void Exit(EntityBase entity, params object[] args)
	{
	}
}