/****************************************************
    文件：StateIdel.cs
	作者：Ling
    邮箱: 1759147969@qq.com
    日期：2019/8/23 9:15:56
	功能：静止状态
*****************************************************/

using System.Text;
using UnityEngine;

public class StateIdle : IState 
{
	public void Enter(EntityBase entity,params object[] args)
	{
		entity.CurrentAniState = AniState.Idle;
		entity.SetAction(Constants.IdleAction);
	}

	public void Process(EntityBase entity,params object[] args)
	{
		if (entity.NextComb != 0)
		{
			entity.Attack(entity.NextComb);
		}
		if (entity.GetDirInput() != Vector2.zero)
		{
			entity.Move();
			entity.SetDir(entity.GetDirInput());
		}
		else
		{
			entity.SetBlend(Constants.BlendIdle);
		}
	}

	public void Exit(EntityBase entity,params object[] args)
	{
	}
}