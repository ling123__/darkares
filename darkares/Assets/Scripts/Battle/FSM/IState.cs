/****************************************************
    文件：IState.cs
	作者：Ling
    邮箱: 1759147969@qq.com
    日期：2019/8/23 9:15:38
	功能：Nothing
*****************************************************/

using UnityEngine;

public interface IState
{
	void Enter(EntityBase entity,params object[] args);
	void Process(EntityBase entity,params object[] args);
	void Exit(EntityBase entity,params object[] args);
}