/****************************************************
    文件：StateHit.cs
	作者：Ling
    邮箱: 1759147969@qq.com
    日期：2019/8/29 11:25:28
	功能：受击状态
*****************************************************/

using UnityEngine;

public class StateHit : IState 
{
	public void Enter(EntityBase entity, params object[] args)
	{
		entity.CurrentAniState = AniState.Hit;
	}

	public void Process(EntityBase entity, params object[] args)
	{
		entity.SetDir(Vector2.zero);
		entity.SetAction(Constants.HitAction);
		TimerService.Instance.AddTimeTask((int tid) =>
		{
			entity.SetAction(Constants.IdleAction);
			entity.Idle();
		}, (int)(GetHitAniLength(entity) * 1000));
	}

	public void Exit(EntityBase entity, params object[] args)
	{
	}

	public float GetHitAniLength(EntityBase entity)
	{
		AnimationClip[] clips = entity.Controller.anim.runtimeAnimatorController.animationClips;
		for (int i = 0; i < clips.Length; i++)
		{
			if (clips[i].name.Contains("hit"))
			{
				return clips[i].length;
			}
		}
		//保护值
		return 1;
	}
}