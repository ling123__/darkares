/****************************************************
    文件：StateDie.cs
	作者：Ling
    邮箱: 1759147969@qq.com
    日期：2019/8/29 10:33:29
	功能：死亡状态
*****************************************************/

using UnityEngine;

public class StateDie : IState 
{
	public void Enter(EntityBase entity, params object[] args)
	{
		entity.CurrentAniState = AniState.Die;
	}

	public void Process(EntityBase entity, params object[] args)
	{
		entity.SetAction(Constants.DieAction);
		TimerService.Instance.AddTimeTask((int tid) =>
		{
			entity.Controller.gameObject.SetActive(false);
		}, Constants.DieTime);
	}

	public void Exit(EntityBase entity, params object[] args)
	{
	}
}