/****************************************************
    文件：EntityMonster.cs
	作者：Ling
    邮箱: 1759147969@qq.com
    日期：2019/8/28 20:39:32
	功能：敌人逻辑实体类
*****************************************************/

using PENet;
using UnityEditor.Build.Content;
using UnityEngine;

public class EntityMonster : EntityBase
{
	public MonsterData MonsterData;

	private float _checkTime = 2;
	private float _checkCountTime = 0;

	private float _atkTime = 2;
	private float _atkCountTime = 0;


	public EntityMonster()
	{
		EntityType = EntityType.Monster;
	}
	public override void SetBattleProps(BattleProps battleProps)
	{
		int level = MonsterData.MLevel;
		BattleProps battle = new BattleProps
		{
			Hp = battleProps.Hp + battleProps.Hp * (level / 2),
			Ad = battleProps.Ad + battleProps.Ad * (level / 2),
			Ap = battleProps.Ap + battleProps.Ap * (level / 2),
			Addef = battleProps.Addef + battleProps.Addef * (level / 2),
			Apdef = battleProps.Apdef + battleProps.Apdef * (level / 2),
			Dodge = battleProps.Dodge + battleProps.Dodge * (level / 2),
			Pierce = battleProps.Pierce + battleProps.Pierce * (level / 2),
			Critical = battleProps.Critical + battleProps.Critical * (level / 2)
		};

		BattleProps = battle;
		Hp = battle.Hp;
	}

	private bool _runAi = true;
	public override void TickAILogic()
	{
		if(!_runAi) return;

		if (CurrentAniState == AniState.Idle || CurrentAniState == AniState.Move)
		{
			_checkCountTime += Time.deltaTime;
			//怪物攻击间隔时间达到
			if (_checkCountTime > _checkTime)
			{
				//找到玩家
				Vector2 dir = CalcTargetDir();

				//判断玩家是否在攻击的范围之内
				if (!InAtkRange())
				{
					//不在：设置移动方向，进入移动状态，向玩家走去
					SetDir(dir);
					Move();
				}
				else
				{
					//在，停止移动，进入攻击状态
					SetDir(Vector2.zero);
					//移动当中也是在计时的
					_atkCountTime += _checkCountTime;
					//判断攻击间隔   //达到攻击时间，转向并攻击
					if (_atkCountTime >= _atkTime)
					{
						SetAtkRotationDir(dir);
						Attack(MonsterData.MonsterCfg.SkillId);
						_atkCountTime = 0;
					}
					//没有达到，进入Idle状态，等待攻击
					else
					{
						Idle();
					}
				}

				_checkCountTime = 0;
				_checkTime = PeTools.RdInt(1, 5) * 1.0f / 10;
			}
		}
	}

	//计算敌人和角色的方向
	public override Vector2 CalcTargetDir()
	{
		EntityPlayer entityPlayer = BattleMgr.Entityplayer;
		if (entityPlayer == null || entityPlayer.CurrentAniState == AniState.Die)
		{
			_runAi = false;
			return Vector2.zero;
		}
		else
		{
			Vector3 target = entityPlayer.GetPos();
			Vector3 self = GetPos();
			return new Vector2(target.x - self.x,target.z-self.z).normalized;
		}
	}
	//计算角色是否在敌人的攻击范围以内
	public bool InAtkRange()
	{
		EntityPlayer entityPlayer = BattleMgr.Entityplayer;
		if (entityPlayer == null || entityPlayer.CurrentAniState == AniState.Die)
		{
			_runAi = false;
			return false;
		}
		else
		{
			Vector3 target = entityPlayer.GetPos();
			Vector3 self = GetPos();
			target.y = 0;
			self.y = 0;
			float distance = Vector3.Distance(target, self);
			if (distance <= MonsterData.MonsterCfg.AtkDis)
			{
				return true;
			}

			return false;
		}
	}
}