/****************************************************
    文件：EntityPlayer.cs
	作者：Ling
    邮箱: 1759147969@qq.com
    日期：2019/8/23 9:34:52
	功能：角色逻辑实体
*****************************************************/

using System.Collections.Generic;
using UnityEngine;

public class EntityPlayer : EntityBase 
{
	public EntityPlayer()
	{
		EntityType = EntityType.Player;
	}
	public override Vector2 GetDirInput()
	{
		return BattleMgr.GetDirInput();
	}

	public override Vector2 CalcTargetDir()
	{
		EntityMonster entityMonster = FindClosedMonster();
		if (entityMonster != null)
		{
			Vector3 target = entityMonster.GetPos();
			Vector3 self = GetPos();
			Vector2 dir = new Vector2(target.x - self.x,target.z - self.z);
			return dir.normalized;
		}
		return Vector2.zero;
	}

	private EntityMonster FindClosedMonster()
	{
		//获得所有怪物列表
		List<EntityMonster> entityMonsters = BattleMgr.GetMonsters();

		if (entityMonsters != null && entityMonsters.Count != 0)
		{
			//获得自己的位置
			Vector3 selfPos = GetPos();
			EntityMonster targetMonster = entityMonsters[0];
			float minDistance = 1000;

			//循环遍历所有的敌人，比较哪个离的最近
			for (int i = 0; i < entityMonsters.Count; i++)
			{
				float curDistance = Vector3.Distance(selfPos, entityMonsters[i].GetPos());
				if (curDistance < minDistance)
				{
					minDistance = curDistance;
					targetMonster = entityMonsters[i];
				}
			}

			//返回离得近的敌人
			return targetMonster;
		}

		//周围没有敌人，返回空
		return null;
	}
}