/****************************************************
    文件：EntityBase.cs
	作者：Ling
    邮箱: 1759147969@qq.com
    日期：2019/8/23 9:15:20
	功能：逻辑实体基类
*****************************************************/

using System;
using System.Collections.Generic;
using System.Security.Permissions;
using TMPro;
using UnityEngine;

public abstract class EntityBase
{
    public AniState CurrentAniState = AniState.None;

    public StateMgr StateMgr = null;
    public SkillMgr SkillMgr = null;
    public Controller Controller = null;
    public BattleMgr BattleMgr = null;

    public EntityType EntityType;
    public BattleProps BattleProps { get; protected set; }

    public SkillCfg _CurtSkillCfg;
    public Queue<int> CombDeQueue = new Queue<int>();
    public int NextComb;
    
    private int _hp;

    public int Hp
    {
        get { return _hp; }
        set
        {
            SetHpVal(_hp, value);
            _hp = value;
        }
    }

    private string name;

    public string Name
    {
        get { return name; }
        set { name = value; }
    }

    #region 逻辑实体状态

    //出生状态
    public void Born()
    {
        StateMgr.ChangeState(this, AniState.Born, null);
    }

    //待机状态
    public void Idle()
    {
        StateMgr.ChangeState(this, AniState.Idle, null);
    }

    //移动状态
    public void Move()
    {
        StateMgr.ChangeState(this, AniState.Move, null);
    }

    //攻击状态
    public void Attack(int skillId)
    {
        StateMgr.ChangeState(this, AniState.Attack, skillId);
    }

    public void Hit()
    {
        StateMgr.ChangeState(this, AniState.Hit, null);
    }

    //死亡状态
    public void Die()
    {
        StateMgr.ChangeState(this, AniState.Die, null);
    }

    #endregion

    #region 战斗文字特效

    public virtual void SetDodge()
    {
        if (Controller != null)
            GameRoot.Instance.dynamicWnd.SetDodge(Name);
    }

    public virtual void SetCritical(int hurt)
    {
        if (Controller != null)
            GameRoot.Instance.dynamicWnd.SetDamage(Name, hurt);
    }

    public virtual void SetDamage(int hurt)
    {
        if (Controller != null)
            GameRoot.Instance.dynamicWnd.SetDamage(Name, hurt);
    }

    public virtual void SetHpVal(int old, int now)
    {
        if (Controller != null)
            GameRoot.Instance.dynamicWnd.SetHpVal(Name, old, now);
    }

    #endregion

    public virtual void SetBlend(float value)
    {
        if (Controller != null)
        {
            Controller.SetBlend(value);
        }
    }

    //设置方向
    public virtual void SetDir(Vector2 dir)
    {
        if (Controller != null)
        {
            Controller.Dir = dir;
        }
    }

    public virtual void SetAction(int action)
    {
        if (Controller != null)
        {
            Controller.SetAction(action);
        }
    }

    //设置战斗属性
    public virtual void SetBattleProps(BattleProps battleProps)
    {
        Hp = battleProps.Hp;
        BattleProps = battleProps;
    }

    //设置特效
    public virtual void SetSKillAni(string name, float destroy)
    {
        if (Controller != null)
        {
            Controller.SetSkillAni(name, destroy);
        }
    }

    //处理技能的攻击
    public virtual void SkillAttack(int skillId)
    {
        SkillMgr.SkillAttack(this, skillId);
    }

    /// <summary>
    /// 设置技能移动状态
    /// </summary>
    /// <param name="move">是否移动</param>
    /// <param name="speed">移动速度</param>
    public virtual void SetSkillMoveState(bool move, float speed = 0f)
    {
        if (Controller != null)
        {
            Controller.SetSkillMoveState(move, speed);
        }
    }

    /// <summary>
    /// 设置角色攻击方向
    /// </summary>
    /// <param name="dir"></param>
    public virtual void SetAtkRotationDir(Vector2 dir,bool offset = false)
    {
        if (Controller)
        {
            if(offset)
                Controller.SetAtkRotationCamera(dir);
            else
            {
                Controller.SetAtkRotationLocal(dir);
            }
        }
    }

    /// <summary>
    /// 获取方向
    /// </summary>
    /// <returns></returns>
    public virtual Vector2 GetDirInput()
    {
        return Vector2.zero;
    }

    public virtual Vector3 GetPos()
    {
        return Controller.transform.position;
    }

    /// <summary>
    /// 获得物体的transform
    /// </summary>
    /// <returns></returns>
    public virtual Transform GetTrans()
    {
        return Controller.transform;
    }

    /// <summary>
    /// 退出当前技能
    /// </summary>
    public void ExitCurrentSkill()
    {
        if (_CurtSkillCfg.IsCombo)
        {
            if (CombDeQueue.Count > 0)
            {
                NextComb = CombDeQueue.Dequeue();
            }
            else
            {
                NextComb = 0;
            }

            SetAction(Constants.IdleAction);
        }
    }

    /// <summary>
    /// 获得目标方向
    /// </summary>
    /// <returns></returns>
    public virtual Vector2 CalcTargetDir()
    {
        return Vector2.zero;
    }

    public virtual void TickAILogic()
    {
        
    }
}