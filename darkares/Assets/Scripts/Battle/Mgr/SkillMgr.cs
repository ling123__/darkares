/****************************************************
    文件：SkillMgr.cs
	作者：Ling
    邮箱: 1759147969@qq.com
    日期：2019/8/22 18:53:26
	功能：技能管理器
*****************************************************/

using System.Collections.Generic;
using UnityEngine;

public class SkillMgr : MonoBehaviour
{
	private ResServices _resSvc;
	private TimerService _timerSvc;
	public void Init()
	{
		_resSvc = ResServices.Instance;
		_timerSvc = TimerService.Instance;
		PECommon.Log("SkillMgr Init Done...");
	}

	public void SkillAttack(EntityBase entity, int skillId)
	{
		AttackDamage(entity,skillId);
		AttackEffect(entity,skillId);
	}
	//技能效果表现
	public void AttackEffect(EntityBase entity, int skillId)
	{
		SkillCfg skillCfg = _resSvc.GetSkillCfg(skillId);

		if (entity.EntityType == EntityType.Player)
		{
			if (entity.GetDirInput() == Vector2.zero)
			{
				//TODO  搜索最近的怪物
				Vector2 dir = entity.CalcTargetDir();
				if (dir != Vector2.zero)
				{
					entity.SetAtkRotationDir(dir);
				}
			}
			else
			{
				entity.SetAtkRotationDir(entity.GetDirInput(), true);
			}
		}

		entity.SetAction(skillCfg.AniAction);
		entity.SetSKillAni(skillCfg.SkillAni,skillCfg.SkillTime);

		CalcSkillMove(entity, skillCfg);
		
		entity.SetDir(Vector2.zero);

		_timerSvc.AddTimeTask((int tid) =>
		{
			entity.Idle();
		}, skillCfg.SkillTime);
	}

	//技能伤害结算
	public void AttackDamage(EntityBase entity, int skillId)
	{
		SkillCfg skillCfg = _resSvc.GetSkillCfg(skillId);
		List<int> actionLst = skillCfg.SkillActionLst;

		int sum = 0;
		for (int i = 0; i < actionLst.Count; i++)
		{
			SKillActionCfg sKillActionCfg = _resSvc.GetSkillActionCfg(actionLst[i]);
			sum += sKillActionCfg.DelayTime;
			int index = i;
			if (sum > 0)
			{
				_timerSvc.AddTimeTask((int tid) => { SkillAction(entity, skillCfg,index); }, sum);
			}
			else
			{
				SkillAction(entity, skillCfg,index);
			}
		}
	}
	
	//技能伤害计算
	public void SkillAction(EntityBase caster, SkillCfg skillCfg,int index)
	{
		SKillActionCfg skillAction = _resSvc.GetSkillActionCfg(skillCfg.SkillActionLst[index]);
		int damage = skillCfg.SkillDamageLst[index];
		//攻击者是敌人，被攻击者是玩家
		if (caster.EntityType == EntityType.Monster)
		{
			EntityBase player = caster.BattleMgr.Entityplayer;
			if (InRange(caster.GetPos(), player.GetPos(), skillAction.Radius)
			    && InAngle(caster.GetTrans(), player.GetPos(), skillAction.Angle))
			{
				CalcDamage(caster, player, skillCfg, damage);
			}
		}
		else if(caster.EntityType == EntityType.Player)
		{
			List<EntityMonster> monsters = caster.BattleMgr.GetMonsters();
			for (int i = 0; i < monsters.Count; i++)
			{
				EntityMonster target = monsters[i];
				if (InRange(caster.GetPos(), target.GetPos(), skillAction.Radius)
				    && InAngle(caster.GetTrans(), target.GetPos(), skillAction.Angle))
				{
					//TODO 计算伤害
					CalcDamage(caster, target, skillCfg, damage);
				}
			}
		}
	}

	System.Random rd = new System.Random();
	private void CalcDamage(EntityBase caster,EntityBase target,SkillCfg skillCfg, int damage)
	{
		int dmgSum = damage;
		bool isCritical = false;
		if (skillCfg.DmgType == DmgType.Ad)
		{
			//计算闪避
			int dodgeNum = PeTools.RdInt(1, 100, rd);
			if (dodgeNum <= target.BattleProps.Dodge)
			{
				target.SetDodge();
				return;
			}
			//计算伤害加成
			dmgSum += caster.BattleProps.Ad;
			//计算暴击
			int criticalNum = PeTools.RdInt(1, 100, rd);
			if (criticalNum <= caster.BattleProps.Critical)
			{
				isCritical = true;
				float criticalRate = 1 + (PeTools.RdInt(1, 100, rd) / 100.0f);
				dmgSum = (int) (dmgSum * criticalRate);
			}
			//计算穿甲
			int addef = (int) ((1 - caster.BattleProps.Pierce / 100.0f) * target.BattleProps.Addef);
			dmgSum -= addef;
			
		}
		else if (skillCfg.DmgType == DmgType.Ap)
		{
			//计算伤害加成
			dmgSum += caster.BattleProps.Ap;
			//计算魔抗
			dmgSum -= target.BattleProps.Apdef;
		}
		//强制扣血
		if (dmgSum <= 0)
			dmgSum = 1;
		//判断显示暴击还是普通攻击
		if(isCritical)
			target.SetCritical(dmgSum);
		else
			target.SetDamage(dmgSum);
		
		if (target.Hp < dmgSum)
		{
			target.Hp = 0;
			target.BattleMgr.RemoveMonster(target.Name);
			target.Die();
		}
		else
		{
			target.Hp -= dmgSum;
			target.Hit();
		}
	}
	
	//判断敌人是否在攻击范围内内
	private bool InRange(Vector3 from, Vector3 to, float range)
	{
		float dis = Vector3.Distance(from, to);
		if (dis <= range)
			return true;
		return false;
	}
	//判断敌人是否射程在
	private bool InAngle(Transform trans, Vector3 to, float angle)
	{
		Vector3 start = trans.forward;
		//角色和敌人之间的向量
		Vector3 end = (to - trans.position).normalized;
		//计算出角色和敌人的夹角
		float ang = Vector3.Angle(start, end);

		if (ang <= angle / 2)
			return true;
		return false;
	}

	private void CalcSkillMove(EntityBase entity,SkillCfg skillCfg)
	{
		System.Collections.Generic.List<int> skillMove = skillCfg.SkillMoveLst;
		int sumTime = 0;
		for (int i = 0; i < skillMove.Count; i++)
		{
			SkillMoveCfg skillMoveCfg = _resSvc.GetSkillMoveCfg(skillMove[i]);
			float speed = skillMoveCfg.MoveDis / (skillMoveCfg.MoveTime / 1000f);
			sumTime += skillMoveCfg.DelayTime;
			if (sumTime > 0)
			{
				_timerSvc.AddTimeTask((int tid) => { entity.SetSkillMoveState(true, speed); }, skillMoveCfg.DelayTime);
			}
			else
			{
				entity.SetSkillMoveState(true, speed);
			}

			sumTime += skillMoveCfg.MoveTime;
			_timerSvc.AddTimeTask((int tid) => { entity.SetSkillMoveState(false); },sumTime);
		}
	}
}