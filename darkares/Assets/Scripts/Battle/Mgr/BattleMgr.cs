/****************************************************
    文件：BattleMgr.cs
	作者：Ling
    邮箱: 1759147969@qq.com
    日期：2019/8/22 18:35:4
	功能：战斗管理器
*****************************************************/

using System.Collections.Generic;
using PEProtocal;
using UnityEngine;

public class BattleMgr : MonoBehaviour
{
	private ResServices _resSvc;
	private AudioServices _audioSvc;
	private TimerService _timerSvc;
	
	private MapMgr _mapMgr;
	private SkillMgr _skillMgr;
	private StateMgr _stateMgr;

	private PlayerController _playerController;
	public EntityPlayer Entityplayer;
	private MonsterController _monsterController;
	private EntityMonster _entityMonster;
	private MapCfg _mapCfg;
	
	private Dictionary<string,EntityMonster> _monsterDict = new Dictionary<string, EntityMonster>();


	//遍历所有的敌人，并调用其AI操作
	private void Update()
	{
		foreach (var entityMonster in _monsterDict)
		{
			EntityMonster em = entityMonster.Value;
			em.TickAILogic();
		}
	}

	public void Init(int mapid)
	{
		_resSvc = ResServices.Instance;
		_audioSvc = AudioServices.Instance;
		_timerSvc = TimerService.Instance;
		
		_skillMgr = gameObject.AddComponent<SkillMgr>();
		_skillMgr.Init();
		_stateMgr = gameObject.AddComponent<StateMgr>();
		_stateMgr.Init();
		
		//加载战斗场景
		_mapCfg = _resSvc.GetMapCfg(mapid);
		_resSvc.AsyncLoadScence(_mapCfg.MapName, () =>
		{
			//初始化地图数据
			//GameObject map = GameObject.FindWithTag("MapRoot");
			GameObject map = GameObject.FindGameObjectWithTag("MapRoot");
			_mapMgr = map.GetComponent<MapMgr>();
			_mapMgr.Init(this);

			map.transform.localPosition = Vector3.zero;
			map.transform.localScale = Vector3.one;

			Camera.main.transform.position = _mapCfg.MainCamPos;
			Camera.main.transform.localEulerAngles = _mapCfg.MainCamRote;
			
			LoadPlayer(_mapCfg);
			Entityplayer.SetBlend(Constants.BlendIdle);
			//激活第一批敌人
			ActiveMonsterByTimer();
			_audioSvc.PlayBgAudio(Constants.BgHuangye);
			MainCitySys.Instance.SetPlayerCtrlWindState(true);
		});
		
		
	}

	private void ActiveMonsterByTimer()
	{
		_timerSvc.AddTimeTask((int tid) =>
		{
			foreach (var monster in _monsterDict)
			{
				monster.Value.Controller.gameObject.SetActive(true);
				monster.Value.Born();
				//900毫秒后切换为待机状态
				_timerSvc.AddTimeTask((int id) => { monster.Value.Idle(); }, 900);
			}
		}, 500);
	}
	//加载角色
	private void LoadPlayer(MapCfg mapCfg)
	{
		GameObject player = _resSvc.LoadPrefab(PathDefind.AssissnBattlePlayerPrefab);
		player.transform.position = mapCfg.PlayerBornPos;
		player.transform.localEulerAngles = mapCfg.PlayerBornRote;

		PlayerData pd = GameRoot.Instance.playerData;
		BattleProps props = new BattleProps
		{
			Hp = pd.hp,
			Ad = pd.ad,
			Ap = pd.ap,
			Addef = pd.addef,
			Apdef = pd.apdef,
			Dodge = pd.dodge,
			Pierce = pd.pierce,
			Critical = pd.critical,
		};
		Entityplayer = new EntityPlayer
		{
			StateMgr = _stateMgr,
			SkillMgr = _skillMgr,
			BattleMgr = this
		};
		Entityplayer.SetBattleProps(props);
		Entityplayer.Name = "player";
		
		_playerController = player.GetComponent<PlayerController>();
		_playerController.Init();
		Entityplayer.Controller = _playerController;   //注入逻辑实体
	}

	public void LoadMonsterByWave(int wave)
	{
		for (int i = 0; i < _mapCfg.MonsterLst.Count; i++)
		{
			MonsterData md = _mapCfg.MonsterLst[i];
			if (md.MWave == wave)
			{
				GameObject enemy = _resSvc.LoadPrefab(md.MonsterCfg.ResPath);
				enemy.transform.localPosition = md.MBornPos;
				enemy.transform.localEulerAngles = md.MBornRot;
				enemy.transform.localScale = Vector3.one;
				enemy.name = "monster" + wave + "_" + md.MIndex;

				_entityMonster = new EntityMonster
				{
					StateMgr = _stateMgr,
					SkillMgr = _skillMgr,
					BattleMgr = this
				};
				_entityMonster.MonsterData = md;
				_entityMonster.SetBattleProps(md.MonsterCfg.MProps);
				_entityMonster.Name = enemy.name;

				_monsterController = enemy.GetComponent<MonsterController>();
				_monsterController.Init();
				_entityMonster.Controller = _monsterController;
				enemy.SetActive(false);
				_monsterDict.Add(enemy.name,_entityMonster);
				
				GameRoot.Instance.dynamicWnd.AddItemEntity(enemy.name,_monsterController.HpRoot, _entityMonster.Hp);
			}
		}
	}

	//返回场景中的所有怪物实体
	public List<EntityMonster> GetMonsters()
	{
		List<EntityMonster> lst = new List<EntityMonster>();
		foreach (var item in _monsterDict)
		{
			lst.Add(item.Value);
		}

		return lst;
	}
	
	//移除
	public void RemoveMonster(string key)
	{
		EntityMonster entityMonster = null;
		if (_monsterDict.TryGetValue(key, out entityMonster))
		{
			_monsterDict.Remove(key);
			GameRoot.Instance.dynamicWnd.RemoveItemEntity(key);
		}
	}
	

	public void SetMoveDir(Vector2 dir)
	{
		if(_playerController.skillMove) return;
		
		if (dir == Vector2.zero)
		{
			Entityplayer.Idle();
			Entityplayer.SetDir(Vector2.zero);
		}
		else
		{
			Entityplayer.Move();
			Entityplayer.SetDir(dir);
		}
	}

	public void ReleaseSKill(int index)
	{
		switch (index)
		{
			case 0:
				NormalAttack();
				break;
			case 1:
				Skill1Attack();
				break;
			case 2:
				Skill2Attack();
				break;
			case 3:
				Skill3Attack();
				break;
		}
	}

	private int[] _skillComb = {111, 112, 113, 114};
	private int _combIndex;
	private bool _isComb;
	private double _lastTime = 0;
	private void NormalAttack()
	{
		if (Entityplayer.CurrentAniState == AniState.Attack)
		{
			double nowTime = TimerService.Instance.GetNowTime();
			if (nowTime - _lastTime < Constants.CombTime && _lastTime != 0)
			{
				if(_combIndex >= 4) return;
				_isComb = true;
				_combIndex += 1;
				Entityplayer.CombDeQueue.Enqueue(_skillComb[_combIndex]);
				_lastTime = TimerService.Instance.GetNowTime();
			}
		}
		else if (Entityplayer.CurrentAniState == AniState.Idle
		          || Entityplayer.CurrentAniState == AniState.Move)
		{
			_combIndex = 0;
			_isComb = false;
			_lastTime = TimerService.Instance.GetNowTime();
			Entityplayer.Attack((int)MotionsEnum.NormalAtk1);
		}
	}

	private void Skill1Attack()
	{
		Entityplayer.Attack((int)MotionsEnum.SKill1);
	}
	private void Skill2Attack()
	{
		Entityplayer.Attack((int)MotionsEnum.Skill2);
	}
	private void Skill3Attack()
	{
		Entityplayer.Attack((int)MotionsEnum.Skill3);
	}

	public Vector2 GetDirInput()
	{
		return BattleSys.Instance.GetDirInput();
	}
}