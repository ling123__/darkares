/****************************************************
    文件：MapMgr.cs
	作者：Ling
    邮箱: 1759147969@qq.com
    日期：2019/8/22 18:52:38
	功能：战斗地图管理器
*****************************************************/

using UnityEngine;

public class MapMgr : MonoBehaviour
{
	private BattleMgr _battleMgr;
	private int wave = 1;
	public void Init(BattleMgr battleMgr)
	{
		_battleMgr = battleMgr;
		
		_battleMgr.LoadMonsterByWave(wave);
		PECommon.Log("MapMgr Init Done...");
	}
}