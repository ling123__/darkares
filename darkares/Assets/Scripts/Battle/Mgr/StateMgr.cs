/****************************************************
    文件：StateMgr.cs
	作者：Ling
    邮箱: 1759147969@qq.com
    日期：2019/8/22 18:53:11
	功能：角色状态管理器
*****************************************************/

using System.Collections.Generic;
using UnityEngine;

public class StateMgr : MonoBehaviour 
{
	private Dictionary<AniState,IState> _fsm = new Dictionary<AniState, IState>();
	
	public void Init()
	{
		_fsm.Add(AniState.Idle,new StateIdle());
		_fsm.Add(AniState.Move,new StateMove());
		_fsm.Add(AniState.Attack,new StateAttack());
		_fsm.Add(AniState.Born,new StateBorn());
		_fsm.Add(AniState.Hit,new StateHit());
		_fsm.Add(AniState.Die,new StateDie());
		
		PECommon.Log("StateMgr Init Done...");
	}

	public void ChangeState( EntityBase entity,AniState targetState,params object[] args)
	{
		if(entity.CurrentAniState == targetState)
			return;

		//如果状态机中包含当前状态
		if (_fsm.ContainsKey(targetState))
		{
			//并且当前状态不为空，则执行当前状态的退出
			if (entity.CurrentAniState != AniState.None)
			{
				_fsm[entity.CurrentAniState].Exit(entity,args);
			}
			//执行目标状态的进入和执行
			_fsm[targetState].Enter(entity,args);
			_fsm[targetState].Process(entity,args);
		}
	}
}