/****************************************************
    文件：PlayerController.cs
	作者：Ling
    邮箱: 1759147969@qq.com
    日期：2019/1/19 12:23:30
	功能：角色控制
*****************************************************/

using UnityEngine;

public class PlayerController : Controller
{
    public GameObject DaggerSkill1;
    public GameObject DaggerSkill2;
    public GameObject DaggerSkill3;
    
    public GameObject DaggerAtk1;
    public GameObject DaggerAtk2;
    public GameObject DaggerAtk3;
    public GameObject DaggerAtk4;
    public GameObject DaggerAtk5;
    
    
    private Vector3 _cameraOffset;

    private float _targetBlend;     //目标混合值
    private float _currentBlend;    //当前混合值
    //初始化
    public override void Init()
    {
        base.Init();
        anim = GetComponent<Animator>();
        charCtr = GetComponent<CharacterController>();
        cameraTran = Camera.main.transform;
        _cameraOffset = transform.position - cameraTran.position;  //位置差

        //添加动画
        SkillAniDic.Add(DaggerSkill1.name,DaggerSkill1);
        SkillAniDic.Add(DaggerSkill2.name,DaggerSkill2);
        SkillAniDic.Add(DaggerSkill3.name,DaggerSkill3);
        SkillAniDic.Add(DaggerAtk1.name,DaggerAtk1);
        SkillAniDic.Add(DaggerAtk2.name,DaggerAtk2);
        SkillAniDic.Add(DaggerAtk3.name,DaggerAtk3);
        SkillAniDic.Add(DaggerAtk4.name,DaggerAtk4);
        SkillAniDic.Add(DaggerAtk5.name,DaggerAtk5);
        
        gameObject.transform.localPosition = new Vector3(0, 0, 0);
    }

    private void Update()
    {
        //        float h = Input.GetAxis("Horizontal");
        //        float v = Input.GetAxis("Vertical");
        //
        //        Vector2 _dir = new Vector2(h, v).normalized;
        //        if (_dir != Vector2.zero) {
        //            Dir = _dir;
        //            SetBlend(Constants.BlendMove);
        //        }
        //        else
        //        {
        //            Dir = Vector2.zero;
        //            SetBlend(Constants.BlendIdle);
        //        }

        if(_currentBlend != _targetBlend)
        {
            UpdateMixBlend();
        }

        if (isMove && !skillMove)
        {
            //设置方向
            SetDir();
            //产生移动
            SetMove();
            //相机跟随
            SetCamera();
        }

        if (skillMove)
        {
            SetSkillMove();
            SetCamera();
        }
    }

    //设置方向
    private void SetDir()
    {
        //加上摄像机的y轴偏移。
        float angle = Vector2.SignedAngle(Dir, new Vector2(0, 1)) + cameraTran.eulerAngles.y;
        transform.localEulerAngles = new Vector3(0, angle, 0);
    }
    //设置移动
    private void SetMove()
    {
        charCtr.Move(transform.forward * Time.deltaTime * Constants.PlayerMoveSpeed);
    }

    //设置技能时移动
    private void SetSkillMove()
    {
        charCtr.Move(transform.forward * Time.deltaTime * skillMoveSpeed);
    }

    //设置摄像机
    public void SetCamera()
    {
        if(cameraTran != null)
        {
            cameraTran.position = transform.position - _cameraOffset;
        }
    }

    //设置动画过渡
    public override void SetBlend(float blend)
    {
        _targetBlend = blend;
    }
    //设置混合值
    private void UpdateMixBlend()
    {
        //如果目标值和当前值小于每帧更新的数值的话，就直接设置当前值为目标值
        if(Mathf.Abs(_currentBlend - _targetBlend) < Constants.AccelerSpeed * Time.deltaTime)
        {
            _currentBlend = _targetBlend;
        }
        else if(_currentBlend > _targetBlend)
        {
            _currentBlend -= Constants.AccelerSpeed * Time.deltaTime;
        }
        else
        {
            _currentBlend += Constants.AccelerSpeed * Time.deltaTime;
        }
        anim.SetFloat("Blend", _currentBlend);
    }

    public override void SetSkillAni(string name, float destroy)
    {
        GameObject go;
        if (SkillAniDic.TryGetValue(name, out go))
        {
            go.SetActive(true);
            _timerSvc.AddTimeTask((int tid) =>
            {
                go.SetActive(false);
            }, destroy);
        }
    }
}