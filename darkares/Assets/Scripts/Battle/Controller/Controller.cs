/****************************************************
    文件：Controller.cs
	作者：Ling
    邮箱: 1759147969@qq.com
    日期：2019/8/23 9:32:46
	功能：Nothing
*****************************************************/

using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;
using Debug = UnityEngine.Debug;

public abstract class Controller : MonoBehaviour 
{
	[HideInInspector]
	public Animator anim;
	[HideInInspector]
	public CharacterController charCtr;
	protected bool isMove = false;
	[HideInInspector]
	public bool skillMove;
	
	protected Transform cameraTran;
	protected Vector2 dir = Vector2.zero;
	protected float skillMoveSpeed;
	public Vector2 Dir
	{
		get
		{
			return dir;
		}

		set
		{
			if (value == Vector2.zero)  //是否移动
			{
				isMove = false;
			}
			else
			{
				isMove = true;
			}
			dir = value;
		}
	}

	protected TimerService _timerSvc;
	protected Dictionary<string,GameObject> SkillAniDic = new Dictionary<string, GameObject>();
	public virtual void Init()
	{
		_timerSvc = TimerService.Instance;
	}
	public virtual void SetBlend(float value)
	{
		anim.SetFloat("Blend",value);
	}

	public virtual void SetAction(int action)
	{
		anim.SetInteger("Action",action);
	}

	public virtual void SetSkillAni(string name, float destroy)
	{
		
	}

	public virtual void SetAtkRotationLocal(Vector2 atkDir)
	{
		float angle = Vector2.SignedAngle(atkDir, new Vector2(0, 1));
		transform.localEulerAngles = new Vector3(0, angle, 0);
	}
	public virtual void SetAtkRotationCamera(Vector2 camDir)
	{
		//加上摄像机的y轴偏移。
		float angle = Vector2.SignedAngle(camDir, new Vector2(0, 1)) + cameraTran.eulerAngles.y;
		transform.localEulerAngles = new Vector3(0, angle, 0);
	}

	public void SetSkillMoveState(bool move, float skillSpeed)
	{
		skillMove = move;
		skillMoveSpeed = skillSpeed;
	}
	
}