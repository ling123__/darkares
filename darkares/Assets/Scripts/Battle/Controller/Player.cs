using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Player : MonoBehaviour
{
    public GameObject Target1;

    private NavMeshAgent _navMesh;

    private void Start()
    {
        _navMesh = GetComponent<NavMeshAgent>();
        _navMesh.enabled = true;
        //_navMesh.SetDestination(Target1.transform.position);
    }

    private void Update()
    {
        float dis = Vector3.Distance(transform.position, Target1.transform.position);
        if (dis > 0.5f)
        {
            _navMesh.speed = 5;
            _navMesh.SetDestination(Target1.transform.position);
        }
        else
        {
            _navMesh.enabled = false;
            _navMesh.isStopped = true;
        }
    }
}
