/****************************************************
    文件：MonsterController.cs
	作者：Ling
    邮箱: 1759147969@qq.com
    日期：2019/8/28 20:39:16
	功能：敌人表现实体控制器类
*****************************************************/

using UnityEngine;

public class MonsterController : Controller
{
	public Transform HpRoot;
	public override void Init()
	{
		base.Init();
		anim = GetComponent<Animator>();
		charCtr = GetComponent<CharacterController>();
	}

	private void Update()
	{
		//敌人AI逻辑表现
		if (isMove)
		{
			SetDir();
			
			SetMove();
		}
	}
	
	//设置方向
	private void SetDir()
	{
		//加上摄像机的y轴偏移。
		float angle = Vector2.SignedAngle(Dir, new Vector2(0, 1));
		transform.localEulerAngles = new Vector3(0, angle, 0);
	}
	
	//设置移动
	private void SetMove()
	{
		charCtr.Move(transform.forward * Time.deltaTime * Constants.MonsterMoveSpeed);
		//给一个向下的力,以便在没有apply root时怪物不会腾空
		charCtr.Move(Vector3.down * Time.deltaTime * Constants.MonsterMoveSpeed);
	}
}