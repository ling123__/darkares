/****************************************************
    文件：GameRoot.cs
	作者：Ling
    邮箱: 1759147969@qq.com
    日期：2019/1/8 21:31:37
	功能：游戏启动入口，初始化各个系统模块
*****************************************************/

using PEProtocal;
using UnityEngine;

public class GameRoot : MonoBehaviour 
{
    public LoadingWind loadingWind;   //因为加载场景需要被频繁的调用，所以保留在GameRoot中
    public DynamicWnd dynamicWnd;
  

    private static GameRoot _instance;
    public static GameRoot Instance
    {
        get { return _instance; }
    }

    private void Start()
    {
        _instance = this;
        DontDestroyOnLoad(this);
        PECommon.Log("GameRoot Init Done...");

        CleanUiWnd();  //初始化所有的窗口

        Init();
    }

    private void CleanUiWnd()
    {
        Transform uiWnd = transform.Find("Canvas");
        for(int i = 0;i < uiWnd.childCount; i++)
        {
            //一个一个关闭
            uiWnd.GetChild(i).gameObject.SetActive(false);
        }
        
    }

    /*初始化各个模块*/
    private void Init()
    {
        //先加载服务模块
        NetServices net = GetComponent<NetServices>();
        net.InitSvc();
        ResServices res = GetComponent<ResServices>();
        res.InitService();
        AudioServices audio = GetComponent<AudioServices>();
        audio.InitAudioSvc();
        TimerService timer = GetComponent<TimerService>();
        timer.InitTimerSvc();

        //在加载业务模块
        LogSys log = GetComponent<LogSys>();
        log.InitSys();
        MainCitySys mainCitySys = GetComponent<MainCitySys>();
        mainCitySys.InitSys();
        DungeonSys dungeonSys = GetComponent<DungeonSys>();
        dungeonSys.InitSys();
        BattleSys battleSys = GetComponent<BattleSys>();
        battleSys.InitSys();

        dynamicWnd.SetWindState();  //默认打开，因为需要随时Tips
        /*进入登陆场景并加载相应的UI*/
        log.EnterLoading();

    }

    public static void AddTips(string tips)
    {
        Instance.dynamicWnd.AddTips(tips);
    }

    /*玩家数据的读写*/
    public PlayerData playerData = null;
    public PlayerData PlayerData
    {
        get { return playerData; }
    }
    public void SetPlayerData(RspLogin data)
    {
        playerData = data.playerData;
    }

    public void SetPlayerName(string name)
    {
        playerData.name = name;
    }
    //更新用户的数据
    public void UpdatePlayerDataByGuide(RspGuide data)
    {
        //更新用户数据
        playerData.guideid = data.npcID;
        playerData.coin = data.coin;
        playerData.exp = data.exp;
        playerData.lv = data.lv;
    }
    //更新用户强化装备后的数据
    public void UpdatePlayerDataByEquip(RspEquip data)
    {
        playerData.coin = data.coin;
        playerData.crystal = data.crystal;
        playerData.hp = data.hp;
        playerData.ap = data.ap;
        playerData.ad = data.ad;
        playerData.addef = data.addef;
        playerData.apdef = data.apdef;
        playerData.equip = data.equip;
    }
    //更新玩家的体力
    public void UpdatePower(int power)
    {
        playerData.power = power;
    }
    //更新玩家的金钱和经验
    public void UpdateExpAndCoin(RspTask data)
    {
        playerData.coin += data.coin;
        playerData.exp = data.exp;
        playerData.lv = data.lv;
        playerData.taskArr = data.taskArr;
        GameRoot.AddTips(Constants.color( "金钱增加 " + data.coin+" 经验增加 " + data.exp,ColorText.Blue));
    }
    //更新玩家任务进度
    public void UpdateTaskPro(PshTaskPro taskPro)
    {
        playerData.taskArr = taskPro.taskArr;
    }
}