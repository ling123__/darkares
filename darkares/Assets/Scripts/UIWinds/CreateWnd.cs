/****************************************************
    文件：CreateWnd.cs
	作者：Ling
    邮箱: 1759147969@qq.com
    日期：2019/1/9 19:34:9
	功能：创建角色界面
*****************************************************/

using PEProtocal;
using UnityEngine;
using UnityEngine.UI;

public class CreateWnd : WindowRoot 
{
    public InputField iptRdName;
    //TODO
    protected override void InitWnd()
    {
        base.InitWnd();

        //显示一个随机名字
        iptRdName.text = _resSvc.GetRdNameData(false);
    }

    //点击骰子随机生成姓名
    public void BtnRdName()
    {
        _audioSvc.PlayeUiAudio(Constants.UiClick);

        string rdName = _resSvc.GetRdNameData(false);
        iptRdName.text = rdName;
    }

    //点击开始游戏
    public void EnterGame()
    {
        _audioSvc.PlayeUiAudio(Constants.UiClick);

        if (iptRdName.text != "")
        {
            //发送名字数据到服务器，登录主城
            GameMsg msg = new GameMsg
            {
                cmd = (int)CMD.ReqRename,
                reqRename = new ReqRename
                {
                    name = iptRdName.text
                }
            };
            _netSvc.SendMsg(msg);
        }
        else
        {
            GameRoot.AddTips("输入名字不合法，请重新输入");
        }
    }
}