/****************************************************
    文件：ItemEntityHp.cs
	作者：Ling
    邮箱: 1759147969@qq.com
    日期：2019/8/29 14:53:1
	功能：血条
*****************************************************/

using UnityEngine;
using UnityEngine.UI;

public class ItemEntityHp : MonoBehaviour
{
	public Image ImgFgYellow;
	public Image ImgFgRed;
	public Text TxtCritical;
//	public Text TxtDodge;
	public Text TxtDamage;
	public Animation AniCritical;
	public Animation AniDodge;
	public Animation AniDamage;

	private int _hpVal;
	private Transform _rootTrans;
	private RectTransform _rect;
	private float _scaleRate = 1.0f * Constants.SceneHeight / Screen.height;

	private void Update()
	{
		Vector3 screenPos = Camera.main.WorldToScreenPoint(_rootTrans.position);
		_rect.anchoredPosition = screenPos;

		//Debug.Log(_currentPrg + "  " + _targetPrg);
		UpdateMixBlend();
		ImgFgYellow.fillAmount = _currentPrg;
	}

	public void UpdateMixBlend()
	{
		if (Mathf.Abs(_currentPrg - _targetPrg) < Constants.AccelerHpSpeed * Time.deltaTime)
		{
			_currentPrg = _targetPrg;
			ImgFgRed.fillAmount = _currentPrg;
		}
		else if (_currentPrg > _targetPrg)
		{
			_currentPrg -= Constants.AccelerHpSpeed * Time.deltaTime;
		}
		else
		{
			_currentPrg += Constants.AccelerHpSpeed * Time.deltaTime;
		}
	}

	public void SetItemInfo(Transform trans,int hp)
	{
		_hpVal = hp;
		_rootTrans = trans;
		_rect = transform.GetComponent<RectTransform>();
		ImgFgYellow.fillAmount = 1;
		ImgFgRed.fillAmount = 1;
	}
	//显示暴击字样
	public void SetCritical(int hurt)
	{
		//暂停显示的动画，重新播放
		AniCritical.Stop();
		TxtCritical.text = "暴击 " + hurt;
		AniCritical.Play();
	}
	//显示闪避字样
	public void SetDodge()
	{
		AniDodge.Stop();
		AniDodge.Play();
	}
	//显示伤害字样
	public void SetDamage(int hurt)
	{
		AniDamage.Stop();
		TxtDamage.text = "-" + hurt;
		AniDamage.Play();
	}

	private float _currentPrg;
	private float _targetPrg = 1;
	
	//血条减少渐变
	public void SetHpVal(int old,int now)
	{
		_currentPrg = old * 1.0f / _hpVal;
		_targetPrg = now * 1.0f / _hpVal;
//		Debug.Log(_currentPrg + " ddd " + _targetPrg);
		
		if(_targetPrg < _currentPrg)
			ImgFgRed.fillAmount = _targetPrg;
	}
}