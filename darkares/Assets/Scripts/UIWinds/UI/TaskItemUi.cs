/****************************************************
    文件：TaskItem.cs
	作者：Ling
    邮箱: 1759147969@qq.com
    日期：2019/8/21 18:15:3
	功能：TaskItem初始化脚本
*****************************************************/

using PEProtocal;
using UnityEditor.VersionControl;
using UnityEngine;
using UnityEngine.UI;

public class TaskItemUi : MonoBehaviour
{
	private Text _taskTitle;
	private Text _taskPro;
	private Text _taskExp;
	private Text _taskCoin;
	private Image _taskProUi;
	private Image _taskFinish;
	private Image _taskFinishUi;
	private Button _taskFinishBtn;

	private ResServices _resSvc;
	private TaskRewardData _rewardData;
	private TaskRewardCfg _rewardCfg;
	private void Init()
	{
		_resSvc = ResServices.Instance;
		
		_taskTitle = transform.Find("TaskTitle").GetComponent<Text>();
		_taskPro = transform.Find("TaskPro").GetComponent<Text>();
		_taskExp = transform.Find("Experience").GetComponent<Text>();
		_taskCoin = transform.Find("Coin").GetComponent<Text>();
		_taskProUi = transform.Find("TaskProUI").GetComponent<Image>();
		_taskFinish = transform.Find("TaskFinish").GetComponent<Image>();
		_taskFinishUi = transform.Find("TaskFinishUI").GetComponent<Image>();
		_taskFinishBtn = transform.Find("TaskFinish").GetComponent<Button>();
		_taskFinishBtn.onClick.AddListener(ClickFinish);
	}

	public void RefreshUi(TaskRewardData data, TaskRewardCfg cfg)
	{
		Init();
		_rewardData = data;
		_rewardCfg = cfg;
		
		_taskTitle.text = cfg.TaskName;
		_taskPro.text = data.Count + "/" + cfg.Count;
		_taskExp.text = "经验" + cfg.Exp;
		_taskCoin.text = "金币" + cfg.Coin;
		_taskProUi.fillAmount = data.Count * 1.0f / cfg.Count;

		if (data.Finish)
		{
			_taskFinishBtn.interactable = false;
			_taskFinish.sprite = _resSvc.LoadSprite(PathDefind.Box1);
			_taskFinishUi.gameObject.SetActive(true);
		}
		else if (data.Count == cfg.Count)
		{
			_taskFinish.sprite = _resSvc.LoadSprite(PathDefind.Box2);
			_taskFinishUi.gameObject.SetActive(false);
		}
		else
		{
			_taskFinish.sprite = _resSvc.LoadSprite(PathDefind.Box1);
			_taskFinishUi.gameObject.SetActive(false);
		}
	}

	public void ClickFinish()
	{
		if(_rewardData.Finish)
		{
			return;
		}
		if (_rewardData.Count == _rewardCfg.Count)
		{
			_taskFinish.sprite = _resSvc.LoadSprite(PathDefind.Box1);
			_taskFinishUi.gameObject.SetActive(true);
			_rewardData.Finish = true;
			_taskFinishBtn.onClick.RemoveListener(ClickFinish);
			_taskFinishBtn.interactable = false;
			NetServices.Instance.SendMsg(new GameMsg
			{
				cmd = (int)CMD.ReqTask,
				reqTask = new ReqTask
				{
					id = _rewardData.ID
				}
			});
		}
	}
}