/****************************************************
    文件：GuideWind.cs
	作者：Ling
    邮箱: 1759147969@qq.com
    日期：2019/1/24 18:50:31
	功能：人物对话框方法
*****************************************************/

using PEProtocal;
using UnityEngine;
using UnityEngine.UI;

public class GuideWind : WindowRoot 
{
    public Text txtTalk;
    public Text txtPlayerName;
    public Image imgPlayer;


    private PlayerData playerData;
    private AutoGuideCfg curTaskData;
    private string[] dialogArr;
    private int index;   //当前对话位置

    protected override void InitWnd()
    {
        base.InitWnd();

        //获得角色信息
        playerData = GameRoot.Instance.playerData;
        //获得对话信息
        curTaskData = MainCitySys.Instance.GetCurtTaskData();
        //分割xml文件中的信息
        dialogArr = curTaskData.DilogArr.Split('#');
        index = 1;

        SetTalk();
    }

    public void SetTalk()
    {
        string[] talkArr = dialogArr[index].Split('|');
        if(talkArr[0] == "0")
        {
            SetSprite(imgPlayer, PathDefind.Assassin);
            SetText(txtPlayerName, playerData.name);
        }
        else
        {
            //对话NPC
            switch (curTaskData.NpcId)
            {
                case 0:
                    SetSprite(imgPlayer, PathDefind.WiseInfo);
                    SetText(txtPlayerName, "智者");
                    break;
                case 1:
                    SetSprite(imgPlayer, PathDefind.GeneralInfo);
                    SetText(txtPlayerName, "将军");
                    break;
                case 2:
                    SetSprite(imgPlayer, PathDefind.ArtisanInfo);
                    SetText(txtPlayerName, "工匠");
                    break;
                case 3:
                    SetSprite(imgPlayer, PathDefind.TraderInfo);
                    SetText(txtPlayerName, "商人");
                    break;
                case 4:
                    SetSprite(imgPlayer, PathDefind.GuideInfo);
                    SetText(txtPlayerName, "引路人");
                    break;
            }
        }
        imgPlayer.SetNativeSize();
        SetText(txtTalk, talkArr[1].Replace("$name", playerData.name));
    }
    //下一步
    public void NextGuide()
    {
        _audioSvc.PlayeUiAudio(Constants.UiClick);

        index += 1;
        if (index >= dialogArr.Length)
        {
            //TODO  发送当前人物完成的提示，并给予奖励
            GameMsg msg = new GameMsg         //定义消息
            {
                cmd = (int)CMD.ReqGuide,
                reqGuide = new ReqGuide
                {
                    npcID = curTaskData.ID
                }
            };
            _netSvc.SendMsg(msg);   //向服务器发送消息
            SetWindState(false);  //关闭窗口。
        }
        else
        {
            SetTalk();  //下面的对话
        } 
    } 
}