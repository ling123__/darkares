/****************************************************
    文件：StrongWind.cs
	作者：Ling
    邮箱: 1759147969@qq.com
    日期：2019/1/28 20:30:38
	功能：强化界面
*****************************************************/

using PEProtocal;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using Object = System.Object;

public class StrongWind : WindowRoot
{
    #region EquipInfo

    public Image ShowItemImage;
    public Text StarLvText;
    public Transform StarTrans;
    public Text LifeText1;
    public Text HurtText1;
    public Text DefendText1;
    public Text LifeText2;
    public Text HurtText2;
    public Text DefendText2;
    public Text NeedLvText;
    public Text CoinText;
    public Text TotalCoinText;
    public Text CrystalText1;
    public Image Arrow1;
    public Image Arrow2;
    public Image Arrow3;
    public Transform costTrans;
    #endregion
    
    public Transform equipList;
    public Transform equipNextList;

    private int currentIndex;
    private PlayerData _playerData;
    private EquipCfg _equipCfg;
    
    protected override void InitWnd()
    {
        base.InitWnd();
        _playerData = GameRoot.Instance.playerData;
        RegisterBtnEvent();
        RefreshItemData();
    }
    //关闭装备强化窗口
    public void CloseStrongWind()
    {
        _audioSvc.PlayeUiAudio(Constants.UiClick);
        SetWindState(false);
    }
    //注册左侧列表物品监听事件
    private void RegisterBtnEvent()
    {
        GameObject go = null;
        for (int i = 0; i < equipList.childCount; i++)
        {
            go = equipList.GetChild(i).gameObject;
            OnClick(go, (Object args) =>
            {
                ClickItem((int)args);
                _audioSvc.PlayeUiAudio(Constants.UiClick);
            },i);
        }
    }

    private void ClickItem(int index)
    {
        currentIndex = index;
        GameObject go = equipNextList.GetChild(index).gameObject;
        if (go.activeSelf)
        {
            return;
        }
        else
        {
            //先将所有的隐藏
            for (int i = 0; i < equipNextList.childCount; i++)
            {
                equipNextList.GetChild(i).gameObject.SetActive(false);
            }
            go.SetActive(true);
        }
        RefreshItemData();
    }

    //点击强化按钮
    public void ClickStrongBtn()
    {
        _audioSvc.PlayeUiAudio(Constants.UiClick);
        if(_equipCfg == null)
        {
            GameRoot.AddTips("物品星级已满！");
            return;
        }
        if (_equipCfg.StartLv <= Constants.StarCount)
        {
            if (_playerData.lv < _equipCfg.MinLv)
            {
                GameRoot.AddTips("角色等级不够");
            }
            if (_playerData.coin < _equipCfg.Coin)
            {
                GameRoot.AddTips("金币不够");
            }
            if (_playerData.crystal < _equipCfg.Crystal)
            {
                GameRoot.AddTips("水晶不够");
            }
            //发送请求强化的数据
            _netSvc.SendMsg(new GameMsg
            {
                cmd = (int)CMD.ReqEquip,
                reqEquip = new ReqEquip
                {
                    pos = currentIndex
                }
            });
        }
        else
        {
            GameRoot.AddTips("物品星级已满！");
        }
    }

    //根据选择的Item刷新数据
    private void RefreshItemData()
    {
        //StartLvText.text = _playerData.equip
        SetText(TotalCoinText,_playerData.coin);
        //根据选择的item显示UI
        switch (currentIndex)
        {
            case 0:
                SetSprite(ShowItemImage,PathDefind.ItemToukui);
                break;
            case 1:
                SetSprite(ShowItemImage,PathDefind.ItemBody);
                break;
            case 2:
                SetSprite(ShowItemImage,PathDefind.ItemYaobu);
                break;
            case 3:
                SetSprite(ShowItemImage,PathDefind.ItemHand);
                break;
            case 4:
                SetSprite(ShowItemImage,PathDefind.ItemLeg);
                break;
            case 5:
                SetSprite(ShowItemImage,PathDefind.ItemFoot);
                break;
        }
        SetText(StarLvText,_playerData.equip[currentIndex] + "星级");
        //设置星星数量
        int star = _playerData.equip[currentIndex];
        for (int i = 0; i < Constants.StarCount; i++)
        {
            if (i < star)
                SetSprite(StarTrans.GetChild(i).GetComponent<Image>(), PathDefind.Star2);
            else
                SetSprite(StarTrans.GetChild(i).GetComponent<Image>(), PathDefind.Star1);
        }

        int nextLevel = star + 1;
        //设置当前生命、伤害、防御
        int hp = ResServices.Instance.GetStarAllProp(currentIndex, nextLevel, 0);
        int hurt = ResServices.Instance.GetStarAllProp(currentIndex, nextLevel, 1);
        int def = ResServices.Instance.GetStarAllProp(currentIndex, nextLevel, 2);
        SetText(LifeText1,"+"+hp);
        SetText(HurtText1,"+"+hurt);
        SetText(DefendText1,"+"+def);

        _equipCfg = _resSvc.GetEquipData(currentIndex, nextLevel);
        if (_equipCfg != null)
        {
            SetActive(LifeText2);
            SetActive(HurtText2);
            SetActive(DefendText2);
            SetActive(costTrans);
            SetActive(Arrow1);
            SetActive(Arrow2);
            SetActive(Arrow3);
            
            SetText(LifeText2,_equipCfg.AddHp);
            SetText(HurtText2,_equipCfg.AddHurt);
            SetText(DefendText2,_equipCfg.AddDef);
            SetText(NeedLvText,_equipCfg.MinLv);
            SetText(CoinText,_equipCfg.Coin);
            SetText(CrystalText1,_equipCfg.Crystal + "/" + _playerData.crystal);
        }
        else
        {
            SetActive(LifeText2,false);
            SetActive(HurtText2,false);
            SetActive(DefendText2,false);
            SetActive(costTrans,false);
            SetActive(Arrow1,false);
            SetActive(Arrow2,false);
            SetActive(Arrow3,false);
        }
    }

    public void UpdateUI()
    {
        _audioSvc.PlayeUiAudio(Constants.EquipStrong);
        RefreshItemData();
    }
}