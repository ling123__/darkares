/****************************************************
    文件：BuyWind.cs
	作者：Ling
    邮箱: 1759147969@qq.com
    日期：2019/8/17 8:29:23
	功能：购买交易窗口
*****************************************************/

using PEProtocal;
using UnityEngine;
using UnityEngine.UI;

public class BuyWind : WindowRoot
{
	public Text buyTxt;
	public Button btnSure;
	
	private int buyType;
	private PlayerData _player;
	protected override void InitWnd()
	{
		base.InitWnd();
		_player = GameRoot.Instance.playerData;
		RefreshUi();
	}

	//设置这个购买窗口是那种类型的购买
	public void SetBuyType(int type)
	{
		//0 钻石买体力  1  钻石卖金币
		buyType = type;
	}

	public void RefreshUi()
	{
		switch (buyType)
		{
			case 0:
				buyTxt.text = "是否花费" + Constants.color("10钻石", ColorText.Red) + "购买" + Constants.color("100体力", ColorText.Green);
				break;
			case 1:
				buyTxt.text = "是否花费" + Constants.color("10钻石", ColorText.Red) + "购买" + Constants.color("1000金币", ColorText.Green);
				break;
		}
	}

	//接受服务器返回的商品购买操作
	public void RspBuy(GameMsg msg)
	{
		RspBuy rspBuy = msg.rspBuy;
		_player.diamond = rspBuy.diamond;
		_player.coin = rspBuy.coin;
		_player.power = rspBuy.power;
		switch (rspBuy.type)
		{
			case 0:
				GameRoot.AddTips("购买体力成功");
				break;
			case 1:
				GameRoot.AddTips("购买金币成功");
				break;
		}
		_audioSvc.PlayeUiAudio(Constants.EquipStrong);
		btnSure.interactable = true;
		SetWindState(false);
	}
	#region 点击事件
	//关闭购买界面
	public void ClickCloseBtn()
	{
		_audioSvc.PlayeUiAudio(Constants.UiClick);
		SetWindState(false);
	}
	//点击购买按钮
	public void ClickBuyBtn()
	{
		//TODO 这里以后需要把10换掉
		if (_player.diamond < 10)
		{
			GameRoot.AddTips("钻石不够！");
			return;
		}
		_netSvc.SendMsg(new GameMsg
		{
			cmd = (int)CMD.ReqBuy,
			reqBuy = new ReqBuy
			{
				type = buyType,
				diamond = _player.diamond
			}
		});
		btnSure.interactable = false;
	}
	#endregion
}