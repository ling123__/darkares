/****************************************************
    文件：PlayerCtrlWind.cs
	作者：Ling
    邮箱: 1759147969@qq.com
    日期：2019/8/22 22:13:11
	功能：战斗界面
*****************************************************/

using PEProtocal;
using UnityEditor;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class PlayerCtrlWind : WindowRoot
{
	public Text TxtHp;          //血条
	public Text TxtMagic;
	public Text TxtLv;
	public Text TxtName;
	public Text TxtExpPrg;
	public Transform ExpPrgTrans;  //存放exp经验条的list
	public Image TxtExp;
	public Image ImgSelfHp;
	public Image ImgRouletteArea;   //轮盘相应的区域
	public Image ImgRouletteBg;     //轮盘的背景
	public Image ImgRoulette;       //轮盘的按钮
	[HideInInspector]
	public Vector2 CurrentDir = Vector2.zero;
	
	private Animator _menuAnim;        //获得开关的动画组件
	private Vector2 _startPos;         //遥感原点的开始位置
	private Vector2 _defaultPos;       //默认位置，遥感背景的
	private float _pointDis;
	private int _totalHp;            //人物总血量
	
	protected override void InitWnd()
	{
		base.InitWnd();
		//得到屏幕的压缩比例。就是屏幕的自适应。
		_pointDis = Screen.height * 1.0f / Constants.SceneHeight * Constants.RouletteDir;

		SetActive(ImgRoulette,false);    //刚开始是不会显示小点的
		_defaultPos = ImgRouletteBg.transform.position;
		ImgSelfHp.fillAmount = 1;

		_totalHp = GameRoot.Instance.playerData.hp;
		SetText(TxtHp,_totalHp+"/"+_totalHp);
		
		
		RegisterTouchEvents();           //调用事件
		for (int i = 1; i < 4; i++)
		{
			_skillCdTime[i] = _resSvc.GetSkillCfg((int) SkillEnum.Tuxi + (i - 1)).CdTime / 1000.0f;
			_skillTime[i] = _skillCdTime[i];
		}
		RefreshUI();
	}
	
	public void RefreshUI()
    {
        //得到用户的数据
        PlayerData playerData = GameRoot.Instance.PlayerData;

        SetText(TxtLv, playerData.lv);   //设置等级
//	    SetText(TxtHp,playerData.hp + "/");
	    SetText(TxtMagic,playerData.ap);
        SetText(TxtName, playerData.name);  //设置名字

        /*经验条布局*/
        //百分比数值的显示
        int expPrgVal = (int)(playerData.exp * 1.0f / PECommon.GetExpUpVal(playerData.lv) * 100);
        SetText(TxtExpPrg, expPrgVal+"%");
        float temp = 100 / 15;  //我的经验条有15个小段，所以需要转换一下
        int index = (int)(expPrgVal / temp);    //得到当前百分比对应的那个经验条

        GridLayoutGroup grid = ExpPrgTrans.GetComponent<GridLayoutGroup>();   //得到布局引用

        //当前Canvas是以Height为基准的
        float globalRate = 1.0f * Constants.SceneHeight / Screen.height;  //屏幕与标准屏幕的比例
        //获得真实宽度
        float screenWidth = Screen.width * globalRate;
        //174  是由76+14*7得到。总共15个经验条
        float width = (screenWidth - 174) / 15;

        grid.cellSize = new Vector2(width, 7);  //设置经验条的长度

        /*循环检索，设置对应经验条*/
        for(int i = 0; i < ExpPrgTrans.childCount; i++)
        {
            Image img = ExpPrgTrans.GetChild(i).GetComponent<Image>();
            if(i < index)
            {
                img.fillAmount = 1;
            }
            else if(i == index)
            {
                //设置当前经验条的显示
                img.fillAmount = expPrgVal % 15 * 1.0f / 15;
            }
            else
            {
                img.fillAmount = 0;
            }
        }
    }
	
	//注册轮盘响应函数
	public void RegisterTouchEvents()
	{
		//轮盘的背景图片移动到鼠标点击的位置
		OnClickDown(ImgRouletteArea.gameObject, (PointerEventData evt) =>
		{
			_startPos = evt.position;
			SetActive(ImgRoulette);
			ImgRouletteBg.transform.position = evt.position;
		});
		OnClickUp(ImgRouletteArea.gameObject, (PointerEventData evt) =>
		{
			ImgRouletteBg.transform.position = _defaultPos;  //返回到默认位置
			SetActive(ImgRoulette, false);
			ImgRoulette.transform.localPosition = Vector2.zero;   //相对于父物体，位置在正中心
			CurrentDir = Vector2.zero;
			BattleSys.Instance.SetMoveDir(CurrentDir);
		});
		OnClickDrag(ImgRouletteArea.gameObject, (PointerEventData evt) =>
		{
			Vector2 dir = evt.position - _startPos;     //得到方向
			float len = dir.magnitude;                 //向量化，得到长度
			if(len > _pointDis)
			{
				//参数方向，最大长度，
				Vector2 clamDir = Vector2.ClampMagnitude(dir, _pointDis);  //将位置限制回来
				ImgRoulette.transform.position = _startPos + clamDir;  //遥感点的位置，已经限制在了遥感背景中
			}
			else
			{
				ImgRoulette.transform.position = evt.position;  //否则就是直接的赋值
			}

			CurrentDir = dir.normalized;
			BattleSys.Instance.SetMoveDir(CurrentDir);
		});
	}

	private void Update()
	{
		float delta = Time.deltaTime;
		for (int i = 1; i < 4; i++)
		{
			if (_skillCd[i])  //该技能在冷却
			{
				_skillTimePro[i] += delta;
				if (_skillTimePro[i] >= _skillCdTime[i])
				{
					_skillCd[i] = false;
					SetActive(ImgSkill[i],false);
					_skillTimePro[i] = 0;
					_skillTime[i] = _skillCdTime[i];
				}
				else
				{
					ImgSkill[i].fillAmount = 1 - _skillTimePro[i] / _skillCdTime[i];
				}

				_cdTime[i] += delta;
				if (_cdTime[i] >= 1)
				{
					_cdTime[i] -= 1;
					_skillTime[i] -= 1;
					if (_skillTime[i] <= 0)
					{
						_skillTime[i] = 0;
						_cdTime[i] = 0;
					}
					SetText(TxtSkill[i], (int) _skillTime[i]);
				}
			}
		}
	}

	public Image[] ImgSkill;
	public Text[] TxtSkill;
	private float[] _cdTime = new float[4];
	private float[] _skillCdTime = new float[4];  //技能剩余冷却时间
	private float[] _skillTimePro = new float[4];    //当前技能冷却进度
	private float[] _skillTime = new float[4];     //当前技能冷却了的时间
	private bool[] _skillCd = new bool[4];    //当前技能是否需要冷却
	//点击释放技能
	public void ClickReleaseSKill(int index)
	{
		if (index == 0)
			BattleSys.Instance.ReleaseSKill(index);
		else if (!_skillCd[index])
		{
			BattleSys.Instance.ReleaseSKill(index);
			_skillCd[index] = true;
			SetActive(ImgSkill[index]);
			ImgSkill[index].fillAmount = 1;
			SetText(TxtSkill[index], (int)_skillTime[index]);
		}
	}
	
	//设置人物血量
	public void SetPlayerHpByVal(int hp)
	{
		SetText(TxtHp, hp + "/" + _totalHp);
		ImgSelfHp.fillAmount = hp * 1.0f / _totalHp;
	}
}