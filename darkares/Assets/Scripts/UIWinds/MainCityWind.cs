/****************************************************
    文件：MainCityWind.cs
	作者：Ling
    邮箱: 1759147969@qq.com
    日期：2019/1/13 12:58:13
	功能：主城界面
*****************************************************/

using PEProtocal;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class MainCityWind : WindowRoot
{
    public Text txtFight;
    public Text txtPower;
    public Image imgPowerPrg;  //体力图片的显示
    public Text txtLevel;
    public Text txtName;
    public Text txtExpPrg;
    public Transform expPrgTrans;  //存放exp经验条的list
    public GameObject MenuBtn;        //控制左下角页面的开关
    public Image imgRouletteArea;   //轮盘相应的区域
    public Image imgRouletteBG;     //轮盘的背景
    public Image imgRoulette;       //轮盘的按钮

    public AutoGuideCfg curTaskData;   //当前任务图标
    public Button btnGuide;            //自动任务引导按钮

    private Animator MenuAnim;        //获得开关的动画组件
    private Vector2 startPos;         //遥感原点的开始位置
    private Vector2 defaultPos;       //默认位置，遥感背景的
    private float pointDis;

    protected override void InitWnd()
    {
        base.InitWnd();
        //得到屏幕的压缩比例。就是屏幕的自适应。
        pointDis = Screen.height * 1.0f / Constants.SceneHeight * Constants.RouletteDir;

        SetActive(imgRoulette,false);    //刚开始是不会显示小点的
        defaultPos = imgRouletteBG.transform.position;
        RegisterTouchEvents();           //调用事件

        RefreshUI();
    }

    public void RefreshUI()
    {
        //得到用户的数据
        PlayerData playerData = GameRoot.Instance.PlayerData;

        //获得角色战斗力，并将其显示在界面上
        SetText(txtFight, PECommon.GetFightByProps(playerData));
        //获取角色的体力
        SetText(txtPower, "体力:"+playerData.power+"/"+PECommon.GetPowerLimit(playerData.lv));
        //设置当前体力占总体力的百分比
        imgPowerPrg.fillAmount = playerData.power * 1.0f / PECommon.GetPowerLimit(playerData.lv);

        SetText(txtLevel, playerData.lv);   //设置等级
        SetText(txtName, playerData.name);  //设置名字

        /*经验条布局*/
        //百分比数值的显示
        int expPrgVal = (int)(playerData.exp * 1.0f / PECommon.GetExpUpVal(playerData.lv) * 100);
        SetText(txtExpPrg, expPrgVal+"%");
        float temp = 100 / 15;  //我的经验条有15个小段，所以需要转换一下
        int index = (int)(expPrgVal / temp);    //得到当前百分比对应的那个经验条

        GridLayoutGroup grid = expPrgTrans.GetComponent<GridLayoutGroup>();   //得到布局引用

        //当前Canvas是以Height为基准的
        float globalRate = 1.0f * Constants.SceneHeight / Screen.height;  //屏幕与标准屏幕的比例
        //获得真实宽度
        float screenWidth = Screen.width * globalRate;
        //174  是由76+14*7得到。总共15个经验条
        float width = (screenWidth - 174) / 15;

        grid.cellSize = new Vector2(width, 7);  //设置经验条的长度

        /*循环检索，设置对应经验条*/
        for(int i = 0; i < expPrgTrans.childCount; i++)
        {
            Image img = expPrgTrans.GetChild(i).GetComponent<Image>();
            if(i < index)
            {
                img.fillAmount = 1;
            }
            else if(i == index)
            {
                //设置当前经验条的显示
                img.fillAmount = expPrgVal % 15 * 1.0f / 15;
            }
            else
            {
                img.fillAmount = 0;
            }
        }
        /*设置自动任务图标*/
        curTaskData = _resSvc.GetAutoGuideData(playerData.guideid);
        if(curTaskData != null)
        {
            SetGuideBtnIcon(curTaskData.NpcId);
        }
        else
        {
            SetGuideBtnIcon(-1);
        }
        
    }
    //设置引导人物的图片显示
    public void SetGuideBtnIcon(int npcid)
    {
        string npcPath = "";
        Image image = btnGuide.GetComponent<Image>();
        switch (npcid)
        {
            case Constants.NpcWiseMan:
                npcPath = PathDefind.WiseHead;
                break;
            case Constants.NpcGeneralMan:
                npcPath = PathDefind.GeneralHead;
                break;
            case Constants.NpcArtisanWoman:
                npcPath = PathDefind.ArtisanHead;
                break;
            case Constants.NpcTraderWoman:
                npcPath = PathDefind.TraderHead;
                break;
            default:
                npcPath = PathDefind.TaskHead;
                break;
        }
        //设置图片
        SetSprite(image, npcPath);
    }
    
    //注册轮盘响应函数
    public void RegisterTouchEvents()
    {
        //轮盘的背景图片移动到鼠标点击的位置
        OnClickDown(imgRouletteArea.gameObject, (PointerEventData evt) =>
        {
            startPos = evt.position;
            SetActive(imgRoulette);
            imgRouletteBG.transform.position = evt.position;
        });
        OnClickUp(imgRouletteArea.gameObject, (PointerEventData evt) =>
        {
            imgRouletteBG.transform.position = defaultPos;  //返回到默认位置
            SetActive(imgRoulette, false);
            imgRoulette.transform.localPosition = Vector2.zero;   //相对于父物体，位置在正中心

            MainCitySys.Instance.SetMoveDir(Vector2.zero);
        });
        OnClickDrag(imgRouletteArea.gameObject, (PointerEventData evt) =>
        {
            Vector2 dir = evt.position - startPos;     //得到方向
            float len = dir.magnitude;                 //向量化，得到长度
            if(len > pointDis)
            {
                //参数方向，最大长度，
                Vector2 clamDir = Vector2.ClampMagnitude(dir, pointDis);  //将位置限制回来
                imgRoulette.transform.position = startPos + clamDir;  //遥感点的位置，已经限制在了遥感背景中
            }
            else
            {
                imgRoulette.transform.position = evt.position;  //否则就是直接的赋值
            }
            MainCitySys.Instance.SetMoveDir(dir.normalized);
        });
    }
    
    #region ClickEvents
    
    private bool isMenuOpen = true;
    public void ClickMenuBtn()
    {
        _audioSvc.PlayeUiAudio(Constants.UiMenuClick);
        MenuAnim = MenuBtn.GetComponent<Animator>();
        if (isMenuOpen)
        {
            MenuAnim.SetBool("Close", true);
            MenuAnim.SetBool("Open", false);
            isMenuOpen = false;
        }
        else
        {
            MenuAnim.SetBool("Close", false);
            MenuAnim.SetBool("Open", true);
            isMenuOpen = true;
        }
    }
    //打开角色信息窗口
    public void ClickPlayerInfo()
    {
        _audioSvc.PlayeUiAudio(Constants.UiOpenPage);
        MainCitySys.Instance.OpenInfoWind();   //打开角色信息窗口
    }
    //打开强化界面
    public void ClickStrong()
    {
        _audioSvc.PlayeUiAudio(Constants.UiOpenPage);
        MainCitySys.Instance.OpenStrongWind();  //打开装备强化界面
    }

    

    //任务自动导航按钮
    public void ClickGuideBtn()
    {
        _audioSvc.PlayeUiAudio(Constants.UiClick);

        if(curTaskData != null)
        {
            MainCitySys.Instance.RunTask(curTaskData);
        }
        else
        {
            GameRoot.AddTips("更多的功能正在开发中......");
        }
    }
    
    
    //打开聊天界面
    public void ClickChatBtn()
    {
        _audioSvc.PlayeUiAudio(Constants.UiClick);
        MainCitySys.Instance.OpenChatWind();
    }
    //打开购买物品界面
    public void ClickBuyBtn()
    {
        _audioSvc.PlayeUiAudio(Constants.UiClick);
        MainCitySys.Instance.OpenBuyWind(0);
    }
    //铸造金币
    public void ClickMkCoinBtn()
    {
        _audioSvc.PlayeUiAudio(Constants.UiClick);
        MainCitySys.Instance.OpenBuyWind(1);
    }
    //任务界面
    public void ClickTaskBtn()
    {
        _audioSvc.PlayeUiAudio(Constants.UiClick);
        MainCitySys.Instance.OpenTaskWind();
    }
    //打开副本界面
    public void ClickTranscriptBtn()
    {
        _audioSvc.PlayeUiAudio(Constants.UiClick);
        MainCitySys.Instance.EnterDungeonSys();
    }
    #endregion
}