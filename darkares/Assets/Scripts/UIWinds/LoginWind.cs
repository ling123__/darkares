/****************************************************
    文件：LoginWind.cs
	作者：Ling
    邮箱: 1759147969@qq.com
    日期：2019/1/9 11:19:3
	功能：游戏登陆界面功能
*****************************************************/

using PEProtocal;
using UnityEngine;
using UnityEngine.UI;

public class LoginWind : WindowRoot 
{
    public InputField iptAcct;
    public InputField iptPass;
    public Button btnNotice;
    public Button btnLogin;

    protected override void InitWnd()
    {
        base.InitWnd();
        /*设置用户登陆信息*/
        if(PlayerPrefs.HasKey("Acct") && PlayerPrefs.HasKey("Pass"))
        {
            iptAcct.text = PlayerPrefs.GetString("Acct");
            iptPass.text = PlayerPrefs.GetString("Pass");
        }
        else
        {
            iptAcct.text = "12";
            iptPass.text = "";
        }
    }

    /*保存用户登陆信息*/
    public void EnterGame()
    {
        _audioSvc.PlayeUiAudio(Constants.GameLogin);

        string acct = iptAcct.text;
        string pass = iptPass.text;
        if(acct != "" && pass != "")
        {
            //更新数据
            PlayerPrefs.SetString("Acct", acct);
            PlayerPrefs.SetString("Pass", pass);

            //TODO 发送网络消息，请求登录
            GameMsg msg = new GameMsg
            {
                cmd = (int)CMD.ReqLogin,  //cmd是PESocket中的，CMD是GameMsg中的.登录相关
                reqLogin = new ReqLogin   //将用户密码传过去
                {
                    acct = acct,
                    pass = pass
                }
            };
            _netSvc.SendMsg(msg);   //发送消息给服务端

            //PS这个操作是模拟的
            //LogSys.Instance.RspLogin();
        }
        else
        {
            GameRoot.AddTips("账号或者密码为空");
        }
    }

    public void GameNotice()
    {
        _audioSvc.PlayeUiAudio(Constants.UiClick);
        GameRoot.AddTips("功能正在开发");
    }
}