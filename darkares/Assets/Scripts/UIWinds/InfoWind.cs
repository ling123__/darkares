/****************************************************
    文件：InfoWind.cs
	作者：Ling
    邮箱: 1759147969@qq.com
    日期：2019/1/20 14:5:27
	功能：角色信息展示界面
*****************************************************/

using PEProtocal;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class InfoWind : WindowRoot 
{
    public Text txtInfo;         //人物姓名，等级
    public Text txtExp;
    public Image imgExpPrg;
    public Text txtPower;
    public Image imgPowerPrg;

    public Text txtJob;         //职业
    public Text txtCombat;      //战力
    public Text txtBlood;       //血量
    public Text txtHurt;        //伤害
    public Text txtDef;         //防御

    public RawImage imgChar;    //角色展示图片
    public Transform detailPanel;  //角色详细信息面板

    public Text txtHp;
    public Text txtAd;
    public Text txtAp;
    public Text txtAddef;
    public Text txtApdef;
    public Text txtdodge;
    public Text txtPierce;
    public Text txtcritical;


    private Vector2 startPos;   //触碰起始位置

    protected override void InitWnd()
    {
        base.InitWnd();
        RegTouchEvts();

        SetActive(detailPanel, false);  //细节面板刚开始关闭

        RefreshUI();
    }

    public void RefreshUI()
    {
        PlayerData playerData = GameRoot.Instance.PlayerData;
        SetText(txtInfo, playerData.name + " LV." + playerData.lv);
        SetText(txtExp, playerData.exp + "/" + PECommon.GetExpUpVal(playerData.lv));
        imgExpPrg.fillAmount = playerData.exp * 1.0f / PECommon.GetExpUpVal(playerData.lv);
        SetText(txtPower, playerData.power + "/" + PECommon.GetPowerLimit(playerData.lv));
        imgPowerPrg.fillAmount = playerData.power * 1.0f / PECommon.GetPowerLimit(playerData.lv);

        SetText(txtJob, "暗夜刺客");
        SetText(txtCombat, PECommon.GetFightByProps(playerData));
        SetText(txtBlood, playerData.hp);
        SetText(txtHurt, playerData.ad + playerData.ap);
        SetText(txtDef, playerData.addef + playerData.apdef);
        //设置详细属性
        SetText(txtHp, playerData.hp);
        SetText(txtAd, playerData.ad);
        SetText(txtAp, playerData.ap);
        SetText(txtAddef, playerData.addef);
        SetText(txtApdef, playerData.apdef);
        SetText(txtdodge, playerData.dodge + "%");
        SetText(txtPierce, playerData.pierce + "%");
        SetText(txtcritical, playerData.critical + "%");
    }

    public void ClosePlayerInfo()
    {
        _audioSvc.PlayeUiAudio(Constants.UiClick);
        MainCitySys.Instance.CloseInfoWind();
    }
    //关闭详细信息
    public void ClosePlayerDetailInfo()
    {
        _audioSvc.PlayeUiAudio(Constants.UiClick);
        SetActive(detailPanel, false);
    }
    //打开角色信息详细面板
    public void OpenPlayerDetailInfo()
    {
        _audioSvc.PlayeUiAudio(Constants.UiClick);
        SetActive(detailPanel);
    }

    private void RegTouchEvts()
    {
        OnClickDown(imgChar.gameObject, (PointerEventData evt) =>
         {
             startPos = evt.position;
             MainCitySys.Instance.SetStartRotate();
         });
        OnClickDrag(imgChar.gameObject, (PointerEventData evt) =>
         {
             float rotate = -(evt.position.x - startPos.x)*0.5f;
             MainCitySys.Instance.SetPlayerRotate(rotate);  
         });
    }
}