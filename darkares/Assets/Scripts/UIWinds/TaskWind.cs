/****************************************************
    文件：TaskWind.cs
	作者：Ling
    邮箱: 1759147969@qq.com
    日期：2019/8/21 13:26:9
	功能：任务界面
*****************************************************/

using System.Collections.Generic;
using PEProtocal;
using UnityEngine;
using UnityEngine.UI;

public class TaskWind : WindowRoot
{
	public Transform ScrollerTrans;
	
	private PlayerData _player;
	private List<TaskRewardData> _taskLst = new List<TaskRewardData>();
	
	protected override void InitWnd()
	{
		base.InitWnd();
		_player = GameRoot.Instance.playerData;
		RefreshUi();
	}

	public void RefreshUi()
	{
		_taskLst.Clear();
		
		List<TaskRewardData> toDoneLst = new List<TaskRewardData>();
		List<TaskRewardData> doneLst = new List<TaskRewardData>();
		List<TaskRewardData> todoLst = new List<TaskRewardData>();

		for (int i = 0; i < _player.taskArr.Length; i++)
		{
			string[] taskInfo = _player.taskArr[i].Split('|');
			TaskRewardData taskData = new TaskRewardData
			{
				ID = int.Parse(taskInfo[0]),
				Count = int.Parse(taskInfo[1]),
				Finish = taskInfo[2].Equals("1")
			};
			TaskRewardCfg cfg = _resSvc.GetTaskCfg(taskData.ID);
			if(taskData.Count == cfg.Count && !taskData.Finish)
				toDoneLst.Add(taskData);
			else if(taskData.Finish)
				doneLst.Add(taskData);
			else
				todoLst.Add(taskData);
		}
		_taskLst.AddRange(toDoneLst);
		_taskLst.AddRange(todoLst);
		_taskLst.AddRange(doneLst);

		for (int i = 0; i < _taskLst.Count; i++)
		{
			if(_resSvc == null)
				return;
			GameObject go = _resSvc.LoadPrefab(PathDefind.TaskItemPrefab);
			go.transform.SetParent(ScrollerTrans,true);
			
			go.name = "TaskItem_" + i;
			go.transform.localPosition = Vector3.zero;
			go.transform.localScale = Vector3.one;
			
			TaskRewardData data = _taskLst[i];
			TaskRewardCfg cfg = _resSvc.GetTaskCfg(data.ID);
			
			go.AddComponent<TaskItemUi>().RefreshUi(data,cfg);
		}
	}

	public void ClickCloseBtn()
	{
		for (int i = 0; i < ScrollerTrans.childCount; i++)
		{
			Destroy(ScrollerTrans.GetChild(i).gameObject);
		}
		_audioSvc.PlayeUiAudio(Constants.UiClick);
		SetWindState(false);
	}
}