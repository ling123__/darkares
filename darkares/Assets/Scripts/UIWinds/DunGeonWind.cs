/****************************************************
    文件：TransScriptWind.cs
	作者：Ling
    邮箱: 1759147969@qq.com
    日期：2019/8/22 12:39:41
	功能：副本界面
*****************************************************/

using PEProtocal;
using UnityEngine;

public class DunGeonWind : WindowRoot
{
	public Transform[] DunsArr;
	public Transform tip;

	private PlayerData _player;
	protected override void InitWnd()
	{
		base.InitWnd();
		_player = GameRoot.Instance.playerData;
		RefreshUi();
	}

	public void RefreshUi()
	{
		int tid = _player.dungeon;
		for (int i = 0; i < DunsArr.Length; i++)
		{
			if (i < tid % 10000)
			{
				SetActive(DunsArr[i].gameObject);
				if (i == tid % 10000 - 1)
				{
					tip.SetParent(DunsArr[i]);
					tip.localPosition = new Vector3(25,100,0);
				}
			}
			else
			{
				SetActive(DunsArr[i],false);
			}
		}
	}

	public void ClickTaskBtn(int tid)
	{
		_audioSvc.PlayeUiAudio(Constants.UiClick);
		int power = _resSvc.GetMapCfg(tid).Power;

		if (_player.power >= power)
		{
			_netSvc.SendMsg(new GameMsg
			{
				cmd = (int)CMD.ReqDungeon,
				reqDungeon = new ReqDungeon
				{
					tid = tid
				}
			});
		}
		else
		{
			GameRoot.AddTips(Constants.color("体力不足！",ColorText.Blue));
		}
	}

	public void ClickCloseBtn()
	{
		_audioSvc.PlayeUiAudio(Constants.UiClick);
		SetWindState(false);
	}
}