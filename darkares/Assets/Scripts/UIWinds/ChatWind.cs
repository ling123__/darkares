/****************************************************
    文件：ChatWind.cs
	作者：Ling
    邮箱: 1759147969@qq.com
    日期：2019/8/16 13:22:5
	功能：聊天窗口
*****************************************************/

using System.Collections.Generic;
using PEProtocal;
using UnityEngine;
using UnityEngine.UI;

public class ChatWind : WindowRoot
{
	public Image imgWord;
	public Image imgUnion;
	public Image imgFriend;
	public Text txtChat;
	public InputField chatInput;

	private int _chatType;
	private bool _canChat;
	private List<string> _wordChatInfo = new List<string>();
	private List<string> _unionChatInfo = new List<string>();
	private List<string> _friendChatInfo = new List<string>();

	protected override void InitWnd()
	{
		base.InitWnd();
		_chatType = 0;
		_canChat = true;
		RefreshUi();
	}


	private void RefreshUi()
	{
		string chatMsg = "";
		SetSprite(imgWord,PathDefind.ItemBtnType2);
		SetSprite(imgUnion,PathDefind.ItemBtnType2);
		SetSprite(imgFriend,PathDefind.ItemBtnType2);
		
		if (_chatType == 0)
		{
			for (int i = 0; i < _wordChatInfo.Count; i++)
			{
				chatMsg += _wordChatInfo[i] + "\n";
			}
			SetText(txtChat,chatMsg);
			SetSprite(imgWord,PathDefind.ItemBtnType1);
		}
		else if (_chatType == 1)
		{
			for (int i = 0; i < _unionChatInfo.Count; i++)
			{
				chatMsg += _unionChatInfo[i] + "\n";
			}
			SetText(txtChat,"暂未加入公会");
			SetSprite(imgUnion,PathDefind.ItemBtnType1);
		}
		else if (_chatType == 2)
		{
			for (int i = 0; i < _friendChatInfo.Count; i++)
			{
				chatMsg += _friendChatInfo[i] + "\n";
			}
			SetText(txtChat,"暂无好友信息");
			SetSprite(imgFriend,PathDefind.ItemBtnType1);
		}
	}
	//关闭聊天界面
	public void CloseChatWind()
	{
		_audioSvc.PlayeUiAudio(Constants.UiClick);
		SetWindState(false);
	}
	//世界聊天按钮
	public void ChatWordInfo()
	{
		_audioSvc.PlayeUiAudio(Constants.UiClick);
		_chatType = 0;
		RefreshUi();
	}
	//工会聊天按钮
	public void ChatUnionInfo()
	{
		_audioSvc.PlayeUiAudio(Constants.UiClick);
		_chatType = 1;
		RefreshUi();
	}
	//好友聊天按钮
	public void ChatFriendInfo()
	{
		_audioSvc.PlayeUiAudio(Constants.UiClick);
		_chatType = 2;
		RefreshUi();
	}
	//发送消息
	public void ClickSendInfo()
	{
		//TODO 提示一下敏感词判定之类的
		if(string.IsNullOrEmpty(chatInput.text)  || chatInput.text == " ")
			return;
		//TODO 向服务器发送消息  定时器，发送信息后一段时间内不能在发送
		if (chatInput.text.Length > 10)
		{
			GameRoot.AddTips("消息过长");
		}
		else
		{
			_netSvc.SendMsg(new GameMsg
			{
				cmd = (int) CMD.SendChat,
				sendChat = new SendChat
				{
					chatType = _chatType,
					msg = chatInput.text
				}
			}); 
			
			chatInput.text = "";
			//每过5秒才可以发送消息
			_canChat = false;
			_timerSvc.AddTimeTask((int tid) => { _canChat = true; }, 5000);
		}
	}
	//接收服务器发送过来的消息
	public void PushMsg(GameMsg msg)
	{
		//窗口没有打开就不用刷新信息了
		if (!gameObject.activeSelf)
		{
			return;
		}
		PushChat pushMsg = msg.pushChat;
		switch (pushMsg.chatType)
		{
			case 0:
				_wordChatInfo.Add(Constants.color(pushMsg.name,ColorText.Green)+"："+pushMsg.msg);
				if(_wordChatInfo.Count > 10)
					_wordChatInfo.RemoveAt(0);
				break;
			case 1:
				_unionChatInfo.Add(Constants.color(pushMsg.name,ColorText.Green)+"："+pushMsg.msg);
				if(_unionChatInfo.Count > 10)
					_unionChatInfo.RemoveAt(0);
				break;
			case 2:
				_friendChatInfo.Add(Constants.color(pushMsg.name,ColorText.Green)+"："+pushMsg.msg);
				if(_friendChatInfo.Count > 10)
					_friendChatInfo.RemoveAt(0);
				break;
		}
		RefreshUi();
	}
}
