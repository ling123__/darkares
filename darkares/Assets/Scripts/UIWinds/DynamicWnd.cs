/****************************************************
    文件：DynamicWnd.cs
	作者：Ling
    邮箱: 1759147969@qq.com
    日期：2019/1/9 16:40:0
	功能：动态UI界面
*****************************************************/

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DynamicWnd : WindowRoot 
{
    public Animation TipsAnim;
    public Text TipsTxt;
    public Transform ItemHpRoot;
    public Animation PlayerDodgeAni;

    //队列,用来存储Tips信息
    private Queue<String> tipsQue = new Queue<String>();
    private bool isTipShow = false;  //是否播放Tips
    private Dictionary<string,ItemEntityHp> _itemEntity = new Dictionary<string, ItemEntityHp>();

    protected override void InitWnd()
    {
        base.InitWnd();

        SetActive(TipsTxt, false);
    }

    public void AddTips(string tips)
    {
        lock (tipsQue)  //因为是多线程的，上锁保证一下
        {
            tipsQue.Enqueue(tips);  //进入队列
        }
    }

    private void Update()
    {
        lock (tipsQue)
        {
            if(tipsQue.Count > 0 && isTipShow == false)
            {
                string tips = tipsQue.Dequeue();
                isTipShow = true;                //正在播放此条Tip
                SetTips(tips);
            }
        }
    }

    private void SetTips(string tips)
    {
        SetActive(TipsTxt);
        SetText(TipsTxt, tips);

        //得到当前播放的动画
        AnimationClip clip = TipsAnim.GetClip("Tips");
        TipsAnim.Play();  //播放动画

        //延时关闭激活状态
        StartCoroutine(AnimPlayDone(clip.length, () =>
        {
            SetActive(TipsTxt, false);  //关闭Tips
            isTipShow = false;
        }));

    }

    //携程
    private IEnumerator AnimPlayDone(float sec,Action ac)
    {
        yield return new WaitForSeconds(sec);
        if(ac != null)
        {
            ac();
        }
    }
    
    public void AddItemEntity(string mName,Transform trans, int hp)
    {
        ItemEntityHp item = null;
        if (_itemEntity.TryGetValue(mName, out item))
        {
            return;
        }
        else
        {
            GameObject go = _resSvc.LoadPrefab(PathDefind.EntityItemPrefab, true);
            go.transform.SetParent(ItemHpRoot);
            go.transform.localPosition = new Vector3(-100,0,0);
            ItemEntityHp entityHp = go.GetComponent<ItemEntityHp>();
            entityHp.SetItemInfo(trans,hp);
            _itemEntity.Add(mName,entityHp);
        }
    }
    
    //移除
    public void RemoveItemEntity(string mName)
    {
        ItemEntityHp item = null;
        if (_itemEntity.TryGetValue(mName, out item))
        {
            Destroy(item.gameObject);
            _itemEntity.Remove(mName);
        }
    }

    public void SetDodge(string key)
    {
        ItemEntityHp item = null;
        if (_itemEntity.TryGetValue(key, out item))
        {
            item.SetDodge();
        }
    }
    public void SetCritical(string key,int hurt)
    {
        ItemEntityHp item = null;
        if (_itemEntity.TryGetValue(key, out item))
        {
            item.SetCritical(hurt);
        }
    }
    public void SetDamage(string key,int hurt)
    {
        ItemEntityHp item = null;
        if (_itemEntity.TryGetValue(key, out item))
        {
            item.SetDamage(hurt);
        }
    }
    
    public void SetHpVal(string key,int old,int now)
    {
        ItemEntityHp item = null;
        if (_itemEntity.TryGetValue(key, out item))
        {
            item.SetHpVal(old,now);
        }
    }
    
    //显示角色的miss动画
    public void SetPlayerDodge()
    {
        PlayerDodgeAni.Stop();
        PlayerDodgeAni.Play();
    }
}