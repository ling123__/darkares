/****************************************************
    文件：LoadingWind.cs
	作者：Ling
    邮箱: 1759147969@qq.com
    日期：2019/1/9 10:15:43
	功能：加载界面
*****************************************************/

using UnityEngine;
using UnityEngine.UI;

public class LoadingWind : WindowRoot 
{
    public Text txtTips;    //游戏Tips提示文本
    public Text txtPrg;     //游戏进度显示文本
    public Image imgFG;     //进度条
    public Image imgPoint;  //光点

    private float fgWidth;  //进度条的宽度

    protected override void InitWnd()
    {
        base.InitWnd();

        fgWidth = imgFG.rectTransform.sizeDelta.x;  //得到进度条图片的宽度

        SetText(txtTips, "这是一条神奇的天路啊~~");
        SetText(txtPrg, "0%");
        imgFG.fillAmount = 0;
        imgPoint.transform.localPosition = new Vector3(-(fgWidth / 2), 0,0);  //光点的起始位置
    }

    public void SetProgress(float prg)
    {
        SetText(txtPrg, (int)(prg * 100) + "%"); //实时显示加载进度
        imgFG.fillAmount = prg;

        float posX = prg * fgWidth - (fgWidth / 2);
        imgPoint.rectTransform.anchoredPosition = new Vector2(posX, 0);
    }
}