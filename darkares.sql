/*
SQLyog Professional v12.08 (64 bit)
MySQL - 8.0.12 : Database - darkgod
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`darkgod` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_bin */;

/*Table structure for table `account` */

DROP TABLE IF EXISTS `account`;

CREATE TABLE `account` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `acct` varchar(255) NOT NULL,
  `pass` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `lv` int(11) NOT NULL,
  `exp` int(11) NOT NULL,
  `power` int(11) NOT NULL,
  `coin` int(11) NOT NULL,
  `diamond` int(11) NOT NULL,
  `crystal` int(11) NOT NULL,
  `hp` int(11) NOT NULL,
  `ad` int(11) NOT NULL,
  `ap` int(11) NOT NULL,
  `addef` int(11) NOT NULL,
  `apdef` int(11) NOT NULL,
  `dodge` int(11) NOT NULL,
  `pierce` int(11) NOT NULL,
  `critical` int(11) NOT NULL,
  `guideid` int(11) NOT NULL,
  `equip` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `timer` bigint(11) NOT NULL,
  `taskArr` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `dungeon` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=52 DEFAULT CHARSET=utf8;

/*Data for the table `account` */

insert  into `account`(`id`,`acct`,`pass`,`name`,`lv`,`exp`,`power`,`coin`,`diamond`,`crystal`,`hp`,`ad`,`ap`,`addef`,`apdef`,`dodge`,`pierce`,`critical`,`guideid`,`equip`,`timer`,`taskArr`,`dungeon`) values (50,'ling1','123456','',1,0,100,5000,50,50,2000,275,265,67,43,7,5,2,1001,'0#0#0#0#0#0#',1647089749626,'1|0|0#2|0|0#3|0|0#4|0|0#5|0|0#6|0|0#',10001);
insert  into `account`(`id`,`acct`,`pass`,`name`,`lv`,`exp`,`power`,`coin`,`diamond`,`crystal`,`hp`,`ad`,`ap`,`addef`,`apdef`,`dodge`,`pierce`,`critical`,`guideid`,`equip`,`timer`,`taskArr`,`dungeon`) values (51,'ling2','123456','羿慕诗',1,0,100,5000,50,50,2000,275,265,67,43,7,5,2,1001,'0#0#0#0#0#0#',1647090735109,'1|0|0#2|0|0#3|0|0#4|0|0#5|0|0#6|0|0#',10001);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
