﻿/****************************************************
	文件：PEMsg.cs
	作者：Ling
	邮箱: 1759147969@qq.com
	日期：2019/09/02 9:37   	
	功能：消息定义类
*****************************************************/
namespace PENet
{
    using System;
    public class PEMsg
    {
        public int seq;
        public int cmd;
        public int err;
    }
}